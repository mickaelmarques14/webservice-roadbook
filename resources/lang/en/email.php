<?php 
return [
    'footer' => 'Best regards',
    'new_account_subject' => 'New Account', 
    'new_account_body' => 'Your new accout has been created. Your password is :password',
    'request_password_subject' => 'Reset password',
    'request_password_body' => 'Please follow the link to continue reset password',
    'new_document_subject' => 'New Document',
    'new_comment_subject' => 'New Comment',
    'new_comment_body' => 
    'New comment as been set from {:user} \n\r
    With Status {:status} \n\r
    With message {:message} \n\r
    Reference to document ',

];