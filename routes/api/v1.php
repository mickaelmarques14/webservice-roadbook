<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
 */
$api = app('Dingo\Api\Routing\Router');

$api->version('v1', [
    'namespace' => 'App\Http\Controllers\Api\V1',
], function ($api) {
    // create token
    $api->post('auth', [
        'as' => 'authorizations.store',
        'uses' => 'AuthController@store',
    ]);

    // login
    $api->post('login', [
        'as' => 'authorizations.login',
        'uses' => 'AuthController@login',
    ]);

    // register
    $api->post('register', [
        'as' => 'authorizations.register',
        'uses' => 'AuthController@register',
    ]);

    // refresh jwt token
    $api->put('authorizations/current', [
        'as' => 'authorizations.update',
        'uses' => 'AuthController@update',
    ]);
    $api->post('recover', [
        'as' => 'authorizations.recover',
        'uses' => 'AuthController@requestrecover',
    ]);
    // update my password
    $api->put('resetpassword', [
        'as' => 'authorizations.reset',
        'uses' => 'AuthController@resetpassword',
    ]);
    // need authentication
    $api->group(['middleware' => 'auth'], function ($api) {
        // logout
        $api->delete('logout', [
            'as' => 'authorizations.logout',
            'uses' => 'AuthController@destroy',
        ]);
        $api->delete('authorizations/current', [
            'as' => 'authorizations.logout',
            'uses' => 'AuthController@destroy',
        ]);

        // userShow
        $api->get('user', [
            'as' => 'user.show',
            'uses' => 'UserController@userShow',
        ]);
        // userShow
        $api->get('user/teste', [
            'as' => 'user.teste',
            'uses' => 'UserController@teste',
        ]);
        // update profile
        $api->put('user/profile', [
            'as' => 'user.profile.update',
            'uses' => 'UserController@updateProfile',
        ]);

        // update my password
        $api->put('user/password', [
            'as' => 'user.password.update',
            'uses' => 'UserController@updatePassword',
        ]);

        // USER
        // user list
        $api->get('users', [
            'as' => 'users.index',
            'uses' => 'UserController@index',
        ]);
        // User add
        $api->post('users', [
            'as' => 'users.store',
            'uses' => 'UserController@store',
        ]);

        // user detail
        $api->get('users/{id}', [
            'as' => 'users.show',
            'uses' => 'UserController@show',
        ]);
        // user update
        $api->put('users/{id}', [
            'as' => 'users.update',
            'uses' => 'UserController@update',
        ]);
        // user delete
        $api->delete('users/{id}', [
            'as' => 'users.destroy',
            'uses' => 'UserController@destroy',
        ]);
        //EVENT
        // list EVENT
        $api->get('event', [
            'as' => 'event.index',
            'uses' => 'EventController@index',
        ]);
        // list upcoming EVENT
        $api->get('event/upcoming/', [
            'as' => 'event.upcoming',
            'uses' => 'EventController@upcoming',
        ]);
        // event detail
        $api->get('event/search/', [
            'as' => 'event.search',
            'uses' => 'EventController@search',
        ]);
        $api->post('eventupload', [
            'as' => 'event.upload',
            'uses' => 'EventController@upload',
        ]);
        //create EVENT
        $api->post('event', [
            'as' => 'event.store',
            'uses' => 'EventController@store',
        ]);
        // event detail
        $api->get('event/{id}', [
            'as' => 'event.show',
            'uses' => 'EventController@show',
        ]);
          // event detail
          $api->get('eventstages/{id}/', [
            'as' => 'event.stages',
            'uses' => 'EventController@stages',
        ]);
        // update a EVENT
        $api->put('event/{id}', [
            'as' => 'event.update',
            'uses' => 'EventController@update',
        ]);
        // delete a post
        $api->delete('event/{id}', [
            'as' => 'event.destroy',
            'uses' => 'EventController@destroy',
        ]);

        //promoter
        // list promoter
        $api->get('promoter', [
            'as' => 'promoter.index',
            'uses' => 'PromoterController@index',
        ]);
        // promoter detail
        $api->get('promoter/search/', [
            'as' => 'promoter.search',
            'uses' => 'PromoterController@search',
        ]);
        //create promoter
        $api->post('promoter', [
            'as' => 'promoter.store',
            'uses' => 'PromoterController@store',
        ]);
        // promoter detail
        $api->get('promoter/{id}', [
            'as' => 'promoter.show',
            'uses' => 'PromoterController@show',
        ]);
        // promoter all detail
        $api->get('promoter/{id}/user', [
            'as' => 'promoter.user',
            'uses' => 'PromoterController@users',
        ]);
        // update a promoter
        $api->put('promoter/{id}', [
            'as' => 'promoter.update',
            'uses' => 'PromoterController@update',
        ]);
        // delete a post
        $api->delete('promoter/{id}', [
            'as' => 'promoter.destroy',
            'uses' => 'PromoterController@destroy',
        ]);
        //production
        // list production
        $api->get('production', [
            'as' => 'production.index',
            'uses' => 'ProductionController@index',
        ]);
        // production detail
        $api->get('production/search/', [
            'as' => 'production.search',
            'uses' => 'ProductionController@search',
        ]);
        //create production
        $api->post('production', [
            'as' => 'production.store',
            'uses' => 'ProductionController@store',
        ]);
        // production detail
        $api->get('production/{id}', [
            'as' => 'production.show',
            'uses' => 'ProductionController@show',
        ]);
        // update a production
        $api->put('production/{id}', [
            'as' => 'production.update',
            'uses' => 'ProductionController@update',
        ]);
        // delete a post
        $api->delete('production/{id}', [
            'as' => 'production.destroy',
            'uses' => 'ProductionController@destroy',
        ]);
       
        //RESTAURANT
        // list RESTAURANT
        $api->get('restaurant', [
            'as' => 'restaurant.index',
            'uses' => 'RestaurantController@index',
        ]);
        // restaurant detail
        $api->get('restaurant/search/', [
            'as' => 'restaurant.search',
            'uses' => 'RestaurantController@search',
        ]);
        //create RESTAURANT
        $api->post('restaurant', [
            'as' => 'restaurant.store',
            'uses' => 'RestaurantController@store',
        ]);
        // restaurant detail
        $api->get('restaurant/{id}', [
            'as' => 'restaurant.show',
            'uses' => 'RestaurantController@show',
        ]);
        // update a RESTAURANT
        $api->put('restaurant/{id}', [
            'as' => 'restaurant.update',
            'uses' => 'RestaurantController@update',
        ]);
        // delete a post
        $api->delete('restaurant/{id}', [
            'as' => 'restaurant.destroy',
            'uses' => 'RestaurantController@destroy',
        ]);
        //HOTEL
        // list HOTEL
        $api->get('hotel', [
            'as' => 'hotel.index',
            'uses' => 'HotelController@index',
        ]);
        // hotel detail
        $api->get('hotel/search/', [
            'as' => 'hotel.search',
            'uses' => 'HotelController@search',
        ]);
        //create HOTEL
        $api->post('hotel', [
            'as' => 'hotel.store',
            'uses' => 'HotelController@store',
        ]);
        // hotel detail
        $api->get('hotel/{id}', [
            'as' => 'hotel.show',
            'uses' => 'HotelController@show',
        ]);
        // update a HOTEL
        $api->put('hotel/{id}', [
            'as' => 'hotel.update',
            'uses' => 'HotelController@update',
        ]);
        // delete a post
        $api->delete('hotel/{id}', [
            'as' => 'hotel.destroy',
            'uses' => 'HotelController@destroy',
        ]);
        //Manager
        // list Manager
        $api->get('manager', [
            'as' => 'manager.index',
            'uses' => 'ManagerController@index',
        ]);
        // manager detail
        $api->get('manager/search/', [
            'as' => 'manager.search',
            'uses' => 'ManagerController@search',
        ]);
        //create Manager
        $api->post('manager', [
            'as' => 'manager.store',
            'uses' => 'ManagerController@store',
        ]);
        // manager detail
        $api->get('manager/{id}', [
            'as' => 'manager.show',
            'uses' => 'ManagerController@show',
        ]);
        // update a Manager
        $api->put('manager/{id}', [
            'as' => 'manager.update',
            'uses' => 'ManagerController@update',
        ]);
        // delete a 
        $api->delete('manager/{id}', [
            'as' => 'manager.destroy',
            'uses' => 'ManagerController@destroy',
        ]);
        //Technician
        // list Technician
        $api->get('technician', [
            'as' => 'technician.index',
            'uses' => 'TechnicianController@index',
        ]);
        // technician detail
        $api->get('technician/search/', [
            'as' => 'technician.search',
            'uses' => 'TechnicianController@search',
        ]);
        //create Technician
        $api->post('technician', [
            'as' => 'technician.store',
            'uses' => 'TechnicianController@store',
        ]);
        // technician detail
        $api->get('technician/{id}', [
            'as' => 'technician.show',
            'uses' => 'TechnicianController@show',
        ]);
        // update a Technician
        $api->put('technician/{id}', [
            'as' => 'technician.update',
            'uses' => 'TechnicianController@update',
        ]);
        // delete a post
        $api->delete('technician/{id}', [
            'as' => 'technician.destroy',
            'uses' => 'TechnicianController@destroy',
        ]);
        //artist
        // list artist
        $api->get('artist', [
            'as' => 'artist.index',
            'uses' => 'ArtistController@index',
        ]);
        // artist detail
        $api->get('artist/search/', [
            'as' => 'artist.search',
            'uses' => 'ArtistController@search',
        ]);
        //create artist
        $api->post('artist', [
            'as' => 'artist.store',
            'uses' => 'ArtistController@store',
        ]);
        // artist detail
        $api->get('artist/{id}', [
            'as' => 'artist.show',
            'uses' => 'ArtistController@show',
        ]);
        // update a artist
        $api->put('artist/{id}', [
            'as' => 'artist.update',
            'uses' => 'ArtistController@update',
        ]);
        // delete a post
        $api->delete('artist/{id}', [
            'as' => 'artist.destroy',
            'uses' => 'ArtistController@destroy',
        ]);
        //stage
        // list stage
        $api->get('stage', [
            'as' => 'stage.index',
            'uses' => 'StageController@index',
        ]);
        // list stage
        $api->get('event/{id}/stage', [
            'as' => 'stage.eventindex',
            'uses' => 'StageController@eventIndex',
        ]);
         // list stage
         $api->get('stage/{id}/information', [
            'as' => 'stage.information',
            'uses' => 'StageController@information',
        ]);
        // list stage
        $api->get('stage/{id}/stafftechnician', [
            'as' => 'stage.stafftechnician',
            'uses' => 'StageController@stafftechnician',
        ]);
        // list stage
        $api->get('stage/{id}/document', [
            'as' => 'stage.document',
            'uses' => 'StageController@document',
        ]);
        // stage detail
        $api->get('stage/search/', [
            'as' => 'stage.search',
            'uses' => 'StageController@search',
        ]);
        //create stage
        $api->post('stage', [
            'as' => 'stage.store',
            'uses' => 'StageController@store',
        ]);
        // stage detail
        $api->get('stage/{id}', [
            'as' => 'stage.show',
            'uses' => 'StageController@show',
        ]);
        // update a stage
        $api->put('stage/{id}', [
            'as' => 'stage.update',
            'uses' => 'StageController@update',
        ]);
        // delete a post
        $api->delete('stage/{id}', [
            'as' => 'stage.destroy',
            'uses' => 'StageController@destroy',
        ]);
        //stagetechnician
        //create stagestafftechnician
        $api->post('stagetechnician', [
            'as' => 'stagetechnician.store',
            'uses' => 'StagetechnicianController@store',
        ]);
        // update a stagestafftechnician
        $api->put('stagetechnician/{id}', [
            'as' => 'stagetechnician.update',
            'uses' => 'StagetechnicianController@update',
        ]);
        // delete a post
        $api->delete('stagetechnician/{id}', [
            'as' => 'stagetechnician.destroy',
            'uses' => 'StagetechnicianController@destroy',
        ]);
        //stageartist
        //create stagestafftechnician
        $api->post('stageartist', [
            'as' => 'stageartist.store',
            'uses' => 'StageartistController@store',
        ]);
        // update a stagestafftechnician
        $api->put('stageartist/{id}', [
            'as' => 'stageartist.update',
            'uses' => 'StageartistController@update',
        ]);
        // delete a post
        $api->delete('stageartist/{id}', [
            'as' => 'stageartist.destroy',
            'uses' => 'StageartistController@destroy',
        ]);
        //schedule
        // list schedule
        $api->get('scheduleartist', [
            'as' => 'scheduleartist.index',
            'uses' => 'ScheduleArtistController@index',
        ]);
        // schedule detail
        $api->get('scheduleartist/search/', [
            'as' => 'schedule.search',
            'uses' => 'ScheduleArtistController@search',
        ]);
        //create schedule
        $api->post('scheduleartist', [
            'as' => 'scheduleartist.store',
            'uses' => 'ScheduleArtistController@store',
        ]);
        // schedule detail
        $api->get('scheduleartist/{id}', [
            'as' => 'scheduleartist.show',
            'uses' => 'ScheduleArtistController@show',
        ]);
        // schedule detail
        $api->get('schedulestageartist/{id}', [
            'as' => 'scheduleartist.stageartist',
            'uses' => 'ScheduleArtistController@stageartist',
        ]);
        // update a schedule
        $api->put('scheduleartist/{id}', [
            'as' => 'scheduleartist.update',
            'uses' => 'ScheduleArtistController@update',
        ]);
        // delete a post
        $api->delete('scheduleartist/{id}', [
            'as' => 'scheduleartist.destroy',
            'uses' => 'ScheduleArtistController@destroy',
        ]);
        //schedule
        // list schedule
        $api->get('scheduletechnician', [
            'as' => 'scheduletechnician.index',
            'uses' => 'ScheduleTechnicianController@index',
        ]);
        // schedule detail
        $api->get('scheduletechnician/search/', [
            'as' => 'schedule.search',
            'uses' => 'ScheduleTechnicianController@search',
        ]);
        //create schedule
        $api->post('scheduletechnician', [
            'as' => 'scheduletechnician.store',
            'uses' => 'ScheduleTechnicianController@store',
        ]);
        // schedule detail
        $api->get('scheduletechnician/{id}', [
            'as' => 'scheduletechnician.show',
            'uses' => 'ScheduleTechnicianController@show',
        ]);
         // schedule detail
        $api->get('schedulestagetechnician/{id}', [
            'as' => 'scheduletechnician.stagetechnician',
            'uses' => 'ScheduleTechnicianController@stagetechnician',
        ]);
        // update a schedule
        $api->put('scheduletechnician/{id}', [
            'as' => 'scheduletechnician.update',
            'uses' => 'ScheduleTechnicianController@update',
        ]);
        // delete a post
        $api->delete('scheduletechnician/{id}', [
            'as' => 'scheduletechnician.destroy',
            'uses' => 'ScheduleTechnicianController@destroy',
        ]);
        //stage and tehcnician
        $api->get('scheduletechnician/stage/{stage}/technician/{technician}', [
            'as' => 'scheduletechnician.stagetechnician',
            'uses' => 'ScheduleTechnicianController@stagetechnician',
        ]);
        //stagedocument
        // list stagedocument
        $api->get('stagedocument', [
            'as' => 'stagedocument.index',
            'uses' => 'StagedocumentController@index',
        ]);
// stagedocument detail
        $api->get('stagedocument/search/', [
            'as' => 'stagedocument.search',
            'uses' => 'StagedocumentController@search',
        ]);
//create stagedocument
        $api->post('stagedocument', [
            'as' => 'stagedocument.store',
            'uses' => 'StagedocumentController@store',
        ]);
// stagedocument detail
        $api->get('stagedocument/{id}', [
            'as' => 'stagedocument.show',
            'uses' => 'StagedocumentController@show',
        ]);
// update a stagedocument
        $api->put('stagedocument/{id}', [
            'as' => 'stagedocument.update',
            'uses' => 'StagedocumentController@update',
        ]);
// delete a post
        $api->delete('stagedocument/{id}', [
            'as' => 'stagedocument.destroy',
            'uses' => 'StagedocumentController@destroy',
        ]);
        $api->post('stagedocumentupload', [
            'as' => 'stagedocument.upload',
            'uses' => 'StagedocumentController@upload',
        ]);
        //post
         $api->post('stage/{id}/document', [
            'as' => 'stagedocument.post',
            'uses' => 'StagedocumentController@post',
        ]); 
//stagedocumentcomment
        // list stagedocumentcomment
        $api->get('stagedocumentcomment', [
            'as' => 'stagedocumentcomment.index',
            'uses' => 'StagedocumentcommentController@index',
        ]);
// stagedocumentcomment detail
        $api->get('stagedocumentcomment/search/', [
            'as' => 'stagedocumentcomment.search',
            'uses' => 'StagedocumentcommentController@search',
        ]);
//create stagedocumentcomment
        $api->post('stagedocumentcomment', [
            'as' => 'stagedocumentcomment.store',
            'uses' => 'StagedocumentcommentController@store',
        ]);
// stagedocumentcomment detail
        $api->get('stagedocumentcomment/{id}', [
            'as' => 'stagedocumentcomment.show',
            'uses' => 'StagedocumentcommentController@show',
        ]);
// update a stagedocumentcomment
        $api->put('stagedocumentcomment/{id}', [
            'as' => 'stagedocumentcomment.update',
            'uses' => 'StagedocumentcommentController@update',
        ]);
// delete a post
        $api->delete('stagedocumentcomment/{id}', [
            'as' => 'stagedocumentcomment.destroy',
            'uses' => 'StagedocumentcommentController@destroy',
        ]);
        $api->get('event/roadbook/{id}', [
            'as' => 'event.roadbook',
            'uses' => 'EventController@roadbook',
        ]);
   
    });
     $api->get('stage/roadbook/{id}', [
            'as' => 'stage.roadbook',
            'uses' => 'StageController@roadbook',
        ]);
});
