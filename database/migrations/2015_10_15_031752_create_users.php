<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsers extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50)->index();
            $table->string('email')->unique()->index();
            $table->string('birthdate', 50)->nullable();
            $table->string('recover',100)->nullable();
            $table->binary('avatar');
            $table->string('password');
            $table->enum('role', ['superadmin','admin','adminproduction','production','technician','manager','promoter','artist','user'])->default('user');
            $table->tinyInteger('active')->default(0);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('user');
    }
}
