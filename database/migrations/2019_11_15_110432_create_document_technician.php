<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentTechnician extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       
        Schema::create('document_technician', function (Blueprint $table) {
            $table->increments('id_document_technician');
            $table->string('name', 100);
            $table->integer('status');

			$table->integer('id_stage_technician')->unsigned()->nullable();
            $table->foreign('id_stage_technician')->references('id_stage_technician')->on('stage_technician');
          	$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('document_technician', function (Blueprint $table) {
            //
        });
		Schema::dropIfExists('document_technician');
    }
}
