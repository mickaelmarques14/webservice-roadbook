<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('event', function (Blueprint $table) {
            $table->increments('id_event');
            $table->string('name', 100);
			$table->dateTime('date_begin')->nullable();
			$table->dateTime('date_end')->nullable();
			$table->text('description')->nullable();
			$table->enum('status', [0, 1])->default(0)->comment('0 => draft, 1 => public');
			$table->string('website',50)->nullable();
			$table->string('facebook',50)->nullable();
			$table->string('instagram',50)->nullable();
			$table->string('twitter',50)->nullable();
			$table->string('youtube',50)->nullable();
			$table->string('latitude',20)->nullable();
			$table->string('longitude',20)->nullable();
			$table->string('address',50)->nullable();
			$table->string('cover',50)->nullable();
			$table->integer('id_promoter')->unsigned()->nullable();
			$table->foreign('id_promoter')->references('id_promoter')->on('promoter');
			$table->integer('id_production')->unsigned()->nullable();
			$table->foreign('id_production')->references('id_production')->on('production');
          	$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		 //  Schema::dropForeign('event_id_roadbook_foreign');
		  Schema::dropIfExists('event');
    }
}
