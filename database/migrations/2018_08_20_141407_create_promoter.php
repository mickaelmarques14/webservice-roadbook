<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromoter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('promoter', function (Blueprint $table) {
            $table->increments('id_promoter');
            $table->string('name', 100);
            $table->string('email', 50)->nullable();
            $table->string('contact', 20)->nullable();
			$table->string('address', 50)->nullable();
			$table->string('latitude', 20)->nullable();
            $table->string('longitude', 20)->nullable();
            $table->integer('id_user')->unsigned()->nullable();
            $table->foreign('id_user')->references('id')->on('user');
          	$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		  Schema::dropIfExists('promoter');
    }
}
