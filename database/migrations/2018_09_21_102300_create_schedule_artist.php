<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScheduleArtist extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
         Schema::create('schedule_artist', function (Blueprint $table) {
            $table->increments('id_schedule_artist');
            $table->string('name', 100);
			$table->dateTime('date_begin')->nullable();
            $table->dateTime('date_end')->nullable();
            $table->string('comment', 100)->nullable();
            //$table->integer('type')->unsigned()->nullable();
            $table->integer('twin')->unsigned()->nullable();
            $table->integer('double')->unsigned()->nullable();
            $table->integer('single')->unsigned()->nullable();
          //$table->dateTime('date_setup')->nullable();
            //$table->enum('status', ['pending','valid','invalid'])->default('pending');
            $table->integer('id_stage_artist')->unsigned()->nullable();
            $table->foreign('id_stage_artist')->references('id_stage_artist')->on('stage_artist');
            $table->integer('id_hotel')->unsigned()->nullable();
			$table->foreign('id_hotel')->references('id_hotel')->on('hotel');
			$table->integer('id_restaurant')->unsigned()->nullable();
			$table->foreign('id_restaurant')->references('id_restaurant')->on('restaurant');
          	$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		  Schema::dropIfExists('schedule_artist');
    }
}
