<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtist extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       
        Schema::create('artist', function (Blueprint $table) {
            $table->increments('id_artist');
            $table->string('name', 100);
            $table->string('email',50);
            $table->string('contact',20)->nullable();
            $table->integer('id_manager')->unsigned()->nullable();
            $table->foreign('id_manager')->references('id_manager')->on('manager');
            $table->integer('id_user')->unsigned()->nullable();
            $table->foreign('id_user')->references('id')->on('user');
          	$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artist', function (Blueprint $table) {
            //
        });
		Schema::dropIfExists('artist');
    }
}
