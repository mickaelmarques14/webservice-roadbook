<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('stage', function (Blueprint $table) {
            $table->increments('id_stage');
            $table->string('name', 100);
			$table->string('picture', 25)->nullable();
			$table->string('width', 10)->nullable();
			$table->string('length', 10)->nullable();
			$table->string('description',100)->nullable();
			$table->integer('id_event')->unsigned()->nullable();
            $table->foreign('id_event')->references('id_event')->on('event');
          	$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		 //  Schema::dropForeign('stage_id_roadbook_foreign');
		  Schema::dropIfExists('stage');
    }
}
