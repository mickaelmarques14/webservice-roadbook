<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionMember extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       
        Schema::create('production_member', function (Blueprint $table) {
            $table->increments('id_production_member');
			$table->integer('id_production')->unsigned()->nullable();
            $table->foreign('id_production')->references('id_production')->on('production');
            $table->integer('id_user')->unsigned()->nullable();
            $table->foreign('id_user')->references('id')->on('user');
            $table->string('name', 100);
            $table->string('contact',50)->nullable();
            $table->string('email',50)->nullable();
          	$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('schedulesetup', function (Blueprint $table) {
            //
        });
		Schema::dropIfExists('schedulesetup');
    }
}
