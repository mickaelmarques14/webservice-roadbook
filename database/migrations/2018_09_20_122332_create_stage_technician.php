<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStageTechnician extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stage_technician', function (Blueprint $table) {
            $table->increments('id_stage_technician');
            $table->integer('id_stage')->unsigned()->nullable();
            $table->foreign('id_stage')->references('id_stage')->on('stage');
            $table->integer('id_technician')->unsigned()->nullable();
            $table->foreign('id_technician')->references('id_technician')->on('technician');
            $table->string('type',50)->nullable();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stage_technician');
    }
}
