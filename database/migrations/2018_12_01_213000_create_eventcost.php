<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventcost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       
        Schema::create('eventcost', function (Blueprint $table) {
            $table->increments('id_eventcost');
            $table->string('name', 50);
            $table->decimal('amount', 10, 2);
            $table->decimal('cost', 10, 2);
            $table->integer('id_event')->unsigned()->nullable();
            $table->foreign('id_event')->references('id_event')->on('event');
          	$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('eventcost', function (Blueprint $table) {
            //
        });
		Schema::dropIfExists('eventcost');
    }
}
