<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStageArtist extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
         Schema::create('stage_artist', function (Blueprint $table) {
            $table->increments('id_stage_artist');
			$table->integer('id_stage')->unsigned()->nullable();
            $table->integer('id_artist')->unsigned()->nullable();
            $table->foreign('id_stage')->references('id_stage')->on('stage');
            $table->foreign('id_artist')->references('id_artist')->on('artist');
          	$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		  Schema::dropIfExists('stage_artist');
    }
}
