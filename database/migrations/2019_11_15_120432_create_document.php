<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentArtist extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       
        Schema::create('document_artist', function (Blueprint $table) {
            $table->increments('id_document_artist');
            $table->string('name', 100);
            $table->integer('status');        
            $table->integer('id_stage_artist')->unsigned()->nullable();
            $table->foreign('id_stage_artist')->references('id_stage_artist')->on('stage_artist');
          	$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('document_artist', function (Blueprint $table) {
            //
        });
		Schema::dropIfExists('document_artist');
    }
}
