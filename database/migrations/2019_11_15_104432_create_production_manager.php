<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionManager extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       
        Schema::create('production_manager', function (Blueprint $table) {
            $table->increments('id_production_manager');
			$table->integer('id_production')->unsigned()->nullable();
            $table->foreign('id_production')->references('id_production')->on('production');
            $table->integer('id_manager')->unsigned()->nullable();
            $table->foreign('id_manager')->references('id_manager')->on('manager');
          	$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('production_manager', function (Blueprint $table) {
            //
        });
		Schema::dropIfExists('production_manager');
    }
}
