<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionTechnician extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       
        Schema::create('production_technician', function (Blueprint $table) {
            $table->increments('id_production_technician');
			$table->integer('id_production')->unsigned()->nullable();
            $table->foreign('id_production')->references('id_production')->on('production');
            $table->integer('id_technician')->unsigned()->nullable();
            $table->foreign('id_technician')->references('id_technician')->on('technician');
          	$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('production_technician', function (Blueprint $table) {
            //
        });
		Schema::dropIfExists('production_technician');
    }
}
