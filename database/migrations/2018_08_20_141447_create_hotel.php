<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('hotel', function (Blueprint $table) {
            $table->increments('id_hotel');
            $table->string('name', 100);
			$table->string('address',50)->nullable();
			$table->string('latitude',20)->nullable();
			$table->string('longitude',20)->nullable();
			$table->string('website',40)->nullable();
			$table->string('facebook',100)->nullable();
			$table->string('contact',10)->nullable();
            $table->string('email',50)->nullable();
            $table->float('rating', 2, 1)->nullable();
          	$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		  Schema::dropIfExists('hotel');
    }
}
