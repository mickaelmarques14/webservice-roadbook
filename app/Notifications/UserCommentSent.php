<?php
namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class UserCommentSent extends Notification
{
    use Queueable;
/** @var Loan */
    public $loan;
/**
 * @param Loan $loan
 */
    public function __construct($loan)
    {
        $this->loan = $loan;
    }
/**
 * Get the notification's delivery channels.
 *
 * @param mixed $notifiable
 * @return array
 */
    public function via($notifiable)
    {
        return ['mail'];
    }
/**
 * Get the mail representation of the notification.
 *
 * @param mixed $notifiable
 * @return \Illuminate\Notifications\Messages\MailMessage
 */
    public function toMail($notifiable)
    {
        $subject = (isset($this->loan['subject'])) ? $this->loan['subject'] : trans('email.new_comment_subject');
        $body = trans('email.new_comment_body',['user' => $this->loan['user'],'status' =>  $this->loan['status'],'message' =>  $this->loan['message']]);
        return (new MailMessage)
            ->subject($subject)
            ->line($body)
            ->action('Url', env('FRONTEND_URL'));
//            ->line(trans('email.footer'));
    }
}
