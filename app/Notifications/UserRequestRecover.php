<?php
namespace App\Notifications;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
class UserRequestRecover extends Notification
{
 use Queueable;
/** @var Loan */
 public $loan;
/**
 * @param Loan $loan
 */
 public function __construct($loan)
 {
	$this->loan = $loan;
 }
/**
 * Get the notification's delivery channels.
 *
 * @param mixed $notifiable
 * @return array
 */
 public function via($notifiable)
 {
	return ['mail'];
 }
/**
 * Get the mail representation of the notification.
 *
 * @param mixed $notifiable
 * @return \Illuminate\Notifications\Messages\MailMessage
 */
 public function toMail($notifiable)
 {
	 //use templates => using blade needs .blade.php | html use .php
	/*return (new MailMessage)
	->subject('Loan Application Submitted!')
	->markdown('mails.sent', [
	'loan' => $this->loan
	]);*/
	$subject = (isset($this->loan['subject'])) ? $this->loan['subject'] : 'Recover password';
	$body = (isset($this->loan['body'])) ? $this->loan['body'] : 'Reset password';
	return (new MailMessage)
	->subject($subject)
	->line($body)
    ->action('Url', env('FRONTEND_URL').'recover/'.$this->loan['key'])
    ->line(trans('email.footer'));
 }
}