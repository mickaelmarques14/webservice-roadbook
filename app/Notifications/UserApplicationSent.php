<?php
namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class UserApplicationSent extends Notification
{
    use Queueable;
/** @var Loan */
    public $loan;
/**
 * @param Loan $loan
 */
    public function __construct($loan)
    {
        $this->loan = $loan;
    }
/**
 * Get the notification's delivery channels.
 *
 * @param mixed $notifiable
 * @return array
 */
    public function via($notifiable)
    {
        return ['mail'];
    }
/**
 * Get the mail representation of the notification.
 *
 * @param mixed $notifiable
 * @return \Illuminate\Notifications\Messages\MailMessage
 */
    public function toMail($notifiable)
    {
        //use templates => using blade needs .blade.php | html use .php
        /*return (new MailMessage)
        ->subject('Loan Application Submitted!')
        ->markdown('mails.sent', [
        'loan' => $this->loan
        ]);*/
        $subject = (isset($this->loan['subject'])) ? $this->loan['subject'] : trans('email.new_account_subject');
        $body = trans('email.new_account_body',['password' =>  $this->loan['password']]);
        return (new MailMessage)
            ->subject($subject)
            ->line($body)
            ->action('Url', env('FRONTEND_URL'));
//            ->line(trans('email.footer'));
    }
}
