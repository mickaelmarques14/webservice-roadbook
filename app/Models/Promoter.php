<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Promoter extends BaseModel
{
	protected $table = 'promoter';
	protected $primaryKey = 'id_promoter';
	
    use SoftDeletes;
	
	public function scopeCustomLike($query, $params){
        foreach ($params as $field => $value)
			$query->where($field, 'LIKE', "%$value%");
		return $query;
	}
	public function scopeUserLeftJoin($query){
		return $query->leftJoin('user', 'promoter.id_user', '=', 'user.id');
	}
	public function user(){
		return $this->belongsTo(User::class, 'id_user','id');
	}
}
