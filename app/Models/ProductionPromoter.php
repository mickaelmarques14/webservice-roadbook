<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class ProductionPromoter extends BaseModel
{
	protected $table = 'production_promoter';
	protected $primaryKey = 'id_production_promoter';
	
    use SoftDeletes;
	
	public function scopeCustomLike($query, $params){
        foreach ($params as $field => $value)
			$query->where($field, 'LIKE', "%$value%");
		return $query;
	}

	public function production(){
		return $this->belongsTo(Production::class, 'id_production','id_production');
	}
	public function promoter(){
		return $this->belongsTo(Promoter::class, 'id_promoter','id_promoter');
	}
}
