<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Hotel extends BaseModel
{
	protected $table = 'hotel';
	protected $primaryKey = 'id_hotel';
	
    use SoftDeletes;
	
	public function scopeCustomLike($query, $params){
        foreach ($params as $field => $value)
			$query->where($field, 'LIKE', "%$value%");
		return $query;
	}
	public function schedulehotel(){
        return $this->hasMany(Schedulehotel::class, 'id_hotel','id_hotel');
    }
}
