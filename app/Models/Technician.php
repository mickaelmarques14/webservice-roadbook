<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Technician extends BaseModel
{
	protected $table = 'technician';
	protected $primaryKey = 'id_technician';
    use SoftDeletes;
	
	public function scopeCustomLike($query, $params){
        foreach ($params as $field => $value)
			$query->where($field, 'LIKE', "%$value%");
		return $query;
	}
	public function stage(){
		return $this->hasMany(Stage::class, 'id_technician','id_technician');
	}
	public function getSchedules($stage,$technician) {
		$stageconection = StageTechnician::find(['id_technician'=>$technician,'id_stage'=>$stage]);
		$return = [];
		foreach($stageconection as $value) {
			$return = ScheduleTechnician::where(['id_stage_technician'=>$value->id_stage_technician])->get();
		}
		return $return;
	}

}
