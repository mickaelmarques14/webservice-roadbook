<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class ScheduleArtist extends BaseModel
{
	protected $table = 'schedule_artist';
	protected $primaryKey = 'id_schedule_artist';
	
    use SoftDeletes;
	
	public function stage(){
		return $this->belongsTo(Stage::class, 'id_stage','id_stage');
	}
	public function artist(){
		return $this->belongsTo(Artist::class, 'id_artist','id_artist');
	}
	public function hotel(){
		return $this->belongsTo(Hotel::class, 'id_hotel','id_hotel');
	}
	public function restaurant(){
		return $this->belongsTo(Restaurant::class, 'id_restaurant','id_restaurant');
	}
}
