<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class ProductionManager extends BaseModel
{
	protected $table = 'production_manager';
	protected $primaryKey = 'id_production_manager';
	
    use SoftDeletes;
	
	public function scopeCustomLike($query, $params){
        foreach ($params as $field => $value)
			$query->where($field, 'LIKE', "%$value%");
		return $query;
	}

	public function production(){
		return $this->belongsTo(Production::class, 'id_production','id_production');
	}
	public function manager(){
		return $this->belongsTo(Manager::class, 'id_manager','id_manager');
	}
}
