<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Stagedocumentcomment extends BaseModel
{
	protected $table = 'stagedocumentcomment';
	protected $primaryKey = 'id_stagedocumentcomment';
	
    use SoftDeletes;
	protected $enumStatuses = [
        'pending',
        'valid',
        'invalid',
    ];

	public function scopeCustomLike($query, $params){
        foreach ($params as $field => $value)
			$query->where($field, 'LIKE', "%$value%");
		return $query;
	}
	public function stagedocument(){
		return $this->belongsTo(Stagedocument::class, 'id_stagedocument','id_stagedocument');
	}
}
