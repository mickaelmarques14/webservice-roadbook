<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class ProductionArtist extends BaseModel
{
	protected $table = 'production_artist';
	protected $primaryKey = 'id_production_artist';
	
    use SoftDeletes;
	
	public function scopeCustomLike($query, $params){
        foreach ($params as $field => $value)
			$query->where($field, 'LIKE', "%$value%");
		return $query;
	}
	public function production(){
		return $this->belongsTo(Production::class, 'id_production','id_production');
	}
	public function artist(){
		return $this->belongsTo(Artist::class, 'id_artist','id_artist');
	}
}
