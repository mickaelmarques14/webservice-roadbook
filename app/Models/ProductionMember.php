<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class ProductionMember extends BaseModel
{
	protected $table = 'production_member';
	protected $primaryKey = 'id_production_member';
	
    use SoftDeletes;
	
	public function scopeCustomLike($query, $params){
        foreach ($params as $field => $value)
			$query->where($field, 'LIKE', "%$value%");
		return $query;
	}
	public function scopeProductionJoin($query) {
		return $query->join('production', 'production.id_production', '=', 'production_member.id_production');
	}
	public function scopeUserLeftJoin($query) {
		return $query->leftJoin('user', 'production_member.id_user', '=', 'user.id');
	}
	public function production(){
		return $this->belongsTo(Production::class, 'id_production','id_production');
	}
	public function user(){
		return $this->belongsTo(User::class, 'id_user','id');
	}
}
