<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Manager extends BaseModel
{
	protected $table = 'manager';
	protected $primaryKey = 'id_manager';
	
    use SoftDeletes;
	
	public function scopeCustomLike($query, $params){
        foreach ($params as $field => $value)
			$query->where($field, 'LIKE', "%$value%");
		return $query;
	}
	public function user(){
		return $this->belongsTo(User::class, 'id_user','id');
	}
	public function artist(){
        return $this->hasMany(Artist::class, 'id_artist','id_artist');
	}
	public function production_manager(){
        return $this->hasMany(ProductionManager::class, 'id_manager','id_manager');
    }
}
