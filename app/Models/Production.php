<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Production extends BaseModel
{
	protected $table = 'production';
	protected $primaryKey = 'id_production';
	
    use SoftDeletes;
	
	public function scopeCustomLike($query, $params){
        foreach ($params as $field => $value)
			$query->where($field, 'LIKE', "%$value%");
		return $query;
	}
	public function productionMember(){
		return $this->hasMany(ProductionMember::class, 'id_production','id_production');
	}
}
