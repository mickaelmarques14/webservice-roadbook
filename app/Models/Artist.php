<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Artist extends BaseModel
{
	protected $table = 'artist';
	protected $primaryKey = 'id_artist';
	protected $fillable = ['id_user', 'name', 'contact', 'email','id_manager'];

    use SoftDeletes;
	
	public function scopeCustomLike($query, $params){
        foreach ($params as $field => $value)
			$query->where($field, 'LIKE', "%$value%");
		return $query;
	}
	public function scopeManagerLeftJoin($query){
		return $query->leftJoin('manager', 'artist.id_manager', '=', 'manager.id_manager');
	}
	public function manager(){
        return $this->belongsTo(Manager::class, 'id_manager','id_manager');
    }
	public function schedule(){
        return $this->hasMany(Schedule::class, 'id_artist','id_artist');
    }
	public function schedulehotel(){
        return $this->hasMany(Schedulehotel::class, 'id_artist','id_artist');
    }
	public function scheduleperformance(){
        return $this->hasMany(Scheduleperformance::class, 'id_artist','id_artist');
    }
	public function schedulerestaurant(){
        return $this->hasMany(Schedulerestaurant::class, 'id_artist','id_artist');
    }
	public function schedulesetup(){
        return $this->hasMany(Schedulesetup::class, 'id_artist','id_artist');
	}
	public function user(){
		return $this->belongsTo(User::class, 'id_user','id');
	}
	public function getSchedules($stage,$artist) {
		$stageconection = StageArtist::find(['id_artist'=>$artist,'id_stage'=>$stage]);
		$return = [];
		foreach($stageconection as $value) {
			$return = ScheduleArtist::where(['id_stage_artist'=>$value->id_stage_artist])->get();
		}
		return $return;
	}

}
