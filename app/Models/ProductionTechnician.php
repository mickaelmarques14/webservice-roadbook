<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class ProductionTechnician extends BaseModel
{
	protected $table = 'production_technician';
	protected $primaryKey = 'id_production_technician';
	
    use SoftDeletes;
	
	public function scopeCustomLike($query, $params){
        foreach ($params as $field => $value)
			$query->where($field, 'LIKE', "%$value%");
		return $query;
	}
	public function production(){
		return $this->belongsTo(Production::class, 'id_production','id_production');
	}
	public function technician(){
		return $this->belongsTo(Technician::class, 'id_technician','id_technician');
	}
}
