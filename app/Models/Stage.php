<?php

namespace App\Models;

use App\Transformers\StageTechnicianTransformer;
use App\Transformers\StageArtistTransformer;
use Illuminate\Database\Eloquent\SoftDeletes;

class Stage extends BaseModel
{
    protected $table = 'stage';
    protected $primaryKey = 'id_stage';

    use SoftDeletes;

    public function scopeEventJoin($query)
    {
        return $query->join('event', 'stage.id_event', '=', 'event.id_event');
    }
    public function scopeCustomLike($query, $params)
    {
        foreach ($params as $field => $value) {
            $query->where($field, 'LIKE', "%$value%");
        }

        return $query;
    }
    public function scopeTechnicianLeftJoin($query)
    {
        return $query->leftJoin('technician', 'stage.id_technician', '=', 'technician.id_technician');
    }
    public function scopeSchedulePerformance($query)
    {
        return $query->join('scheduleperformance', 'stage.id_stage', '=', 'scheduleperformance.id_stage')
            ->join('artist', 'artist.id_artist', '=', 'scheduleperformance.id_artist')
            ->orderBy('scheduleperformance.date_begin', 'asc');
    }
    public function technician()
    {
        return $this->hasManyThrough(Technician::class, StageTechnician::class, 'id_stage', 'id_technician', 'id_stage', 'id_technician');
    }

    public function artist()
    {
        return $this->hasManyThrough(Artist::class, StageArtist::class, 'id_stage', 'id_artist', 'id_stage', 'id_artist');
    }
    public function event()
    {
        return $this->belongsTo(Event::class, 'id_event', 'id_event');
    }
    public function schedule_techinician()
    {
        $object = [];
        $techs = StageTechnician::where('id_stage', $this->id_stage)->get();
        //$techs = $this->hasMany(StageTechnician::class, 'id_stage','id_stage');
        foreach ($techs as $tech) {
			$staget = StageTechnician::find($tech['id_stage_technician']);
		
            if (isset($staget)) {
                $fractal = new \League\Fractal\Manager();
                $fractal->setSerializer(new \League\Fractal\Serializer\DataArraySerializer());
                $fractal->parseIncludes(['restaurant', 'hotel']);
                $response = new \League\Fractal\Resource\Item($staget, new StageTechnicianTransformer());
                $object[] = $fractal->createData($response)->toArray();
            }
        }
        return $object;
    }
    public function schedule_artist()
    {
        $object = [];
        $techs = StageArtist::where('id_stage', $this->id_stage)->get();
        //$techs = $this->hasMany(StageArtist::class, 'id_stage','id_stage');
        foreach ($techs as $tech) {
            $staget = StageArtist::find($tech['id_stage_artist']);
            if (isset($staget)) {
                $fractal = new \League\Fractal\Manager();
                $fractal->setSerializer(new \League\Fractal\Serializer\DataArraySerializer());
                $fractal->parseIncludes(['restaurant', 'hotel']);
                $response = new \League\Fractal\Resource\Item($staget, new StageArtistTransformer());
                $object[] = $fractal->createData($response)->toArray();
            }
        }
        return $object;
    }
    public function scheduletecnician()
    {
        return $this->hasMany(ScheduleTechnician::class, 'id_stage', 'id_stage');
    }
    public function stagedocument()
    {
        return $this->hasMany(Stagedocument::class, 'id_stage', 'id_stage');
    }
}
