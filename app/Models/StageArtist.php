<?php

namespace App\Models;
use App\Transformers\HotelTransformer;
use App\Transformers\RestaurantTransformer;
use Illuminate\Database\Eloquent\SoftDeletes;

class StageArtist extends BaseModel
{
    protected $table = 'stage_artist';
    protected $primaryKey = 'id_stage_artist';

   
    use SoftDeletes;

    public function stage(){
        return $this->hasMany(Stage::class, 'id_stage','id_stage');
    }
    public function artist(){
        return $this->hasMany(Artist::class, 'id_artist','id_artist');
    }
    public function hotel(){
        return $this->hasOne(Hotel::class, 'id_hotel','id_hotel');
    }
    public function restaurant(){
        return $this->hasOne(Restaurant::class, 'id_restaurant','id_restaurant');
    }
    
    public function scheduleartist()
    {
        //  $object = $this->id_stage_artist;
        //   return $object;
        $schedules = [];
        //    foreach ($object as $value) {
        $schedules = ScheduleArtist::where('id_stage_artist', $this->id_stage_artist)->orderBy('date_begin', 'asc')->get();
        foreach ($schedules as $schedule) {
            $restaurant = null;
            $hotel = null;
            if ($schedule->id_restaurant) {
                $restaurant = Restaurant::find($schedule->id_restaurant);
                if (isset($restaurant)) {
                    $fractal = new \League\Fractal\Manager();
                    $fractal->setSerializer(new \League\Fractal\Serializer\DataArraySerializer());
                    $response = new \League\Fractal\Resource\Item($restaurant, new RestaurantTransformer());
                    $schedule['restaurant'] = $fractal->createData($response)->toArray();
                }
            }
            if ($schedule->id_hotel) {
                $hotel = Hotel::find($schedule->id_hotel);
                if (isset($hotel)) {
                    $fractal = new \League\Fractal\Manager();
                    $fractal->setSerializer(new \League\Fractal\Serializer\DataArraySerializer());
                    $response = new \League\Fractal\Resource\Item($hotel, new HotelTransformer());
                    $schedule['hotel'] = $fractal->createData($response)->toArray();
                }
            }
        }
        //    }
        return $schedules;
    }
}
