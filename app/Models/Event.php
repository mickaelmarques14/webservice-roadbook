<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends BaseModel
{
	protected $table = 'event';
	protected $primaryKey = 'id_event';
	
    use SoftDeletes;

	public function scopePromoterLeftJoin($query){
		return $query->leftJoin('promoter', 'event.id_promoter', '=', 'promoter.id_promoter');
	}
	public function scopeProductionLeftJoin($query){
		return $query->leftJoin('production', 'event.id_production', '=', 'production.id_production');
	}
	public function scopeCustomLike($query, $params){
        foreach ($params as $field => $value)
			$query->where($field, 'LIKE', "%$value%");
		return $query;
	}
	public function stage(){
		return $this->hasMany(Stage::class, 'id_event','id_event');
	}
	public function production() {
		return $this->belongsTo(Production::class, 'id_production','id_production');
	}
	public function promoter() {
		return $this->belongsTo(Promoter::class, 'id_promoter','id_promoter');
	}
/*public static function boot()
{
public $timestamps = false;
parent::boot();
 
static::creating(function($model) {
$dt = new DateTime;
$model->created_at = $dt->format('m-d-y H:i:s');
return true;
});
 
static::updating(function($model) {
$dt = new DateTime;
$model->updated_at = $dt->format('m-d-y H:i:s');
return true;
});
}
*/
}
