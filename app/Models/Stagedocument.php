<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Stagedocument extends BaseModel
{
	protected $table = 'stagedocument';
	protected $primaryKey = 'id_stagedocument';
	
    use SoftDeletes;
	

	public function scopeCustomLike($query, $params){
        foreach ($params as $field => $value)
			$query->where($field, 'LIKE', "%$value%");
		return $query;
	}
	public function stage(){
		return $this->belongsTo(Stage::class, 'id_stage','id_stage');
	}
	public function getstatus($id) {
	return Stagedocumentcomment::where('id_stagedocument',$id)->
		whereIn('id_stagedocumentcomment', function($query) {
			$query->selectRaw('MAX(id_stagedocumentcomment)')->from('stagedocumentcomment')->whereNull('deleted_at')->groupBy('id_user');
		})->get(['status'])->toArray();
	
	}

}
