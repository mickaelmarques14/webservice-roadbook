<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class ScheduleTechnician extends BaseModel
{
	protected $table = 'schedule_technician';
	protected $primaryKey = 'id_schedule_technician';
	
    use SoftDeletes;
	
	public function technician(){
		return $this->belongsTo(Technician::class, 'id_technician','id_technician');
	}
	public function artist(){
		return $this->belongsTo(Artist::class, 'id_artist','id_artist');
	}
	public function hotel(){
		return $this->belongsTo(Hotel::class, 'id_hotel','id_hotel');
	}
	public function restaurant(){
		return $this->belongsTo(Restaurant::class, 'id_restaurant','id_restaurant');
	}
}
