<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Restaurant extends BaseModel
{
	protected $table = 'restaurant';
	protected $primaryKey = 'id_restaurant';
	
    use SoftDeletes;
	
	public function scopeCustomLike($query, $params){
        foreach ($params as $field => $value)
			$query->where($field, 'LIKE', "%$value%");
		return $query;
	}
	public function schedulrestaurant(){
        return $this->hasMany(Schedulerestaurant::class, 'id_restaurant','id_restaurant');
    }
}
