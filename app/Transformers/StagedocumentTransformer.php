<?php

namespace App\Transformers;

use App\Models\Stagedocument;
use League\Fractal\ParamBag;
use League\Fractal\TransformerAbstract;


class StagedocumentTransformer extends TransformerAbstract
{
    protected $defaultIncludes = ['stagedocumentcomment'];
    public function transform(Stagedocument $stagedocument)
    {
        $attributesToArray = $stagedocument->attributesToArray();
        $attributesToArray['path'] = ($stagedocument->file ? url('files/'.$stagedocument->file) : '');
        $attributesToArray['status']= array_column($stagedocument->getstatus($stagedocument->id_stagedocument),'status');
		return $attributesToArray;
    }

    public function includeStagedocumentcomment(Stagedocument $stagedocument)
    {
        $Stagedocumentcomment = \App\Models\Stagedocumentcomment::where('id_stagedocument', $stagedocument->id_stagedocument)->get();
        if($Stagedocumentcomment)
        return $this->collection($Stagedocumentcomment, new StagedocumentcommentTransformer);
    }
   
   
}
