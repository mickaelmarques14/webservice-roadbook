<?php

namespace App\Transformers;

use App\Models\Scheduleperformance;
use League\Fractal\ParamBag;
use League\Fractal\TransformerAbstract;


class ScheduleperformanceTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['stage','artist'];

    public function transform(Scheduleperformance $scheduleperformance)
    {
        return $scheduleperformance->attributesToArray();
    }
    public function includeStage(Scheduleperformance $scheduleperformance)
    {
        $stage = \App\Models\Stage::find($scheduleperformance->id_stage);
        return $this->item($stage, new StageTransformer);
    }
    public function includeArtist(Scheduleperformance $scheduleperformance)
    {
        $artist = \App\Models\Artist::find($scheduleperformance->id_artist);
        return $this->item($artist, new ArtistTransformer);
    }
 
   
   
}
