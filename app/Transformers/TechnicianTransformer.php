<?php

namespace App\Transformers;

use App\Models\Technician;
use League\Fractal\ParamBag;
use League\Fractal\TransformerAbstract;


class TechnicianTransformer extends TransformerAbstract
{

    public function transform(Technician $technician)
    {
        $attributesToArray = $technician->attributesToArray();
        return $attributesToArray;
    }

 
   
   
}
