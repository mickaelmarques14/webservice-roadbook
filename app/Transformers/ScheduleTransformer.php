<?php

namespace App\Transformers;

use App\Models\Schedule;
use League\Fractal\ParamBag;
use League\Fractal\TransformerAbstract;


class ScheduleTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['stage','artist'];
    public function transform(Schedule $schedule)
    {
        return $schedule->attributesToArray();
    }

    public function includeStage(Schedule $schedule)
    {
        $stage = \App\Models\Stage::find($schedule->id_stage);
        return $this->item($stage, new StageTransformer);
    }
    public function includeArtist(Schedule $schedule)
    {
        $artist = \App\Models\Artist::find($schedule->id_artist);
        return $this->item($artist, new ArtistTransformer);
    }
   
   
}
