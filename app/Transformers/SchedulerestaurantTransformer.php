<?php

namespace App\Transformers;

use App\Models\Schedulerestaurant;
use League\Fractal\ParamBag;
use League\Fractal\TransformerAbstract;


class SchedulerestaurantTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['restaurant','stage','artist'];

    public function transform(Schedulerestaurant $schedulerestaurant)
    {
        return $schedulerestaurant->attributesToArray();
    }
    public function includeStage(Schedulerestaurant $schedulerestaurant)
    {
        $stage = \App\Models\Stage::find($schedulerestaurant->id_stage);
        return $this->item($stage, new StageTransformer);
    }
    public function includeArtist(Schedulerestaurant $schedulerestaurant)
    {
        $artist = \App\Models\Artist::find($schedulerestaurant->id_artist);
        return $this->item($artist, new ArtistTransformer);
    }
    public function includeRestaurant(Schedulerestaurant $schedulerestaurant)
    {
        $restaurant = \App\Models\Restaurant::find($schedulerestaurant->id_restaurant);
        if($restaurant)
        return $this->item($restaurant, new RestaurantTransformer);
    }
}
