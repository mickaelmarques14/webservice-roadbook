<?php

namespace App\Transformers;

use App\Models\Manager;
use League\Fractal\ParamBag;
use League\Fractal\TransformerAbstract;


class ManagerTransformer extends TransformerAbstract
{
   protected $availableIncludes = ['user'];

    public function transform(Manager $manager)
    {
        return $manager->attributesToArray();
    }
    public function includeUser(Manager $manager)
    {
     
        $user = \App\Models\User::find($manager->id_user);
        if($user)
        return $this->item($user, new UserTransformer);
    }
 
   
   
}
