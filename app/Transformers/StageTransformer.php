<?php

namespace App\Transformers;

use App\Models\Stage;
use League\Fractal;
use League\Fractal\ParamBag;
use League\Fractal\TransformerAbstract;


class StageTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['event','stagestafftechnician','stagedocument'];

    public function transform(Stage $stage)
    {
        $attributesToArray = $stage->attributesToArray();
        $attributesToArray['artist'] = $stage->schedule_artist();
        $attributesToArray['technician'] = $stage->schedule_techinician();
        return $attributesToArray;
    }
  
    public function includeEvent(Stage $stage)
    {
        $event = \App\Models\Event::find($stage->id_event);
        return $this->item($event, new EventTransformer);
    }
    public function includeArtist(Stage $stage)
    {
        return $stage->artist()->get();
    }
    public function includeTechnician(Stage $stage)
    {
        return $stage->technician()->get();
    }
    public function includeStagestafftechnician(Stage $stage)
    {
        $Stagestafftechnician = \App\Models\Stagestafftechnician::where('id_stage', $stage->id_stage)->get();
        if($Stagestafftechnician)
        return $this->collection($Stagestafftechnician, new StagestafftechnicianTransformer);
    }
    public function includeStagedocument(Stage $stage)
    {
        $Stagedocument = \App\Models\Stagedocument::where('id_stage', $stage->id_stage)->get();
        if($Stagedocument)
        return $this->collection($Stagedocument, new StagedocumentTransformer);
    }
 

}