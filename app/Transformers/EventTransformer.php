<?php

namespace App\Transformers;

use App\Models\Post;
use App\Models\Event;
use League\Fractal\ParamBag;
use League\Fractal\TransformerAbstract;


class EventTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['user', 'production', 'promoter', 'stage'];

    public function transform(Event $event)
    {
        $attributesToArray = $event->attributesToArray();
		$attributesToArray['coverlink'] = ($event->cover ? url('images/'.$event->cover) : '');
		return $attributesToArray;
    }

    public function includeUser(Event $event)
    {
        if (! $event->user) {
            return $this->null();
        }

        return $this->item($event->user, new UserTransformer());
    }
    public function includeProduction(Event $event)
    {
        $production = \App\Models\Production::find($event->id_production);
        if($production)
        return $this->item($production, new ProductionTransformer);
    }
    public function includePromoter(Event $event)
    {
        $promoter = \App\Models\Promoter::find($event->id_promoter);
        if($promoter)
        return $this->item($promoter, new PromoterTransformer);
    }
    public function includeStage(Event $event)
    {
        $stage = $event->stage()->get();
         return $this->collection($stage, new StageTransformer());
    }
   
}
