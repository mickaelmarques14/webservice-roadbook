<?php

namespace App\Transformers;

use App\Models\Promoter;
use League\Fractal\ParamBag;
use League\Fractal\TransformerAbstract;


class PromoterTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['user'];
    public function transform(Promoter $promoter)
    {

        return $promoter->attributesToArray();
    }

    public function includeUser(Promoter $promoter)
    {
        
        if (! $promoter->id_user) {
            return $this->null();
        }
        return $this->item($promoter->id_user, new UserTransformer());
    }

   
   
}
