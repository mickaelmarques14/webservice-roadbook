<?php

namespace App\Transformers;

use App\Models\Artist;
use League\Fractal\ParamBag;
use League\Fractal\TransformerAbstract;


class ArtistTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['user', 'manager'];

    public function transform(Artist $artist)
    {
        return $artist->attributesToArray();
    }
    public function includeManager(Artist $artist)
    {
     
        $manager = \App\Models\Manager::find($artist->id_manager);
        if($manager)
        return $this->item($manager, new ManagerTransformer);
    }
    public function includeUser(Artist $artist)
    {
     
        $user = \App\Models\User::find($artist->id_user);
        if($user)
        return $this->item($user, new UserTransformer);
    }
   
   
}
