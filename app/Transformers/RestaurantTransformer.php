<?php

namespace App\Transformers;

use App\Models\Restaurant;
use League\Fractal\ParamBag;
use League\Fractal\TransformerAbstract;


class RestaurantTransformer extends TransformerAbstract
{

    public function transform(Restaurant $restaurant)
    {
        return $restaurant->attributesToArray();
    }

 
   
   
}
