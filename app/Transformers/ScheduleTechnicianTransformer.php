<?php

namespace App\Transformers;

use App\Models\ScheduleTechnician;
use League\Fractal\ParamBag;
use League\Fractal\TransformerAbstract;


class ScheduleTechnicianTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['stage','technician','restaurant','hotel'];
    public function transform(ScheduleTechnician $scheduletechnician)
    {
    $attributesToArray = $scheduletechnician->attributesToArray();
    $attributesToArray['hotel'] = $scheduletechnician->hotel()->get(); 
    $attributesToArray['restaurant'] = $scheduletechnician->restaurant()->get();
    return $attributesToArray;
    }

    public function includeStage(ScheduleTechnician $scheduletechnician)
    {
        $stage = \App\Models\Stage::find($scheduletechnician->id_stage);
        return $this->item($stage, new StageTransformer);
    }
    public function includeTechnician(ScheduleTechnician $scheduletechnician)
    {
        $technician = \App\Models\Technician::find($scheduletechnician->id_technician);
        return $this->item($technician, new TechnicianTransformer);
    }
    public function includeHotel(ScheduleTechnician $scheduletechnician)
    {
        $hotel = \App\Models\Hotel::find($scheduletechnician->id_thotel);
        return $this->item($hotel, new HotelTransformer);
    }
    public function includeRestaurant(ScheduleTechnician $scheduletechnician)
    {
        $technician = \App\Models\Restaurant::find($scheduletechnician->id_technician);
        return $this->item($technician, new RestaurantTransformer);
    }
   
   
}
