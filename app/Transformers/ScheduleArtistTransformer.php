<?php

namespace App\Transformers;

use App\Models\ScheduleArtist;
use League\Fractal\TransformerAbstract;

class ScheduleArtistTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['stage', 'artist', 'hotel', 'restaurant'];
    public function transform(ScheduleArtist $scheduleartist)
    {
        $attributesToArray = $scheduleartist->attributesToArray();
            $attributesToArray['hotel'] = $scheduleartist->hotel()->get(); 
        $attributesToArray['restaurant'] = $scheduleartist->restaurant()->get();
        return $attributesToArray;
    }

    public function includeStage(ScheduleArtist $scheduleartist)
    {
        $stage = \App\Models\Stage::find($scheduleartist->id_stage);
        return $this->item($stage, new StageTransformer);
    }
    public function includeArtist(ScheduleArtist $scheduleartist)
    {
        $artist = \App\Models\Artist::find($scheduleartist->id_artist);
        return $this->item($artist, new ArtistTransformer);
    }

}
