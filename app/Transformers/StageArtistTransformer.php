<?php

namespace App\Transformers;

use App\Models\StageArtist;
use League\Fractal;
use League\Fractal\ParamBag;
use League\Fractal\TransformerAbstract;


class StageArtistTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['artist','stage'];

    public function transform(StageArtist $stageArtist)
    {
        $attributesToArray = $stageArtist->attributesToArray();
        $attributesToArray['artist'] =  $stageArtist->artist()->get();
        $attributesToArray['schedule'] =  $stageArtist->scheduleartist();
        $attributesToArray['newschedule'] =  ['start'=>'','end'=>'','desc'=>'', 'hotel'=> null, 'restaurant' => null ];
       // $attributesToArray['schedule'] =  [];
        return $attributesToArray;
    }
  
    public function includeArtist(StageArtist $stageArtist)
    {
        $techician = \App\Models\Techician::where('id_techician', $stageArtist->id_techician)->get();
        if($techician)
        return $this->collection($techician, new TechicianTransformer);
    }
    public function includeStage(StageArtist $stageArtist)
    {
        $stage = \App\Models\Stage::where('id_stage', $stageArtist->id_stage)->get();
        if($stage)
        return $this->collection($stage, new StageTransformer);
    }
}