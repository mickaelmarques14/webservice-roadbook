<?php

namespace App\Transformers;

use App\Models\Stagedocumentcomment;
use League\Fractal\ParamBag;
use League\Fractal\TransformerAbstract;


class StagedocumentcommentTransformer extends TransformerAbstract
{
    public function transform(Stagedocumentcomment $stagedocumentcomment)
    {
        return $stagedocumentcomment->attributesToArray();
    }


   
   
}
