<?php

namespace App\Transformers;

use App\Models\Production;
use League\Fractal\ParamBag;
use League\Fractal\TransformerAbstract;


class ProductionTransformer extends TransformerAbstract
{

    public function transform(Production $production)
    {
        return $production->attributesToArray();
    }

 
   
   
}
