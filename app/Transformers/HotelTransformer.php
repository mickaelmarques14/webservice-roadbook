<?php

namespace App\Transformers;

use App\Models\Hotel;
use League\Fractal\ParamBag;
use League\Fractal\TransformerAbstract;


class HotelTransformer extends TransformerAbstract
{

    public function transform(Hotel $hotel)
    {
        return $hotel->attributesToArray();
    }

 
   
   
}
