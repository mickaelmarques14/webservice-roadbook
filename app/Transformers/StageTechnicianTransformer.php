<?php

namespace App\Transformers;

use App\Models\StageTechnician;
use League\Fractal;
use League\Fractal\ParamBag;
use League\Fractal\TransformerAbstract;


class StageTechnicianTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['technician','stage'];

    public function transform(StageTechnician $stageTechnician)
    {
        $attributesToArray = $stageTechnician->attributesToArray();
        $attributesToArray['technician'] =  $stageTechnician->technician()->get();
        $attributesToArray['schedule'] =  $stageTechnician->scheduletechnician();
        $attributesToArray['newschedule'] =  ['start'=>'','end'=>'','desc'=>'', 'hotel'=> null, 'restaurant' => null ];
         return $attributesToArray;
        
    }
  
    public function includeTechnician(StageTechnician $stageTechnician)
    {
        $techician = \App\Models\Techician::where('id_techician', $stageTechnician->id_techician)->get();
        if($techician)
        return $this->collection($techician, new TechicianTransformer);
    }
    public function includeStage(StageTechnician $stageTechnician)
    {
        $stage = \App\Models\Stage::where('id_stage', $stageTechnician->id_stage)->get();
        if($stage)
        return $this->collection($stage, new StageTransformer);
    }
}