<?php

namespace App\Transformers;

use App\Models\Schedulesetup;
use League\Fractal\ParamBag;
use League\Fractal\TransformerAbstract;


class SchedulesetupTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['stage','artist'];


    public function transform(Schedulesetup $schedulesetup)
    {
        return $schedulesetup->attributesToArray();
    }

    public function includeStage(Schedulesetup $schedulesetup)
    {
        $stage = \App\Models\Stage::find($schedulesetup->id_stage);
        return $this->item($stage, new StageTransformer);
    }
    public function includeArtist(Schedulesetup $schedulesetup)
    {
        $artist = \App\Models\Artist::find($schedulesetup->id_artist);
        return $this->item($artist, new ArtistTransformer);
    }
 
   
   
}
