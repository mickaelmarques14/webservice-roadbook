<?php

namespace App\Transformers;

use App\Models\Schedulehotel;
use League\Fractal\ParamBag;
use League\Fractal\TransformerAbstract;


class SchedulehotelTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['hotel','stage','artist'];
    public function transform(Schedulehotel $schedulehotel)
    {
        return $schedulehotel->attributesToArray();
    }

    public function includeHotel(Schedulehotel $schedulehotel)
    {
     
        $hotel = \App\Models\Hotel::find($schedulehotel->id_hotel);
        if($hotel)
        return $this->item($hotel, new HotelTransformer);
    }
    public function includeStage(Schedulehotel $schedulehotel)
    {
        $stage = \App\Models\Stage::find($schedulehotel->id_stage);
        return $this->item($stage, new StageTransformer);
    }
    public function includeArtist(Schedulehotel $schedulehotel)
    {
        $artist = \App\Models\Artist::find($schedulehotel->id_artist);
        return $this->item($artist, new ArtistTransformer);
    }
   
}
