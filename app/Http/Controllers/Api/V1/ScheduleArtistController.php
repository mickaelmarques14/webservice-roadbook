<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\ScheduleArtist;
use App\Models\StageArtist;
use App\Transformers\ScheduleArtistTransformer;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ScheduleArtistController extends BaseController
{
    private $scheduleartist;

    public function __construct(ScheduleArtist $scheduleartist)
    {
        $this->scheduleartist = $scheduleartist;
    }

    public function index()
    {
        $limit = (app('request')->input('limit') && app('request')->input('limit') <= 50 ? app('request')->input('limit') : 10);
        $pagen = (app('request')->input('page') ? app('request')->input('page') : 1);
        $pagename = (app('request')->input('pagename') ? app('request')->input('pagename') : 'page');
        $scheduleartist = $this->scheduleartist->paginate($limit, ['*'], 'page', $pagen);
        return $this->response->paginator($scheduleartist, new ScheduleArtistTransformer());
    }
    public function stageartist($id)
    {
       $scheduleartist = $this->scheduleartist->where('id_stage_artist',$id)->orderBy('date_begin')->get();
       return $this->collection($scheduleartist, new ScheduleArtistTransformer());
    }

    public function show($id)
    {
        $scheduleartist = $this->scheduleartist->findOrFail($id);
        $fractal = new \League\Fractal\Manager();
        $fractal->setSerializer(new \League\Fractal\Serializer\DataArraySerializer());
        $fractal->parseIncludes(['stage', 'artist']);
        $response = new \League\Fractal\Resource\Item($scheduleartist, new ScheduleArtistTransformer());
        return $this->response->array($fractal->createData($response)->toArray());
    }
    /**
     *
     *
     *
     *
     *
     */
    public function search(Request $request)
    {
        $scheduleartist = $this->scheduleartist->customLike($request->all())->orderBy('name')->get();
        return $this->response->item($scheduleartist, new ScheduleArtistTransformer());
    }
    /**
     * @api {scheduleartist} /scheduleartist (create scheduleartist)
     * @apiDescription (create scheduleartist)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiParam {String} title
     * @apiParam {String} description
     * @apiParam {DateTime} start
     * @apiParam {DateTime} end
     * @apiParam {DateTime} end
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 201 Created
     */
    public function store(Request $request)
    {

        $validator = \Validator::make($request->input(), [
            'name' => 'required|string|max:50',
            'stage' => 'required',
            'artist' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        $stageArtist = StageArtist::firstOrCreate(['id_artist'=>  $request->artist,'id_stage'=> $request->stage]);

        $start = null;
        $end = null;
        if ($request->has('startdate')) {
            $start = Carbon::parse($request->startdate)->format('Y-m-d H:i:s');
        }
        if ($request->has('enddate') && !empty($request->enddate)) {
            $end = Carbon::parse($request->enddate)->format('Y-m-d H:i:s');
        }
        if ($request->has('status')) {
            $attributes['status'] = $request->status;
        }

        $attributes['name'] = $request->name;
        $attributes['date_begin'] = $start;
        $attributes['date_end'] = $end;
        $attributes['id_restaurant'] = $request->has('id_restaurant') ? $request->id_restaurant : null;
        $attributes['id_hotel'] = $request->has('id_hotel') ? $request->id_hotel : null;
        $attributes['twin'] = $request->has('twin') ? $request->twin : null;
        $attributes['double'] = $request->has('double') ? $request->double : null;
        $attributes['single'] = $request->has('single') ? $request->single : null;
        $attributes['comment'] = $request->has('commenthotel') ? $request->commenthotel : null;
        $attributes['comment'] = $request->has('commentrestaurant') ? $request->commentrestaurant : null;
        $attributes['id_stage_artist'] = $stageArtist->id_stage_artist;
        $scheduleartist = $this->scheduleartist->create($attributes);
        // Return 201 plus data
        return $this->response
            ->item($scheduleartist, new ScheduleArtistTransformer())
            ->setStatusCode(201);
    }

    /**
     * @api {put} /scheduleartist/{id} (update post)
     * @apiDescription (update post)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiParam {String} title  post title
     * @apiParam {String} content  post content
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204 NO CONTENT
     */
    public function update($id, Request $request)
    {
        $scheduleartist = $this->scheduleartist->findOrFail($id);

        $validator = \Validator::make($request->input(), [
            'name' => 'required|string|max:50',
            'stage' => 'required',
            'artist' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        $stageArtist = StageArtist::firstOrCreate(['id_artist'=>  $request->artist,'id_stage'=> $request->stage]);
        $start = null;
        $end = null;
        if ($request->has('startdate')) {
            $start = Carbon::parse($request->startdate)->format('Y-m-d H:i:s');
        }
        if ($request->has('enddate') && !empty($request->enddate)) {
            $end = Carbon::parse($request->enddate)->format('Y-m-d H:i:s');
        }
        if ($request->has('status')) {
            $scheduleartist->status = $request->status;
        }

        $scheduleartist->name = $request->name;
        $scheduleartist->date_begin = $start;
        $scheduleartist->date_end = $end;
        $scheduleartist->id_hotel = $request->has('hotel') ? $request->hotel : null;
        $scheduleartist->id_restaurant = $request->has('restaurant') ? $request->restaurant : null;;
        $scheduleartist->id_stage_artist = $stageArtist->id_stage_artist;
        $scheduleartist->save();
        
        return $this->response->item($scheduleartist, new ScheduleArtistTransformer());
    }

    /**
     * @api {delete} /scheduleartist/{id} (delete post)
     * @apiDescription (delete post)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204 NO CONTENT
     */
    public function destroy($id)
    {
        $scheduleartist = $this->scheduleartist->findOrFail($id);
        $scheduleartist->delete();
        //$post->forceDelete(); delete from table
        return $this->response->noContent();
    }
}
