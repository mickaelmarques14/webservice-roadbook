<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Event;
use App\Models\Stage;
use App\Models\Promoter;
use App\Models\Production;
use App\Models\Technician;
use App\Models\Artist;
use App\Transformers\EventTransformer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use PDF;

class EventController extends BaseController
{
    private $event;
    public function __construct(Event $event)
    {
        $this->event = $event;

    }

    /**
     * @api {get} /posts (post list)
     * @apiDescription (post list)
     * @apiGroup Post
     * @apiPermission none
     * @apiParam {String='comments:limit(x)','user'} [include]  include
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 200 OK
     *   {
     *     "data": [
     *       {
     *         "id": 1,
     *         "user_id": 3,
     *         "title": "foo",
     *         "content": "",
     *         "created_at": "2016-03-30 15:36:30",
     *         "user": {
     *           "data": {
     *             "id": 3,
     *             "email": "foo@bar.com1",
     *             "name": "",
     *             "avatar": "",
     *             "created_at": "2016-03-30 15:34:01",
     *             "updated_at": "2016-03-30 15:34:01",
     *             "deleted_at": null
     *           }
     *         },
     *         "comments": {
     *           "data": [],
     *           "meta": {
     *             "total": 0
     *           }
     *         }
     *       }
     *     ],
     *     "meta": {
     *       "pagination": {
     *         "total": 2,
     *         "count": 2,
     *         "per_page": 15,
     *         "current_page": 1,
     *         "total_pages": 1,
     *         "links": []
     *       }
     *     }
     *   }
     */
    public function index()
    {

        $limit = (app('request')->input('limit') && app('request')->input('limit') <= 50 ? app('request')->input('limit') : 10);
        $pagen = (app('request')->input('page') ? app('request')->input('page') : 1);
        $pagename = (app('request')->input('pagename') ? app('request')->input('pagename') : 'page');
        if (in_array('admin', $this->user()->role) || in_array('superadmin', $this->user()->role)) {
            $event = $this->event->paginate($limit, ['*'], 'page', $pagen);
        }
        if (in_array('adminproduction', $this->user()->role) || in_array('production', $this->user()->role)) {
            $restrict = $this->user()->production_member()->first();
            $condition = isset($restrict->id_production) ? $restrict->id_production : null;
            $event = $this->event->with('production')->where('id_production', $condition)->paginate($limit, ['*'], $pagename, $pagen);
        }
        if (in_array('promoter', $this->user()->role)) {
            $restrict = $this->user()->promoter()->first();
            $condition = isset($restrict->id_promoter) ? $restrict->id_promoter : null;
            $event = $this->event->with('production')->where('id_promoter', $condition)->paginate($limit, ['*'], $pagename, $pagen);
        }
        return $this->response->paginator($event, new EventTransformer());
    }
    public function upcoming()
    {
        $limit = (app('request')->input('limit') && app('request')->input('limit') <= 50 ? app('request')->input('limit') : 10);
        $pagen = (app('request')->input('page') ? app('request')->input('page') : 1);
        $pagename = (app('request')->input('pagename') ? app('request')->input('pagename') : 'page');
        if (in_array('admin', $this->user()->role) || in_array('superadmin', $this->user()->role)) {
            $event = $this->event->whereDate('date_end', '>=', Carbon::today()->toDateString())->paginate($limit, ['*'], 'page', $pagen);
        }
        if (in_array('adminproduction', $this->user()->role) || in_array('production', $this->user()->role)) {
            $restrict = $this->user()->production_member()->first();
            $condition = isset($restrict->id_production) ? $restrict->id_production : null;
            $event = $this->event->with('production')->where('id_production', $condition)->whereDate('date_end', '>=', Carbon::today()->toDateString())->paginate($limit, ['*'], $pagename, $pagen);
        }
        if (in_array('promoter', $this->user()->role)) {
            $restrict = $this->user()->promoter()->first();
            $condition = isset($restrict->id_promoter) ? $restrict->id_promoter : null;
            $event = $this->event->with('production')->where('id_promoter', $condition)->whereDate('date_end', '>=', Carbon::today()->toDateString())->paginate($limit, ['*'], $pagename, $pagen);
        }
        return $this->response->paginator($event, new EventTransformer());
    }
    public function search(Request $request)
    {
        $event = $this->event->customLike($request->all())->orderBy('name')->get();
        return $this->response->item($event, new EventTransformer());
    }
    /**
     * @api {get} /posts/{id} (post detail)
     * @apiDescription (post detail)
     * @apiGroup Post
     * @apiPermission none
     * @apiParam {String='comments','user'} [include]  include
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 200 OK
     *   {
     *     "data": {
     *       "id": 1,
     *       "user_id": 3,
     *       "title": "foo",
     *       "content": "",
     *       "created_at": "2016-03-30 15:36:30",
     *       "user": {
     *         "data": {
     *           "id": 3,
     *           "email": "foo@bar.com1",
     *           "name": "",
     *           "avatar": "",
     *           "created_at": "2016-03-30 15:34:01",
     *           "updated_at": "2016-03-30 15:34:01",
     *           "deleted_at": null
     *         }
     *       },
     *       "comments": {
     *         "data": [
     *           {
     *             "id": 1,
     *             "post_id": 1,
     *             "user_id": 1,
     *             "reply_user_id": 0,
     *             "content": "foobar",
     *             "created_at": "2016-04-06 14:51:34"
     *           }
     *         ],
     *         "meta": {
     *           "total": 1
     *         }
     *       }
     *     }
     *   }
     */
    public function show($id)
    {
        $event = $this->event->findOrFail($id);
        $fractal = new \League\Fractal\Manager();
        $fractal->setSerializer(new \League\Fractal\Serializer\DataArraySerializer());
        $fractal->parseIncludes(['promoter', 'production']);
        $response = new \League\Fractal\Resource\Item($event, new EventTransformer());
        return $this->response->array($fractal->createData($response)->toArray());
    }

    /**
     * @api {event} /event (create event)
     * @apiDescription (create event)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiParam {String} title
     * @apiParam {String} description
     * @apiParam {DateTime} start
     * @apiParam {DateTime} end
     * @apiParam {DateTime} end
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 201 Created
     */
    public function store(Request $request)
    {
        
        $validator = \Validator::make($request->input(), [
            'name' => 'required|string|max:50',
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        $attributes= $request->all();
        if ($request->has('startdate')) {
            $$attributes['date_begin'] = Carbon::parse($request->startdate)->format('Y-m-d H:i:s');
        }
        if ($request->has('enddate')) {
            $attributes['date_end'] = Carbon::parse($request->enddate)->format('Y-m-d H:i:s');
        }
        if (in_array('adminproduction', $this->user()->role) || in_array('production', $this->user()->role)) {
            $restrict = $this->user()->production_member()->first();
            if(!$restrict)
            return $this->response->errorUnauthorized('Access denied');
            $attributes['id_production'] = $restrict->id_production;
        }
        $event = $this->event->create($attributes);
        //return $start;
        // Return 201 plus data
        return $this->response
            ->item($event, new EventTransformer())
            ->setStatusCode(201);
    }

    /**
     * @api {put} /event/{id} (update post)
     * @apiDescription (update post)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiParam {String} title  post title
     * @apiParam {String} content  post content
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204 NO CONTENT
     */
    public function update($id, Request $request)
    {

        $event = $this->event->findOrFail($id);
        $validator = \Validator::make($request->input(), [
            'name' => 'required|string|max:50',
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        $start = null;
        $end = null;
        if ($request->has('startdate')) {
            $start = Carbon::parse($request->startdate)->format('Y-m-d H:i:s');
        }
        if ($request->has('enddate')) {
            $end = Carbon::parse($request->enddate)->format('Y-m-d H:i:s');
        }
        $event->name = $request->name;
        $event->description = ($request->has('description') ? $request->description : '');
        $event->status = (string) $request->status;
        $event->date_begin = $start;
        $event->date_end = $end;
        $event->website = ($request->has('website') ? $request->website : '');
        $event->facebook = ($request->has('facebook') ? $request->facebook : '');
        $event->youtube = ($request->has('youtube') ? $request->youtube : '');
        $event->instagram = ($request->has('instagram') ? $request->instagram : '');
        $event->twitter = ($request->has('twitter') ? $request->twitter : '');
        $event->address = ($request->has('address') ? $request->address : '');
        if ($request->has('cover')) {
            $event->cover = $request->cover;
        }

        if ($request->has('latitude') && $request->has('longitude')) {
            $event->latitude = $request->latitude;
            $event->longitude = $request->longitude;
        }
        if ($request->has('promoter')) {
            $event->id_promoter = $request->promoter;
        }

        if ($request->has('production')) {
            $event->id_production = $request->production;
        }

        $event->save();

        return $this->response->item($event, new EventTransformer());
    }

    /**
     * @api {delete} /event/{id} (delete post)
     * @apiDescription (delete post)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204 NO CONTENT
     */
    public function destroy($id)
    {
        $event = $this->event->findOrFail($id);
        $event->delete();
        return $this->response->noContent();
    }

    public function upload(Request $request)
    {

        $validator = \Validator::make($request->all(), [
            //'file' => 'image|mimes:jpeg,png,jpg,gif,svg,JPEG,PNG,JPG,GIF,SVG',
        ]);
        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        
        $imageData = $request->file('file');
        $filename = time() . str_random(5) . '.' . $imageData->getClientOriginalExtension();
        $destinationPath = rtrim(app()->basePath('public/images/'));
        $imageData->move($destinationPath, $filename);
        return $this->response->array([
            'status_code' => 200,
            'data' => ['file' => $filename, 'path' => url('images/' . $filename)],
        ])->setStatusCode(200);

    }
    public function stages($id)
    {
        $event = $this->event->findOrFail($id);
        $fractal = new \League\Fractal\Manager();
        $fractal->setSerializer(new \League\Fractal\Serializer\DataArraySerializer());
        $fractal->parseIncludes(['promoter', 'production','stage']);
        $response = new \League\Fractal\Resource\Item($event, new EventTransformer());
        foreach ($response as $obj) {
            return $this->response->array($obj->data->id_event);
        }
        return $this->response->array($fractal->createData($response)->toArray());
    }
    public function roadbook($id){
       
    }
}
