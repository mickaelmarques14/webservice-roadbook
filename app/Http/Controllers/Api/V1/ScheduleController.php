<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Schedule;
use App\Transformers\ScheduleTransformer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class ScheduleController extends BaseController
{
    private $schedule;

    public function __construct(Schedule $schedule)
    {
        $this->schedule = $schedule;
    }

    public function index()
    {
        $limit = (app('request')->input('limit') && app('request')->input('limit') <= 50 ? app('request')->input('limit') : 10);
        $pagen = (app('request')->input('page') ? app('request')->input('page') : 1);
        $pagename = (app('request')->input('pagename') ? app('request')->input('pagename') : 'page');
        $schedule = $this->schedule->paginate($limit, ['*'], 'page', $pagen);
        return $this->response->paginator($schedule, new ScheduleTransformer());
    }
    public function stageIndex($id)
    {
        $limit = (app('request')->input('limit') && app('request')->input('limit') <= 50 ? app('request')->input('limit') : 50);
        $pagen = (app('request')->input('page') ? app('request')->input('page') : 1);
        $pagename = (app('request')->input('pagename') ? app('request')->input('pagename') : 'page');
        $schedule = $this->schedule
            ->where(['id_stage' => $id])
            ->paginate($limit, ['*'], 'page', $pagen);
        return $this->response->paginator($schedule, new ScheduleTransformer());
    }
    public function scheduledays($id, Request $request)
    {
        $validator = \Validator::make($request->input(), [
            'startdate' => 'required',
            'enddate' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
          $start = Carbon::parse($request->startdate)->format('Y-m-d H:i:s');
           $end = Carbon::parse($request->enddate)->format('Y-m-d H:i:s');
        $schedule = $this->schedule->artistJoin()
            ->where('id_stage', '=', $id)
            ->whereDate('date_begin','>=',$request->startdate)
            ->whereDate('date_end','<=',$request->enddate)
            ->get(
                ['schedule.id_schedule as id_schedule', 'schedule.name as title',  \DB::raw('DATE_FORMAT(schedule.date_begin, "%Y-%m-%dT%TZ") as start'), \DB::raw('DATE_FORMAT(schedule.date_end, "%Y-%m-%dT%TZ") as end'), 'schedule.id_artist as id_artist','schedule.comment', 'artist.name as artist', \DB::raw('"5" as type')]);
        return $this->response->item($schedule, new ScheduleTransformer);
        
    }
    public function all(Request $request){
        $limit = (app('request')->input('limit') && app('request')->input('limit') <= 50 ? app('request')->input('limit') : 50);
        $pagen = (app('request')->input('page') ? app('request')->input('page') : 1);
        $pagename = (app('request')->input('pagename') ? app('request')->input('pagename') : 'page');
        
        $schedulehotel = \DB::table("schedulehotel")->join('artist', 'schedulehotel.id_artist', '=', 'artist.id_artist')->whereNull('schedulehotel.deleted_at')->select('schedulehotel.id_schedulehotel as id_schedule', 'schedulehotel.name as title',  \DB::raw('DATE_FORMAT(schedulehotel.date_begin, "%Y-%m-%dT%TZ") as start'), \DB::raw('DATE_FORMAT(schedulehotel.date_end, "%Y-%m-%dT%TZ") as end'), 'schedulehotel.id_artist as id_artist', 'artist.name as artist', \DB::raw('"3" as type'));
        $scheduleperformance = \DB::table("scheduleperformance")->join('artist', 'scheduleperformance.id_artist', '=', 'artist.id_artist')->whereNull('scheduleperformance.deleted_at')->select('scheduleperformance.id_scheduleperformance as id_schedule', 'scheduleperformance.name as title',  \DB::raw('DATE_FORMAT(scheduleperformance.date_begin, "%Y-%m-%dT%TZ") as start'), \DB::raw('DATE_FORMAT(scheduleperformance.date_end, "%Y-%m-%dT%TZ") as end'), 'scheduleperformance.id_artist as id_artist', 'artist.name as artist', \DB::raw('"1" as type'));
        $schedulerestaurant =\ DB::table("schedulerestaurant")->join('artist', 'schedulerestaurant.id_artist', '=', 'artist.id_artist')->whereNull('schedulerestaurant.deleted_at')->select('schedulerestaurant.id_schedulerestaurant as id_schedule', 'schedulerestaurant.name as title',  \DB::raw('DATE_FORMAT(schedulerestaurant.date_begin, "%Y-%m-%dT%TZ") as start'), \DB::raw('DATE_FORMAT(schedulerestaurant.date_end, "%Y-%m-%dT%TZ") as end'), 'schedulerestaurant.id_artist as id_artist', 'artist.name as artist', \DB::raw('"4" as type'));
        $schedulesetup = \DB::table("schedulesetup")->join('artist', 'schedulesetup.id_artist', '=', 'artist.id_artist')->whereNull('schedulesetup.deleted_at')->select('schedulesetup.id_schedulesetup as id_schedule', 'schedulesetup.name as title',  \DB::raw('DATE_FORMAT(schedulesetup.date_begin, "%Y-%m-%dT%TZ") as start'), \DB::raw('DATE_FORMAT(schedulesetup.date_end, "%Y-%m-%dT%TZ") as end'), 'schedulesetup.id_artist as id_artist', 'artist.name as artist', \DB::raw('"2" as type'));
        $schedule = \DB::table("schedule")->join('artist', 'schedule.id_artist', '=', 'artist.id_artist')->whereNull('schedule.deleted_at')->select('schedule.id_schedule as id_schedule', 'schedule.name as title',  \DB::raw('DATE_FORMAT(schedule.date_begin, "%Y-%m-%dT%TZ") as start'), \DB::raw('DATE_FORMAT(schedule.date_end, "%Y-%m-%dT%TZ") as end'), 'schedule.id_artist as id_artist', 'artist.name as artist', \DB::raw('"5" as type'))
        ->union($schedulehotel)
        ->union($scheduleperformance)
        ->union($schedulerestaurant)
        ->union($schedulesetup)
        ->get();
        $result = new LengthAwarePaginator($schedule, count($schedule), $limit, $pagen);
        return $result;
    }
    /*

    $start = Carbon::parse($request->start)->format('Y-m-d H:i:s');
    $end = Carbon::parse($request->end)->format('Y-m-d H:i:s');
    $schedule = $this->schedule->eventJoin()->where('stage.id_stage' ,'=', $id)
    ->where('schedule.date_begin','>=',$start)
    ->where('schedule.date_end','<=',$end)
    ->get();
    return $this->response->item($schedule, new ScheduleTransformer());
     */
    public function show($id)
    {

        $schedule = $this->schedule->findOrFail($id);
        $fractal = new \League\Fractal\Manager();
        $fractal->setSerializer(new \League\Fractal\Serializer\DataArraySerializer());
        $fractal->parseIncludes(['stage', 'artist']);
        $response = new \League\Fractal\Resource\Item($schedule, new ScheduleTransformer());
        return $this->response->array($fractal->createData($response)->toArray());
    }
    /**
     *
     *
     *
     *
     *
     */
    public function search(Request $request)
    {
        $schedule = $this->schedule->customLike($request->all())->orderBy('name')->get();
        return $this->response->item($schedule, new ScheduleTransformer());
    }
    /**
     * @api {schedule} /schedule (create schedule)
     * @apiDescription (create schedule)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiParam {String} title
     * @apiParam {String} description
     * @apiParam {DateTime} start
     * @apiParam {DateTime} end
     * @apiParam {DateTime} end
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 201 Created
     */
    public function store(Request $request)
    {
      
        $validator = \Validator::make($request->input(), [
            'name' => 'required|string|max:50',
            'stage' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        $start = null;
        $end = null;
        if ($request->has('startdate')) {
            $start = Carbon::parse($request->startdate)->format('Y-m-d H:i:s');
        }
        if ($request->has('enddate')) {
            $end = Carbon::parse($request->enddate)->format('Y-m-d H:i:s');
        }
        if ($request->has('status')) $attributes['status'] = $request->status;
        $attributes['name'] = $request->name;
        $attributes['date_begin'] = $start;
        $attributes['date_end'] = $end;
        $attributes['id_stage'] = $request->stage;
        $attributes['comment'] = $request->comment;

        if ($request->has('artist')) {
            $attributes['id_artist'] = $request->artist;
        }

        $schedule = $this->schedule->create($attributes);
        // Return 201 plus data
        return $this->response
            ->item($schedule, new ScheduleTransformer())
            ->setStatusCode(201);
    }

    /**
     * @api {put} /schedule/{id} (update post)
     * @apiDescription (update post)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiParam {String} title  post title
     * @apiParam {String} content  post content
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204 NO CONTENT
     */
    public function update($id, Request $request)
    {
        $schedule = $this->schedule->findOrFail($id);

        $validator = \Validator::make($request->input(), [
            'name' => 'required|string|max:50',
            'stage' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        $start = null;
        $end = null;
        if ($request->has('startdate')) {
            $start = Carbon::parse($request->startdate)->format('Y-m-d H:i:s');
        }
        if ($request->has('enddate')) {
            $end = Carbon::parse($request->enddate)->format('Y-m-d H:i:s');
        }
        if ($request->has('status')) $schedule->status = $request->status;
        $schedule->name = $request->name;
        $schedule->date_begin = $start;
        $schedule->date_end = $end;
        if ($request->has('artist')) {
            $schedule->id_artist = $request->artist;
        }
        $schedule->comment = $request->comment;
        $schedule->id_stage = $request->stage;

        $schedule->save();

        return $this->response->item($schedule, new ScheduleTransformer());
    }

    /**
     * @api {delete} /schedule/{id} (delete post)
     * @apiDescription (delete post)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204 NO CONTENT
     */
    public function destroy($id)
    {
        $schedule = $this->schedule->findOrFail($id);
        $schedule->delete();
        //$post->forceDelete(); delete from table
        return $this->response->noContent();
    }
}
