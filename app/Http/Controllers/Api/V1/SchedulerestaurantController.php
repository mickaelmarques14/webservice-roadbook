<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Schedulerestaurant;
use App\Transformers\SchedulerestaurantTransformer;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SchedulerestaurantController extends BaseController
{
    private $schedulerestaurant;

    public function __construct(Schedulerestaurant $schedulerestaurant)
    {
        $this->schedulerestaurant = $schedulerestaurant;
    }

    public function index()
    {
        $limit = (app('request')->input('limit') && app('request')->input('limit') <= 50 ? app('request')->input('limit') : 10);
        $pagen = (app('request')->input('page') ? app('request')->input('page') : 1);
        $pagename = (app('request')->input('pagename') ? app('request')->input('pagename') : 'page');
        $schedulerestaurant = $this->schedulerestaurant->paginate($limit, ['*'], 'page', $pagen);
        return $this->response->paginator($schedulerestaurant, new SchedulerestaurantTransformer());
    }
    public function stageIndex($id)
    {
        $limit = (app('request')->input('limit') && app('request')->input('limit') <= 50 ? app('request')->input('limit') : 50);
        $pagen = (app('request')->input('page') ? app('request')->input('page') : 1);
        $pagename = (app('request')->input('pagename') ? app('request')->input('pagename') : 'page');
        $schedulerestaurant = $this->schedulerestaurant
            ->where(['id_stage' => $id])
            ->paginate($limit, ['*'], 'page', $pagen);
        return $this->response->paginator($schedulerestaurant, new SchedulerestaurantTransformer());
    }
    public function schedulerestaurantdays($id, Request $request)
    {

        $validator = \Validator::make($request->input(), [
            'startdate' => 'required',
            'enddate' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        //$start = Carbon::parse($request->startdate)->format('Y-m-d H:i:s');
        //$end = Carbon::parse($request->enddate)->format('Y-m-d H:i:s');
        $schedulerestaurant = $this->schedulerestaurant->artistJoin()->restaurantJoin()
            ->where('id_stage', '=', $id)
            ->whereDate('date_begin', '>=', $request->startdate)
            ->whereDate('date_end', '<=', $request->enddate)->get(
            ['schedulerestaurant.id_schedulerestaurant as id_schedule', 'schedulerestaurant.name as title',  \DB::raw('DATE_FORMAT(schedulerestaurant.date_begin, "%Y-%m-%dT%TZ") as start'), \DB::raw('DATE_FORMAT(schedulerestaurant.date_end, "%Y-%m-%dT%TZ") as end'),
            'schedulerestaurant.id_restaurant as restaurant', 'restaurant.name as restaurantname', 'schedulerestaurant.comment',
            'schedulerestaurant.id_artist as id_artist', 'artist.name as artist', \DB::raw('"4" as type')]);
        return $this->response->item($schedulerestaurant, new SchedulerestaurantTransformer());

    }
    /*

    $start = Carbon::parse($request->start)->format('Y-m-d H:i:s');
    $end = Carbon::parse($request->end)->format('Y-m-d H:i:s');
    $schedulerestaurant = $this->schedulerestaurant->eventJoin()->where('stage.id_stage' ,'=', $id)
    ->where('schedulerestaurant.date_begin','>=',$start)
    ->where('schedulerestaurant.date_end','<=',$end)
    ->get();
    return $this->response->item($schedulerestaurant, new SchedulerestaurantTransformer());
     */
    public function show($id)
    {
        $schedulerestaurant = $this->schedulerestaurant->findOrFail($id);
        $fractal = new \League\Fractal\Manager();
        $fractal->setSerializer(new \League\Fractal\Serializer\DataArraySerializer());
        if (isset($schedulerestaurant->id_restaurant)) {
            $fractal->parseIncludes(['restaurant', 'stage', 'artist']);
        } else {
            $fractal->parseIncludes(['stage', 'artist']);
        }

        $response = new \League\Fractal\Resource\Item($schedulerestaurant, new SchedulerestaurantTransformer());
        return $this->response->array($fractal->createData($response)->toArray());
    }
    /**
     *
     *
     *
     *
     *
     */
    public function search(Request $request)
    {
        $schedulerestaurant = $this->schedulerestaurant->customLike($request->all())->orderBy('name')->get();
        return $this->response->item($schedulerestaurant, new SchedulerestaurantTransformer());
    }
    /**
     * @api {schedulerestaurant} /schedulerestaurant (create schedulerestaurant)
     * @apiDescription (create schedulerestaurant)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiParam {String} title
     * @apiParam {String} description
     * @apiParam {DateTime} start
     * @apiParam {DateTime} end
     * @apiParam {DateTime} end
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 201 Created
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->input(), [
            'name' => 'required|string|max:50',
            'stage' => 'required',
            'restaurant' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        $start = null;
        $end = null;
        if ($request->has('startdate')) {
            $start = Carbon::parse($request->startdate)->format('Y-m-d H:i:s');
        }
        if ($request->has('enddate')) {
            $end = Carbon::parse($request->enddate)->format('Y-m-d H:i:s');
        }
        if ($request->has('status'))  $attributes['status'] = $request->status;
        $attributes['name'] = $request->name;
        $attributes['date_begin'] = $start;
        $attributes['date_end'] = $end;
        $attributes['id_stage'] = $request->stage;
        if ($request->has('artist')) {
            $attributes['id_artist'] = $request->artist;
        }
        $attributes['id_restaurant'] = $request->restaurant;
        $attributes['comment'] = $request->comment;

        $schedulerestaurant = $this->schedulerestaurant->create($attributes);
        // Return 201 plus data
        return $this->response
            ->item($schedulerestaurant, new SchedulerestaurantTransformer())
            ->setStatusCode(201);
    }

    /**
     * @api {put} /schedulerestaurant/{id} (update post)
     * @apiDescription (update post)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiParam {String} title  post title
     * @apiParam {String} content  post content
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204 NO CONTENT
     */
    public function update($id, Request $request)
    {
        $schedulerestaurant = $this->schedulerestaurant->findOrFail($id);

        $validator = \Validator::make($request->input(), [
            'name' => 'required|string|max:50',
            'stage' => 'required',
            'restaurant' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        $start = null;
        $end = null;
        if ($request->has('startdate')) {
            $start = Carbon::parse($request->startdate)->format('Y-m-d H:i:s');
        }
        if ($request->has('enddate')) {
            $end = Carbon::parse($request->enddate)->format('Y-m-d H:i:s');
        }
        if ($request->has('status'))   $schedulerestaurant->status = $request->status;
        $schedulerestaurant->name = $request->name;
        $schedulerestaurant->date_begin = $start;
        $schedulerestaurant->date_end = $end;
        if ($request->has('artist')) {
            $schedulerestaurant->id_artist = $request->artist;
        }

        $schedulerestaurant->id_artist = $request->restaurant;
        $schedulerestaurant->comment = $request->comment;

        $schedulerestaurant->id_stage = $request->stage;

        $schedulerestaurant->save();

        return $this->response->item($schedulerestaurant, new SchedulerestaurantTransformer());
    }

    /**
     * @api {delete} /schedulerestaurant/{id} (delete post)
     * @apiDescription (delete post)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204 NO CONTENT
     */
    public function destroy($id)
    {
        $schedulerestaurant = $this->schedulerestaurant->findOrFail($id);
        $schedulerestaurant->delete();
        //$post->forceDelete(); delete from table
        return $this->response->noContent();
    }
}
