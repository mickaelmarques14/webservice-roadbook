<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\ScheduleTechnician;
use App\Models\StageTechnician;
use App\Transformers\ScheduleTechnicianTransformer;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ScheduleTechnicianController extends BaseController
{
    private $scheduletechnician;

    public function __construct(ScheduleTechnician $scheduletechnician)
    {
        $this->scheduletechnician = $scheduletechnician;
    }

    public function index()
    {
        $limit = (app('request')->input('limit') && app('request')->input('limit') <= 50 ? app('request')->input('limit') : 10);
        $pagen = (app('request')->input('page') ? app('request')->input('page') : 1);
        $pagename = (app('request')->input('pagename') ? app('request')->input('pagename') : 'page');
        $scheduletechnician = $this->scheduletechnician->paginate($limit, ['*'], 'page', $pagen);
        return $this->response->paginator($scheduletechnician, new ScheduleTechnicianTransformer());
    }
    public function stageIndex($id)
    {
        $limit = (app('request')->input('limit') && app('request')->input('limit') <= 50 ? app('request')->input('limit') : 50);
        $pagen = (app('request')->input('page') ? app('request')->input('page') : 1);
        $pagename = (app('request')->input('pagename') ? app('request')->input('pagename') : 'page');
        $scheduletechnician = $this->scheduletechnician
            ->where(['id_stage' => $id])
            ->paginate($limit, ['*'], 'page', $pagen);
        return $this->response->paginator($scheduletechnician, new ScheduleTechnicianTransformer());
    }
    public function stagetechnician($id)
    {
       $scheduletechnician = $this->scheduletechnician->where('id_stage_technician',$id)->orderBy('date_begin')->get();
       return $this->collection($scheduletechnician, new ScheduleTechnicianTransformer());
    }

    /*

    $start = Carbon::parse($request->start)->format('Y-m-d H:i:s');
    $end = Carbon::parse($request->end)->format('Y-m-d H:i:s');
    $scheduletechnician = $this->scheduletechnician->eventJoin()->where('stage.id_stage' ,'=', $id)
    ->where('scheduletechnician.date_begin','>=',$start)
    ->where('scheduletechnician.date_end','<=',$end)
    ->get();
    return $this->response->item($scheduletechnician, new ScheduleTechnicianTransformer());
     */
    public function show($id)
    {
        $scheduletechnician = $this->scheduletechnician->findOrFail($id);
        $fractal = new \League\Fractal\Manager();
        $fractal->setSerializer(new \League\Fractal\Serializer\DataArraySerializer());
        $response = new \League\Fractal\Resource\Item($scheduletechnician, new ScheduleTechnicianTransformer());
        return $this->response->array($fractal->createData($response)->toArray());
    }
    /**
     *
     *
     *
     *
     *
     */
    public function search(Request $request)
    {
        $scheduletechnician = $this->scheduletechnician->customLike($request->all())->orderBy('name')->get();
        return $this->response->item($scheduletechnician, new ScheduleTechnicianTransformer());
    }
    /**
     * @api {scheduletechnician} /scheduletechnician (create scheduletechnician)
     * @apiDescription (create scheduletechnician)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiParam {String} title
     * @apiParam {String} description
     * @apiParam {DateTime} start
     * @apiParam {DateTime} end
     * @apiParam {DateTime} end
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 201 Created
     */
    public function store(Request $request)
    {

        $validator = \Validator::make($request->input(), [
            'name' => 'required|string|max:50',
            'stage' => 'required',
            'technician' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        $stageTechnician = StageTechnician::firstOrCreate(['id_technician'=>  $request->technician,'id_stage'=> $request->stage]);

        $start = null;
        $end = null;
        if ($request->has('startdate')) {
            $start = Carbon::parse($request->startdate)->format('Y-m-d H:i:s');
        }
        if ($request->has('enddate') && !empty($request->enddate)) {
            $end = Carbon::parse($request->enddate)->format('Y-m-d H:i:s');
        }
        if ($request->has('status')) {
            $attributes['status'] = $request->status;
        }

        $attributes['name'] = $request->name;
        $attributes['date_begin'] = $start;
        $attributes['date_end'] = $end;
        $attributes['id_restaurant'] = $request->has('id_restaurant') ? $request->id_restaurant : null;
        $attributes['id_hotel'] = $request->has('id_hotel') ? $request->id_hotel : null;
        $attributes['twin'] = $request->has('twin') ? $request->twin : null;
        $attributes['double'] = $request->has('double') ? $request->double : null;
        $attributes['single'] = $request->has('single') ? $request->single : null;
        $attributes['comment'] = $request->has('commenthotel') ? $request->commenthotel : null;
        $attributes['comment'] = $request->has('commentrestaurant') ? $request->commentrestaurant : null;
        $attributes['id_stage_technician'] = $stageTechnician->id_stage_technician;

        $scheduletechnician = $this->scheduletechnician->create($attributes);
        
        // Return 201 plus data
        return $this->response
             ->item($scheduletechnician, new ScheduleTechnicianTransformer())
             ->setStatusCode(201);
    }

    /**
     * @api {put} /scheduletechnician/{id} (update post)
     * @apiDescription (update post)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiParam {String} title  post title
     * @apiParam {String} content  post content
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204 NO CONTENT
     */
    public function update($id, Request $request)
    {
        $scheduletechnician = $this->scheduletechnician->findOrFail($id);

        $validator = \Validator::make($request->input(), [
            'name' => 'required|string|max:50',
            'type' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        $start = null;
        $end = null;
        if ($request->has('startdate')) {
            $start = Carbon::parse($request->startdate)->format('Y-m-d H:i:s');
        }
        if ($request->has('enddate')) {
            $end = Carbon::parse($request->enddate)->format('Y-m-d H:i:s');
        }
        if ($request->has('status')) {
            $scheduletechnician->status = $request->status;
        }
        $scheduletechnician->name = $request->name;
        $scheduletechnician->type = $request->type;
        $scheduletechnician->id_restaurant = $request->has('restaurant') ? $request->restaurant : null;
        $scheduletechnician->id_hotel = $request->has('hotel') ? $request->hotel : null ;
        $scheduletechnician->single = $request->has('hotelsingle') ? $request->hotelsingle : null;
        $scheduletechnician->double = $request->has('hoteldouble') ? $request->hoteldouble : null;
        $scheduletechnician->twin = $request->has('hoteltwin') ? $request->hoteltwin : null;
        $scheduletechnician->date_begin = $start;
        $scheduletechnician->date_end = $end;
        $scheduletechnician->comment = $request->comment;
        $scheduletechnician->save();
        return $this->response->item($scheduletechnician, new ScheduleTechnicianTransformer());
    }

    /**
     * @api {delete} /scheduletechnician/{id} (delete post)
     * @apiDescription (delete post)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204 NO CONTENT
     */
    public function destroy($id)
    {
        $scheduletechnician = $this->scheduletechnician->findOrFail($id);
        $scheduletechnician->delete();
        //$post->forceDelete(); delete from table
        return $this->response->noContent();
    }
  
}
