<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Schedulesetup;
use App\Transformers\SchedulesetupTransformer;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SchedulesetupController extends BaseController
{
    private $schedulesetup;

    public function __construct(Schedulesetup $schedulesetup)
    {
        $this->schedulesetup = $schedulesetup;
    }

    public function index()
    {
        $limit = (app('request')->input('limit') && app('request')->input('limit') <= 50 ? app('request')->input('limit') : 10);
        $pagen = (app('request')->input('page') ? app('request')->input('page') : 1);
        $pagename = (app('request')->input('pagename') ? app('request')->input('pagename') : 'page');
        $schedulesetup = $this->schedulesetup->paginate($limit, ['*'], 'page', $pagen);
        return $this->response->paginator($schedulesetup, new SchedulesetupTransformer());
    }
    public function stageIndex($id)
    {
        $limit = (app('request')->input('limit') && app('request')->input('limit') <= 50 ? app('request')->input('limit') : 50);
        $pagen = (app('request')->input('page') ? app('request')->input('page') : 1);
        $pagename = (app('request')->input('pagename') ? app('request')->input('pagename') : 'page');
        $schedulesetup = $this->schedulesetup
            ->where(['id_stage' => $id])
            ->paginate($limit, ['*'], 'page', $pagen);
        return $this->response->paginator($schedulesetup, new SchedulesetupTransformer());
    }
    public function schedulesetupdays($id, Request $request)
    {

        $validator = \Validator::make($request->input(), [
            'startdate' => 'required',
            'enddate' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
     
        //$start = Carbon::parse($request->startdate);
       // $end = Carbon::parse($request->enddate);
        $schedulesetup = $this->schedulesetup->artistJoin()
        ->where('id_stage' ,'=', $id)
        ->whereDate('date_begin','>=',($request->startdate))
        ->whereDate('date_end','<=',($request->enddate))->get(
            ['schedulesetup.id_schedulesetup as id_schedule', 'schedulesetup.name as title', \DB::raw('DATE_FORMAT(schedulesetup.date_begin, "%Y-%m-%dT%TZ") as start'), \DB::raw('DATE_FORMAT(schedulesetup.date_end, "%Y-%m-%dT%TZ") as end'),\DB::raw('DATE_FORMAT(schedulesetup.date_setup, "%Y-%m-%dT%TZ") as setup'),'schedulesetup.id_artist as id_artist','artist.name as artist',\DB::raw('"2" as type')]);

        return  $this->response->item($schedulesetup, new SchedulesetupTransformer());
    }
    /*

    $start = Carbon::parse($request->start)->format('Y-m-d H:i:s');
    $end = Carbon::parse($request->end)->format('Y-m-d H:i:s');
    $schedulesetup = $this->schedulesetup->eventJoin()->where('stage.id_stage' ,'=', $id)
    ->where('schedulesetup.date_begin','>=',$start)
    ->where('schedulesetup.date_end','<=',$end)
    ->get();
    return $this->response->item($schedulesetup, new SchedulesetupTransformer());
     */
    public function show($id)
    {

        $schedulesetup = $this->schedulesetup->findOrFail($id);
        $fractal = new \League\Fractal\Manager();
        $fractal->setSerializer(new \League\Fractal\Serializer\DataArraySerializer());
        $fractal->parseIncludes(['stage', 'artist']);
        $response = new \League\Fractal\Resource\Item($schedulesetup, new SchedulesetupTransformer());
        return $this->response->array($fractal->createData($response)->toArray());
    }
    /**
     *
     *
     *
     *
     *
     */
    public function search(Request $request)
    {
        $schedulesetup = $this->schedulesetup->customLike($request->all())->orderBy('name')->get();
        return $this->response->item($schedulesetup, new SchedulesetupTransformer());
    }
    /**
     * @api {schedulesetup} /schedulesetup (create schedulesetup)
     * @apiDescription (create schedulesetup)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiParam {String} title
     * @apiParam {String} description
     * @apiParam {DateTime} start
     * @apiParam {DateTime} end
     * @apiParam {DateTime} end
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 201 Created
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->input(), [
            'name' => 'required|string|max:50',
            'stage' => 'required',
            'setup' => 'required|integer',
            'startdate' => 'required',
            'enddate' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        $start = null;
        $end = null;
        if ($request->has('startdate')) {
            $carbondate = Carbon::parse($request->startdate);
            $start = $carbondate->format('Y-m-d H:i:00');
            $datesetup=  $carbondate->subMinutes($request->setup)->format('Y-m-d H:i:00');
        }
        if ($request->has('enddate')) {
            $end = Carbon::parse($request->enddate)->format('Y-m-d H:i:s');
        }
        if ($request->has('status'))  $attributes['status'] = $request->status;
        $attributes['name'] = $request->name;
        $attributes['date_begin'] = $start;
        $attributes['date_end'] = $end;
        $attributes['id_stage'] = $request->stage;
        $attributes['date_setup'] = $datesetup;
        if ($request->has('artist')) {
            $attributes['id_artist'] = $request->artist;
        }

        $schedulesetup = $this->schedulesetup->create($attributes);
        // Return 201 plus data
        return $this->response
            ->item($schedulesetup, new SchedulesetupTransformer())
            ->setStatusCode(201);
    }

    /**
     * @api {put} /schedulesetup/{id} (update post)
     * @apiDescription (update post)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiParam {String} title  post title
     * @apiParam {String} content  post content
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204 NO CONTENT
     */
    public function update($id, Request $request)
    {
        $schedulesetup = $this->schedulesetup->findOrFail($id);

        $validator = \Validator::make($request->input(), [
            'name' => 'required|string|max:50',
            'stage' => 'required',
            'setup' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        $start = null;
        $end = null;
        if ($request->has('startdate')) {
            $carbondate = Carbon::parse($request->startdate);
            $start = $carbondate->format('Y-m-d H:i:00');
            $datesetup=  $carbondate->subMinutes($request->setup)->format('Y-m-d H:i:00');
        }
        if ($request->has('enddate')) {
            $end = Carbon::parse($request->enddate)->format('Y-m-d H:i:s');
        }
        if ($request->has('status'))   $schedulesetup->status = $request->status;
        $schedulesetup->name = $request->name;
        $schedulesetup->date_setup = $datesetup;
        $schedulesetup->date_begin = $start;
        $schedulesetup->date_end = $end;
        if ($request->has('artist')) {
            $schedulesetup->id_artist = $request->artist;
        }
        $schedulesetup->id_stage = $request->stage;

        $schedulesetup->save();

        return $this->response->item($schedulesetup, new SchedulesetupTransformer());
    }

    /**
     * @api {delete} /schedulesetup/{id} (delete post)
     * @apiDescription (delete post)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204 NO CONTENT
     */
    public function destroy($id)
    {
        $schedulesetup = $this->schedulesetup->findOrFail($id);
        $schedulesetup->delete();
        //$post->forceDelete(); delete from table
        return $this->response->noContent();
    }
}
