<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Artist;
use App\Models\Production;
use App\Models\Promoter;
use App\Models\Stage;
use App\Models\Technician;
use App\Transformers\StageTransformer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use PDF;

class StageController extends BaseController
{
    private $stage;
    private $listfields = [
        'stage.id_stage', 'stage.name', 'stage.width', 'stage.length', 'stage.description', 'stage.id_technician',
        'technician.name as technician',
        'event.id_event', 'event.name as event', 'event.date_begin as eventstart', 'event.date_end as eventend'];
    private $listfields2 = [
        'stage.id_stage', 'stage.name', 'stage.id_technician',
        'technician.name as technician'];
    public function __construct(Stage $stage)
    {
        $this->stage = $stage;
    }
    public function index()
    {
        $limit = (app('request')->input('limit') && app('request')->input('limit') <= 50 ? app('request')->input('limit') : 10);
        $pagen = (app('request')->input('page') ? app('request')->input('page') : 1);
        $pagename = (app('request')->input('pagename') ? app('request')->input('pagename') : 'page');
        $stage = $this->stage->eventJoin()->technicianLeftJoin()->paginate($limit, $this->listfields, 'page', $pagen);
        return $this->response->paginator($stage, new StageTransformer());
    }
    public function eventIndex($id)
    {
        $stages = Stage::find(['id_event' => $id]);
        return $this->collection($stages, new StageTransformer());
        // $stage = $this->stage->technicianLeftJoin()->where(['id_event' => $id])->get($this->listfields2);
        // return $this->response->item($stage, new StageTransformer());
    }
    public function stafftechnician($id, Request $request)
    {
        $stage = $this->stage->findOrFail($id);
        $fractal = new \League\Fractal\Manager();
        $fractal->setSerializer(new \League\Fractal\Serializer\DataArraySerializer());
        $fractal->parseIncludes(['event', 'stagestafftechnician', 'technician']);
        $response = new \League\Fractal\Resource\Item($stage, new StageTransformer());
        return $this->response->array($fractal->createData($response)->toArray());
    }
    public function document($id, Request $request)
    {
        $stage = $this->stage->findOrFail($id);
        $fractal = new \League\Fractal\Manager();
        $fractal->setSerializer(new \League\Fractal\Serializer\DataArraySerializer());
        $fractal->parseIncludes(['event', 'stagedocument', 'technician']);
        $response = new \League\Fractal\Resource\Item($stage, new StageTransformer());
        return $this->response->array($fractal->createData($response)->toArray());
    }
    public function information($id, Request $request)
    {
        $stage = $this->stage->findOrFail($id);
        $fractal = new \League\Fractal\Manager();
        $fractal->setSerializer(new \League\Fractal\Serializer\DataArraySerializer());
        $fractal->parseIncludes(['event', 'stagestafftechnician', 'technician', 'stagedocument']);
        $response = new \League\Fractal\Resource\Item($stage, new StageTransformer());
        return $this->response->array($fractal->createData($response)->toArray());
    }
    /**
     * @api {get} /posts/{id} (post detail)
     * @apiDescription (post detail)
     * @apiGroup Post
     * @apiPermission none
     * @apiParam {String='comments','user'} [include]  include
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 200 OK
     *   {
     *     "data": {
     *       "id": 1,
     *       "user_id": 3,
     *       "title": "foo",
     *       "content": "",
     *       "created_at": "2016-03-30 15:36:30",
     *       "user": {
     *         "data": {
     *           "id": 3,
     *           "email": "foo@bar.com1",
     *           "name": "",
     *           "avatar": "",
     *           "created_at": "2016-03-30 15:34:01",
     *           "updated_at": "2016-03-30 15:34:01",
     *           "deleted_at": null
     *         }
     *       },
     *       "comments": {
     *         "data": [
     *           {
     *             "id": 1,
     *             "post_id": 1,
     *             "user_id": 1,
     *             "reply_user_id": 0,
     *             "content": "foobar",
     *             "created_at": "2016-04-06 14:51:34"
     *           }
     *         ],
     *         "meta": {
     *           "total": 1
     *         }
     *       }
     *     }
     *   }
     */
    public function show($id)
    {

        $stage = $this->stage->findOrFail($id);
        $fractal = new \League\Fractal\Manager();
        $fractal->setSerializer(new \League\Fractal\Serializer\DataArraySerializer());
        $fractal->parseIncludes(['technician', 'event', 'stagestafftechnician']);
        $response = new \League\Fractal\Resource\Item($stage, new StageTransformer());
        return $this->response->array($fractal->createData($response)->toArray());
    }
    /**
     *
     *
     *
     *
     *
     */
    public function search(Request $request)
    {
        $stage = $this->stage->customLike($request->all())->orderBy('name')->get();
        return $this->response->item($stage, new StageTransformer());
    }
    /**
     * @api {stage} /stage (create stage)
     * @apiDescription (create stage)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiParam {String} title
     * @apiParam {String} description
     * @apiParam {DateTime} start
     * @apiParam {DateTime} end
     * @apiParam {DateTime} end
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 201 Created
     */
    public function store(Request $request)
    {

        $validator = \Validator::make($request->input(), [
            'name' => 'required|string|max:50',
            'id_event' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        $attributes = $request->all();
        $stage = $this->stage->create($attributes);
        return $this->response
            ->item($stage, new StageTransformer())
            ->setStatusCode(201);

    }

    /**
     * @api {put} /stage/{id} (update post)
     * @apiDescription (update post)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiParam {String} title  post title
     * @apiParam {String} content  post content
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204 NO CONTENT
     */
    public function update($id, Request $request)
    {
        $stage = $this->stage->findOrFail($id);

        $validator = \Validator::make($request->input(), [
            'name' => 'required|string|max:50',
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }

        $stage->name = $request->name;
        if ($request->has('length')) {
            $stage->length = $request->length;
        }

        if ($request->has('width')) {
            $stage->width = $request->width;
        }

        if ($request->has('description')) {
            $stage->description = $request->description;
        }
        $stage->save();
        return $this->response->item($stage, new StageTransformer());

    }

    /**
     * @api {delete} /stage/{id} (delete post)
     * @apiDescription (delete post)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204 NO CONTENT
     */
    public function destroy($id)
    {
        $stage = $this->stage->findOrFail($id);
        $stage->delete();
        //$post->forceDelete(); delete from table
        return $this->response->noContent();
    }
    public function roadbook($id)
    {
        $stage = Stage::find($id);
        $promoter = Promoter::find($stage->event->id_promoter);
        $production = Production::find($stage->event->id_production);
        $filename = time() . str_random(5) . '.pdf';
        $destinationPath = rtrim(app()->basePath('public/images/'));
        PDF::SetTitle('Roadbook');
        $style = '
        <style>
        table {
            width:860px;
        }
        .h2 {
            font-size: 15px;
            font-weight: bold;
            text-align: center;
            border-bottom: 1px solid #30412F;
        }
        .h3 {
            font-size: 13px;
            font-weight: bold;
            text-align: center;
            border-bottom: 1px solid #30412F;
        }
        .h5 {
            font-size: 12px;
            text-align: center;
            border-bottom: 1px solid #30412F;
        }
        .color1h{
            border-radius: 5px;
            background-color: rgb(48, 65, 86);
            color: white;

        }
        .color1b {
            border-radius: 5px;
                 background-color: rgb(48, 65, 86);
                 color: white;
                 width:100px;
           }
        .color1 {
         border-radius: 5px;
        	  background-color: rgb(48, 65, 86);
              color: white;
              width:100px;
        }
        .color2 {
           border-radius: 5px;
            background-color: #f3f3f3;
        }
        </style>';
        $style .= '
        <div class="h2">RoadBook</div>
        <table cellpadding="5" cellspacing="5">
        <tbody>
        <tr>
        <td class="color1">Evento</td>
        <td class="color2">' . $stage->event->name . '</td>
        </tr>
        <tr>
        <td class="color1">Data</td>
        <td class="color2">' . $stage->event->date_begin . '  até ' . $stage->event->date_end . '</td>
        </tr>
        <tr>
        <td class="color1">Morada</td>
        <td class="color2">' . $stage->event->address . '</td>
        </tr>
        <tr>
        <td class="color1">Local</td>
        <td class="color2"><a href="https://maps.google.com/?q=' . $stage->event->latitude . ',' . $stage->event->longitude . '">Mapa</a></td>
        </tr>
        <tr>
        <td class="color1">Promotor</td>
        <td class="color2">' . $promoter->name . '</td>
        </tr>
        <tr>
        <td class="color1">Produção</td>
        <td class="color2">' . $production->name . '</td>
        </tr>
        </tbody>
        </table>';
        $style .=
        '<div class="h3">Palco</div>
        <table cellpadding="5" cellspacing="5" >
        <tbody>
        <tr>
        <td class="color1">Palco</td>
        <td class="color2">' . $stage->name . '</td>
        </tr>
        <tr>
        <td class="color1">Medidas</td>
        <td class="color2">' . $stage->length . ' ' . $stage->width . '</td>
        </tr>
        <tr>
        <td class="color1">Descrição</td>
        <td class="color2">' . $stage->description . '</td>
        </tr>
        </tbody>
        </table>
        ';
  
       
        $techinician = $stage->technician;
        foreach ($techinician as $t) {
            $html = $style;
            $html .=
            '<div class="h3">Técnico ' . $t->name . '</div>
        <table cellpadding="5" cellspacing="5" >
        <tbody>
        <tr>
        <td class="color1">Nome</td>
        <td class="color2">' . $t->name . '</td>
        </tr>
        <tr>
        <td class="color1">Email</td>
        <td class="color2">' . $t->email . '</td>
        </tr>
        <tr>
        <td class="color1">Contacto</td>
        <td class="color2">' . $t->contacto . '</td>
        </tr>
        </tbody>
        </table>
        ';
            $html .=
                '<div class="h5">Agenda</div>
        <table cellpadding="5" cellspacing="5" style="width:540px;">
        <thead>
        <tr>
        <th class="color1h">Descrição</th>
        <th class="color1h size">Início</th>
        <th class="color1h">Fim</th>
        <th class="color1h"> </th>
        </tr>
        </thead>
        <tbody>';
            $schedules = $t->getSchedules($stage->id_stage, $t->id_technician);
            foreach ($schedules as $s) {
                $html .= '
        <tr>
        <td class="color2">' . $s['name'] . '</td>
        <td class="color2">' . Carbon::parse($s['date_begin'])->format('d-m-Y H:m') . '</td>
        <td class="color2">' . Carbon::parse($s['date_end'])->format('d-m-Y H:m') . '</td>
        <td class="color2">' . '</td>
        </tr>';
            }
            $html .= '</tbody>
        </table>
        ';
        PDF::AddPage();
        PDF::writeHTML($html, true, false, true, false, '');

        }
    
        $artist = $stage->artist;
        foreach ($artist as $a) {
            $html = $style;
            $html .=
            '<div class="h3">Artista '.$a->name .'</div>
        <table cellpadding="5" cellspacing="5" >
        <tbody>
        <tr>
        <td class="color1">Nome</td>
        <td class="color2">' . $a->name . '</td>
        </tr>
        <tr>
        <td class="color1">Email</td>
        <td class="color2">' . $a->email . '</td>
        </tr>
        <tr>
        <td class="color1">Contacto</td>
        <td class="color2">' . $a->contact . '</td>
        </tr>
        <tr>
        <td class="color1">Manager</td>
        <td class="color2">' . '-' . '</td>
        </tr>
        </tbody>
        </table>';
            $html .=
                '<div class="h5">Agenda</div>
        <table cellpadding="5" cellspacing="5" style="width:540px;" >
        <thead>
        <tr>
        <th class="color1h">Descrição</th>
        <th class="color1h">Início</th>
        <th class="color1h">Fim</th>
        <th class="color1h"> </th>
        </tr>
        </thead>
        <tbody>';
            $schedules = $a->getSchedules($stage->id_stage, $a->id_artist);
            foreach ($schedules as $s) {
                $html .= '
            <tr>
            <td class="color2">' . $s['name'] . '</td>
            <td class="color2">' . Carbon::parse($s['date_begin'])->format('d-m-Y H:m') . '</td>
            <td class="color2">' . Carbon::parse($s['date_end'])->format('d-m-Y H:m') . '</td>
            <td class="color2">' . '</td>
            </tr>';
            }
            $html .= '</tbody>
        </table>
        ';
        PDF::AddPage();
        PDF::writeHTML($html, true, false, true, false, '');

        }
        PDF::Output('hello_world.pdf');
        // PDF::Output( $destinationPath . $filename,'F');
        return $this->response->array([
            'status_code' => 200,
            'data' => ['file' => $filename, 'path' => url('images/' . $filename)],
        ])->setStatusCode(200);
    }
}
