<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Artist;
use App\Models\ProductionArtist;
use App\Models\User;
use App\Transformers\ArtistTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Notifications\UserApplicationSent;

class ArtistController extends BaseController
{
    private $artist;
    public function __construct(Artist $artist)
    {
        $this->artist = $artist;
    }

    public function index()
    {
        $limit = (app('request')->input('limit') && app('request')->input('limit') <= 50 ? app('request')->input('limit') : 10);
        $pagen = (app('request')->input('page') ? app('request')->input('page') : 1);
        $pagename = (app('request')->input('pagename') ? app('request')->input('pagename') : 'page');
        if (in_array('admin', $this->user()->role) || in_array('superadmin', $this->user()->role)) {
            $artist = $this->artist->paginate($limit, ['*'], 'page', $pagen);
        }
        if (in_array('adminproduction', $this->user()->role) || in_array('production', $this->user()->role)) {
            $restrict = $this->user()->production_member()->first();
            $condition = isset($restrict->id_production) ? $restrict->id_production : null;
            $artist = $this->artist
            ->join('production_artist','production_artist.id_artist','artist.id_artist')
            ->where('id_production', $condition)->paginate($limit, ['*'], $pagename, $pagen);
        }
        if (in_array('manager', $this->user()->role)) {
            $restrict = $this->user()->manager()->first();
            $condition = isset($restrict->id_manager) ? $restrict->id_manager : null;
            $artist = $this->artist->where('id_manager', $condition)->paginate($limit, ['*'], $pagename, $pagen);
        }
        return $this->response->paginator($artist, new ArtistTransformer());
    }

    /**
     * @api {get} /posts/{id} (post detail)
     * @apiDescription (post detail)
     * @apiGroup Post
     * @apiPermission none
     * @apiParam {String='comments','user'} [include]  include
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 200 OK
     *   {
     *     "data": {
     *       "id": 1,
     *       "user_id": 3,
     *       "title": "foo",
     *       "content": "",
     *       "created_at": "2016-03-30 15:36:30",
     *       "user": {
     *         "data": {
     *           "id": 3,
     *           "email": "foo@bar.com1",
     *           "name": "",
     *           "avatar": "",
     *           "created_at": "2016-03-30 15:34:01",
     *           "updated_at": "2016-03-30 15:34:01",
     *           "deleted_at": null
     *         }
     *       },
     *       "comments": {
     *         "data": [
     *           {
     *             "id": 1,
     *             "post_id": 1,
     *             "user_id": 1,
     *             "reply_user_id": 0,
     *             "content": "foobar",
     *             "created_at": "2016-04-06 14:51:34"
     *           }
     *         ],
     *         "meta": {
     *           "total": 1
     *         }
     *       }
     *     }
     *   }
     */
    public function show($id)
    {
        $artist = $this->artist->findOrFail($id);
        $fractal = new \League\Fractal\Manager();
        $fractal->setSerializer(new \League\Fractal\Serializer\DataArraySerializer());
        $fractal->parseIncludes(['manager', 'user']);
        $response = new \League\Fractal\Resource\Item($artist, new ArtistTransformer());
        return $this->response->array($fractal->createData($response)->toArray());
    }
    /*
     */
    public function search(Request $request)
    {
        $artist = $this->artist->customLike($request->all())->orderBy('name')->get();
        return $this->response->item($artist, new ArtistTransformer());
    }
    /**
     * @api {artist} /artist (create artist)
     * @apiDescription (create artist)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiParam {String} title
     * @apiParam {String} description
     * @apiParam {DateTime} start
     * @apiParam {DateTime} end
     * @apiParam {DateTime} end
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 201 Created
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->input(), [
            'name' => 'required|string|max:50',
            'email' => 'required',
        ]);

        if ($validator->fails()) {
            return 2;
            return $this->errorBadRequest($validator);
        }
        \DB::beginTransaction();
        try {
            $userid = null;
            if ($request->has('usercreate') && (bool) $request->usercreate) {
                $validator = \Validator::make($request->all(), [
                    'useremail' => 'required|email|unique:user,email',
                    'username' => 'required|min:3',
                    'active' => 'required',
                ]);
                if ($validator->fails()) {
                    \DB::rollback();
                    return $this->errorBadRequest($validator);
                }
                if ($request->has('userpassword')) {
                    $password = $request->get('userpassword');
                } else {
                    $password =str_random(8);
                }
                if ($request->has('userrole')) {
                    $role = $request->userrole;
                } else { // default role
                    $role = 'artist';
                }
                $user_attributes = [
                    'email' => $request->useremail,
                    'name' => $request->username,
                    'password' =>  Hash::make($password),
                    'role' => $role,
                    'active' => (int) $request->active,
                ];
                $user = User::create($user_attributes);
                $userid = $user['id'];
                $notification_ = array('password' => $password);
                $user->notify(new UserApplicationSent($notification_));
            }
            $attributes['name'] = $request->name;
            $attributes['email'] = $request->email;
            $attributes['id_manager'] = ($request->has('manager') ? $request->manager : null);
            $attributes['id_user'] = $userid;
            $attributes['contact'] = ($request->has('contact') ? $request->contact : '');
            $artist = $this->artist->create($attributes);
            if (in_array('adminproduction', $this->user()->role) || in_array('production', $this->user()->role)) {
                $restrict = $this->user()->production_member()->first();
                $condition = isset($restrict->id_production) ? $restrict->id_production : null;
                ProductionArtist::create(['id_artist' => $artist['id_artist'], 'id_production' => $condition]);
            }
            \DB::commit();
            return $this->response
                ->item($artist, new ArtistTransformer())
                ->setStatusCode(201);
        } catch (\Exception $e) {
            \DB::rollback();
            if ($validator->fails()) {
                return $this->errorBadRequest($validator);
            }
            return $this->response->array([
                'status_code' => 400,
                'message' => $e->getMessage(),
            ])->setStatusCode(400);
        } catch (\Throwable $e) {
            \DB::rollback();
            return $this->response->array([
                'status_code' => 400,
                'message' => $e->getMessage(),
            ])->setStatusCode(400);

        }
    }

    /**
     * @api {put} /artist/{id} (update post)
     * @apiDescription (update post)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiParam {String} title  post title
     * @apiParam {String} content  post content
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204 NO CONTENT
     */
    public function update($id, Request $request)
    {
        $artist = $this->artist->findOrFail($id);

        $validator = \Validator::make($request->input(), [
            'name' => 'required|string|max:50',
            'email' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        \DB::beginTransaction();
        try {
            $userid = $artist->id_user;
            if ($request->has('usercreate') && (bool) $request->usercreate) { //create user true
                if ($request->has('userpassword')) {
                    $password = $request->get('userpassword');
                } else {
                    $password = str_random(8);
                }
                if ($request->has('userrole')) {
                    $role = $request->userrole;
                } else { // default role
                    $role = 'user';
                }
            
            if (isset($userid)) { // update current user
                $validator = \Validator::make($request->all(), [
                    'useremail' => 'required|email|unique:user,email,'.$userid,
                    'username' => 'required|min:3',
                    'active' => 'required',
                ]);
                if ($validator->fails()) {
                    \DB::rollback();
                    return $this->errorBadRequest($validator);
                }
                $user = User::findOrFail($userid);
                $user->email = $request->useremail;
                $user->name = $request->username;
                $user->password = $password;
                $user->role = $role;
                $user->active = (int) $request->active;
                $user->save();
            } else { //create user
                $validator = \Validator::make($request->all(), [
                    'useremail' => 'required|email|unique:user,email',
                    'username' => 'required|min:3',
                    'active' => 'required',
                ]);
                if ($validator->fails()) {
                    \DB::rollback();
                    return $this->errorBadRequest($validator);
                }
                $user_attributes = [
                    'email' => $request->useremail,
                    'name' => $request->username,
                    'password' => Hash::make($password),
                    'role' => $role,
                    'active' => (int) $request->active,
                ];
                $user = User::create($user_attributes);
                $userid = $user['id'];
                $notification_ = array('password' => $password);
                $user->notify(new UserApplicationSent($notification_));
            }
            }
            $artist->name = $request->name;
            $artist->email = $request->email;
            $artist->id_user = $userid;
            $artist->contact = ($request->has('contact') ? $request->contact : '');
            $artist->id_manager = ($request->has('manager') ? $request->manager : null);
            $artist->save();
            \DB::commit();
            return $this->response->item($artist, new ArtistTransformer());
        } catch (\Exception $e) {
            \DB::rollback();
            if ($validator->fails()) {
                return $this->errorBadRequest($validator);
            }
            return $this->response->array([
                'status_code' => 400,
                'message' => $e->getMessage(),
            ])->setStatusCode(400);
        } catch (\Throwable $e) {
            \DB::rollback();
            return $this->response->array([
                'status_code' => 400,
                'message' => $e->getMessage(),
            ])->setStatusCode(400);

        }
    }

    /**
     * @api {delete} /artist/{id} (delete post)
     * @apiDescription (delete post)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204 NO CONTENT
     */
    public function destroy($id)
    {
        $artist = $this->artist->findOrFail($id);
        if (in_array('adminproduction', $this->user()->role) || in_array('production', $this->user()->role)) {
            $restrict = $this->user()->production_member()->first();
            $condition = isset($restrict->id_production) ? $restrict->id_production : null;
            ProductionArtist::where('id_artist', $artist['id_artist'])->orWhere('id_production', $condition)->delete();
        }else {
            $artist->delete();
        }
        //$post->forceDelete(); delete from table
        return $this->response->noContent();
    }
}
