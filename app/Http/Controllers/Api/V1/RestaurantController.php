<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Restaurant;
use Illuminate\Http\Request;
use App\Transformers\RestaurantTransformer;
use Carbon\Carbon;

class RestaurantController extends BaseController
{
    private $restaurant;

    public function __construct(Restaurant $restaurant)
    {
        $this->restaurant = $restaurant;
    }

    public function index()
    {
		$limit = (app('request')->input('limit') && app('request')->input('limit') <= 50 ? app('request')->input('limit') : 10) ;
		$pagen = (app('request')->input('page') ? app('request')->input('page') : 1);
		$pagename = (app('request')->input('pagename') ? app('request')->input('pagename') : 'page') ;
		$restaurant = $this->restaurant->paginate($limit,['*'], 'page',$pagen);    
        return $this->response->paginator($restaurant, new RestaurantTransformer());
    }


    /**
     * @api {get} /posts/{id} (post detail)
     * @apiDescription (post detail)
     * @apiGroup Post
     * @apiPermission none
     * @apiParam {String='comments','user'} [include]  include
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 200 OK
     *   {
     *     "data": {
     *       "id": 1,
     *       "user_id": 3,
     *       "title": "foo",
     *       "content": "",
     *       "created_at": "2016-03-30 15:36:30",
     *       "user": {
     *         "data": {
     *           "id": 3,
     *           "email": "foo@bar.com1",
     *           "name": "",
     *           "avatar": "",
     *           "created_at": "2016-03-30 15:34:01",
     *           "updated_at": "2016-03-30 15:34:01",
     *           "deleted_at": null
     *         }
     *       },
     *       "comments": {
     *         "data": [
     *           {
     *             "id": 1,
     *             "post_id": 1,
     *             "user_id": 1,
     *             "reply_user_id": 0,
     *             "content": "foobar",
     *             "created_at": "2016-04-06 14:51:34"
     *           }
     *         ],
     *         "meta": {
     *           "total": 1
     *         }
     *       }
     *     }
     *   }
     */
    public function show($id)
    {
		
        $restaurant = $this->restaurant->findOrFail($id);

        return $this->response->item($restaurant, new RestaurantTransformer());
    }
	/*
	*/
	public function search(Request $request)
    {
		$restaurant = $this->restaurant->customLike($request->all())->orderBy('name')->get();
		return $this->response->item($restaurant, new RestaurantTransformer());
    }
    /**
     * @api {restaurant} /restaurant (create restaurant)
     * @apiDescription (create restaurant)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiParam {String} title
     * @apiParam {String} description
	 * @apiParam {DateTime} start 
	 * @apiParam {DateTime} end
	 * @apiParam {DateTime} end
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 201 Created
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->input(), [
            'name' => 'required|string|max:50',
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }

        $attributes['name'] = $request->name;
		if($request->has('lat') && $request->has('lng')){
			$attributes['latitude']=$request->lat;
			$attributes['longitude']=$request->lng;	
		}
		$attributes['address'] = ($request->has('address')? $request->address :'');
		$attributes['website'] = ($request->has('website')? $request->website :'');
		$attributes['contact'] = ($request->has('contact')? $request->contact :'');
        $restaurant = $this->restaurant->create($attributes);
		//return $start;
        // Return 201 plus data
        return $this->response
            ->item($restaurant, new RestaurantTransformer())
            ->setStatusCode(201);
    }

    /**
     * @api {put} /restaurant/{id} (update post)
     * @apiDescription (update post)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiParam {String} title  post title
     * @apiParam {String} content  post content
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204 NO CONTENT
     */
    public function update($id, Request $request)
    {
        $restaurant = $this->restaurant->findOrFail($id);
		
        $validator = \Validator::make($request->input(), [
            'name' => 'required|string|max:50',
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
		$restaurant->name = $request->name;
        $restaurant->address = ($request->has('address')?$request->address:'');
		$restaurant->website = ($request->has('website')?$request->website:'');
		$restaurant->contact = ($request->has('contact')?$request->contact:'');
		if($request->has('lat') && $request->has('lng')){
			$restaurant->latitude=$request->lat;
			$restaurant->longitude=$request->lng;	
		}
        $restaurant->save();


        return $this->response->item($restaurant, new RestaurantTransformer());
    }

    /**
     * @api {delete} /restaurant/{id} (delete post)
     * @apiDescription (delete post)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204 NO CONTENT
     */
    public function destroy($id)
    {
        $restaurant = $this->restaurant->findOrFail($id);
        $restaurant->delete();
		//$post->forceDelete(); delete from table
        return $this->response->noContent();
    }
}
