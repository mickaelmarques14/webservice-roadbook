<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Hotel;
use Illuminate\Http\Request;
use App\Transformers\HotelTransformer;
use Carbon\Carbon;

class HotelController extends BaseController
{
    private $hotel;

    public function __construct(Hotel $hotel)
    {
        $this->hotel = $hotel;
    }

    public function index()
    {
		$limit = (app('request')->input('limit') && app('request')->input('limit') <= 50 ? app('request')->input('limit') : 10) ;
		$pagen = (app('request')->input('page') ? app('request')->input('page') : 1);
		$pagename = (app('request')->input('pagename') ? app('request')->input('pagename') : 'page') ;
		$hotel = $this->hotel->paginate($limit,['*'], 'page',$pagen);    
        return $this->response->paginator($hotel, new HotelTransformer());
    }


    /**
     * @api {get} /posts/{id} (post detail)
     * @apiDescription (post detail)
     * @apiGroup Post
     * @apiPermission none
     * @apiParam {String='comments','user'} [include]  include
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 200 OK
     *   {
     *     "data": {
     *       "id": 1,
     *       "user_id": 3,
     *       "title": "foo",
     *       "content": "",
     *       "created_at": "2016-03-30 15:36:30",
     *       "user": {
     *         "data": {
     *           "id": 3,
     *           "email": "foo@bar.com1",
     *           "name": "",
     *           "avatar": "",
     *           "created_at": "2016-03-30 15:34:01",
     *           "updated_at": "2016-03-30 15:34:01",
     *           "deleted_at": null
     *         }
     *       },
     *       "comments": {
     *         "data": [
     *           {
     *             "id": 1,
     *             "post_id": 1,
     *             "user_id": 1,
     *             "reply_user_id": 0,
     *             "content": "foobar",
     *             "created_at": "2016-04-06 14:51:34"
     *           }
     *         ],
     *         "meta": {
     *           "total": 1
     *         }
     *       }
     *     }
     *   }
     */
    public function show($id)
    {
		
        $hotel = $this->hotel->findOrFail($id);

        return $this->response->item($hotel, new HotelTransformer());
    }
	/** 
	*
	*
	*
	*
	*
	*/
	public function search(Request $request)
    {
		$hotel = $this->hotel->customLike($request->all())->orderBy('name')->get();
		return $this->response->item($hotel, new HotelTransformer());
    }
    /**
     * @api {hotel} /hotel (create hotel)
     * @apiDescription (create hotel)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiParam {String} title
     * @apiParam {String} description
	 * @apiParam {DateTime} start 
	 * @apiParam {DateTime} end
	 * @apiParam {DateTime} end
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 201 Created
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->input(), [
            'name' => 'required|string|max:50',
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }

        $attributes['name'] = $request->name;
		if($request->has('lat') && $request->has('lng')){
			$attributes['latitude']=$request->lat;
			$attributes['longitude']=$request->lng;	
		}
		$attributes['address'] = ($request->has('address')? $request->address :'');
		$attributes['website'] = ($request->has('website')? $request->website :'');
		$attributes['facebook'] = ($request->has('facebook')? $request->facebook :'');
		$attributes['contact'] = ($request->has('contact')? $request->contact :'');
		$attributes['email'] = ($request->has('email')? $request->email :'');
		$attributes['rating'] = ($request->has('rating')? $request->rating :'');
        $hotel = $this->hotel->create($attributes);
	        // Return 201 plus data
        return $this->response
            ->item($hotel, new HotelTransformer())
            ->setStatusCode(201);
    }

    /**
     * @api {put} /hotel/{id} (update post)
     * @apiDescription (update post)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiParam {String} title  post title
     * @apiParam {String} content  post content
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204 NO CONTENT
     */
    public function update($id, Request $request)
    {
        $hotel = $this->hotel->findOrFail($id);
		
        $validator = \Validator::make($request->input(), [
            'name' => 'required|string|max:50',
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
		$hotel->name = $request->name;
        $hotel->address = ($request->has('address')?$request->address:'');
		$hotel->website = ($request->has('website')?$request->website:'');
		$hotel->facebook = ($request->has('facebook')?$request->facebook:'');
		$hotel->email = ($request->has('email')?$request->email:'');
		$hotel->contact = ($request->has('contact')?$request->contact:'');
		$hotel->rating = ($request->has('rating')?$request->rating:'');
		if($request->has('lat') && $$request->has('lng')){
			$hotel->latitude=$request->lat;
			$hotel->longitude=$request->lng;	
		}
        $hotel->save();


        return $this->response->item($hotel, new HotelTransformer());
    }

    /**
     * @api {delete} /hotel/{id} (delete post)
     * @apiDescription (delete post)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204 NO CONTENT
     */
    public function destroy($id)
    {
        $hotel = $this->hotel->findOrFail($id);
        $hotel->delete();
		//$post->forceDelete(); delete from table
        return $this->response->noContent();
    }
}
