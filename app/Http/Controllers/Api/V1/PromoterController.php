<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Promoter;
use App\Models\ProductionPromoter;
use App\Models\User;
use App\Notifications\UserApplicationSent;
use App\Transformers\PromoterTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class PromoterController extends BaseController
{
    private $promoter;
    public $listfields = ['user.email as useremail','user.name as username','promoter.id_promoter',
    'promoter.name', 'promoter.email', 'promoter.address', 'promoter.latitude', 'promoter.longitude', 'promoter.id_user', 'promoter.contact'];
    public function __construct(Promoter $promoter)
    {
        $this->promoter = $promoter;
    }

    public function index()
    {
        $limit = (app('request')->input('limit') && app('request')->input('limit') <= 50 ? app('request')->input('limit') : 10);
        $pagen = (app('request')->input('page') ? app('request')->input('page') : 1);
        $pagename = (app('request')->input('pagename') ? app('request')->input('pagename') : 'page');
        if (in_array('admin', $this->user()->role) || in_array('superadmin', $this->user()->role)) {
            $promoter = $this->promoter->paginate($limit, ['*'], 'page', $pagen);
        }
        if (in_array('adminproduction', $this->user()->role) || in_array('production', $this->user()->role)) {
            $restrict = $this->user()->production_member()->first();
            $condition = isset($restrict->id_production) ? $restrict->id_production : null;
            $promoter = $this->promoter
            ->join('production_promoter','production_promoter.id_promoter','promoter.id_promoter')
            ->where('id_production', $condition)->paginate($limit, ['*'], $pagename, $pagen);
        }
        return $this->response->paginator($promoter, new PromoterTransformer());
    }

    /**
     * @api {get} /promoter/{id} (promoter detail)
     * @apiDescription (promoter detail)
     * @apiGroup Promoter
     * @apiPermission none
     * @apiParam {'user'} [include]  include
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 200 OK
     *   {
     *     "data": {
     *       "id": 1,
     *       "user_id": 3,
     *       "title": "foo",
     *       "content": "",
     *       "created_at": "2016-03-30 15:36:30",
     *       "user": {
     *         "data": {
     *           "id": 3,
     *           "email": "foo@bar.com1",
     *           "name": "",
     *           "avatar": "",
     *           "created_at": "2016-03-30 15:34:01",
     *           "updated_at": "2016-03-30 15:34:01",
     *           "deleted_at": null
     *         }
     *       },
     *       "user": {
     *         "data": [
     *           {
     *             "id": 1,
     *             "emial": 1,
     *             "user_id": 1,
     *             "reply_user_id": 0,
     *             "content": "foobar",
     *             "created_at": "2016-04-06 14:51:34"
     *           }
     *         ],
     *         "meta": {
     *           "total": 1
     *         }
     *       }
     *     }
     *   }
     */
    public function show($id)
    {

        $promoter = $this->promoter->findOrFail($id);

        return $this->response->item($promoter, new PromoterTransformer());
    }
    public function users($id)
    {

        $promoter = $this->promoter->userLeftJoin()->select($this->listfields)->findOrFail($id);

        return $this->response->item($promoter, new PromoterTransformer());
    }
    /**
     *
     *
     *
     *
     *
     */
    public function search(Request $request)
    {
        $promoter = $this->promoter->customLike($request->all())->orderBy('name')->get();
        return $this->response->item($promoter, new PromoterTransformer());
    }
    /**
     * @api {promoter} /promoter (create promoter)
     * @apiDescription (create promoter)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiParam {String} title
     * @apiParam {String} description
     * @apiParam {DateTime} start
     * @apiParam {DateTime} end
     * @apiParam {DateTime} end
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 201 Created
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->input(), [
            'name' => 'required|string|max:50',
            'email' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        \DB::beginTransaction();
        try {
            $userid = null;
            if ($request->has('usercreate') && (bool) $request->usercreate) { //create user true
                $validator = \Validator::make($request->all(), [
                    'useremail' => 'required|email',
                    'username' => 'required|min:3',
                    'active' => 'required',
                ]);
                if ($validator->fails()) {
                    \DB::rollback();
                    return $this->errorBadRequest($validator);
                }
                $userexists = User::where('email',  $request->useremail)->first();
                if($userexists) {
                    //notify connection
                    $userid =$userexists->id;
                } else {
                    if ($request->has('userpassword')) {
                        $password = $request->get('userpassword');
                    } else {
                        $password = str_random(8);
                    }
                    if ($request->has('userrole')) {
                        $role = $request->userrole;
                    } else { // default role
                        $role = 'promoter';
                    }
                    $user_attributes = [
                        'email' => $request->useremail,
                        'name' => $request->username,
                        'password' => Hash::make($password),
                        'role' => $role,
                        'active' => (int) $request->active,
                    ];
                    $user = User::create($user_attributes);
                    $userid = $user['id'];
                    $notification_ = array('password' => $password);
                    $user->notify(new UserApplicationSent($notification_));
                }
            }
            $attributes['id_user'] = $userid;
            $attributes['name'] = $request->name;
            if ($request->has('contact')) {
                $attributes['contact'] = $request->contact;
            }

            if ($request->has('email')) {
                $attributes['email'] = $request->email;
            }

            if ($request->has('lat') && $request->has('lng')) {
                $attributes['latitude'] = $request->lat;
                $attributes['longitude'] = $request->lng;
            }
            if ($request->has('address')) {
                $attributes['address'] = $request->address;
            }

            $promoter = $this->promoter->create($attributes);
            if (in_array('adminproduction', $this->user()->role) || in_array('production', $this->user()->role)) {
                $restrict = $this->user()->production_member()->first();
                $condition = isset($restrict->id_production) ? $restrict->id_production : null;
                ProductionPromoter::create(['id_promoter' => $promoter['id_promoter'], 'id_production' => $condition]);
            }
            \DB::commit();
            return $this->response
                ->item($promoter, new PromoterTransformer())
                ->setStatusCode(201);
        } catch (\Exception $e) {
            \DB::rollback();
            if ($validator->fails()) {
                return $this->errorBadRequest($validator);
            }
            return $this->response->array([
                'status_code' => 400,
                'message' => $e->getMessage(),
            ])->setStatusCode(400);
        } catch (\Throwable $e) {
            \DB::rollback();
            return $this->response->array([
                'status_code' => 400,
                'message' => $e->getMessage(),
            ])->setStatusCode(400);

        }

        //return $start;
        // Return 201 plus data
        return $this->response
            ->item($promoter, new PromoterTransformer())
            ->setStatusCode(201);
    }

    /**
     * @api {put} /promoter/{id} (update post)
     * @apiDescription (update post)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiParam {String} title  post title
     * @apiParam {String} content  post content
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204 NO CONTENT
     */
    public function update($id, Request $request)
    {
        $promoter = $this->promoter->findOrFail($id);

        $validator = \Validator::make($request->input(), [
            'name' => 'required|string|max:50',
            'email' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        \DB::beginTransaction();
        try {
            $userid = $promoter->id_user;
            if ($request->has('usercreate') && (bool) $request->usercreate) { //create user true
                if ($request->has('userpassword')) {
                    $password = $request->get('userpassword');
                } else {
                    $password = str_random(8);
                }
                if ($request->has('userrole')) {
                    $role = $request->userrole;
                } else { // default role
                    $role = 'user';
                }
           
            if (isset($userid)) { // update current user
                $validator = \Validator::make($request->all(), [
                    'useremail' => 'required|email|unique:user,email',
                    'username' => 'required|min:3',
                    'active' => 'required',
                ]);
                if ($validator->fails()) {
                    \DB::rollback();
                    return $this->errorBadRequest($validator);
                }
                $user = User::findOrFail($userid);
                $user->email = $request->useremail;
                $user->name = $request->username;
                $user->password = $password;
                $user->role = $role;
                $user->active = (int) $request->active;
                $user->save();
            } else { //create user
                $validator = \Validator::make($request->all(), [
                    'useremail' => 'required|email|unique:user,email,'.$userid,
                    'username' => 'required|min:3',
                    'active' => 'required',
                ]);
                if ($validator->fails()) {
                    \DB::rollback();
                    return $this->errorBadRequest($validator);
                }
                $user_attributes = [
                    'email' => $request->useremail,
                    'name' => $request->username,
                    'password' => Hash::make($password),
                    'role' => $role,
                    'active' => (int) $request->active,
                ];
                $user = User::create($user_attributes);
                $userid = $user['id'];
                $notification_ = array('password' => $password);
                $user->notify(new UserApplicationSent($notification_));
            }
        }
            $promoter->id_user = $userid;
            $promoter->name = $request->name;
            if ($request->has('contact')) {
                $promoter->contact = $request->contact;
            }

            if ($request->has('email')) {
                $promoter->email = $request->email;
            }

            if ($request->has('address')) {
                $promoter->address = $request->address;
            }

            if ($request->has('lat') && $request->has('lng')) {
                $promoter->latitude = $request->lat;
                $promoter->longitude = $request->lng;
            }
            $promoter->save();
        
            \DB::commit();
            return $this->response->item($promoter, new PromoterTransformer());
        } catch (\Exception $e) {
            \DB::rollback();
            if ($validator->fails()) {
                return $this->errorBadRequest($validator);
            }
            return $this->response->array([
                'status_code' => 400,
                'message' => $e->getMessage(),
            ])->setStatusCode(400);
        } catch (\Throwable $e) {
            \DB::rollback();
            return $this->response->array([
                'status_code' => 400,
                'message' => $e->getMessage(),
            ])->setStatusCode(400);

        }

        return $this->response->item($promoter, new PromoterTransformer());
    }

    /**
     * @api {delete} /promoter/{id} (delete post)
     * @apiDescription (delete post)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204 NO CONTENT
     */
    public function destroy($id)
    {
        $promoter = $this->promoter->findOrFail($id);
        if (in_array('adminproduction', $this->user()->role) || in_array('production', $this->user()->role)) {
            $restrict = $this->user()->production_member()->first();
            $condition = isset($restrict->id_production) ? $restrict->id_production : null;
            ProductionPromoter::where('id_promoter', $promoter['id_promoter'])->orWhere('id_production', $condition)->delete();
        }else {
            $promoter->delete();
        }
     
        //$post->forceDelete(); delete from table
        return $this->response->noContent();
    }
}
