<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Technician;
use App\Models\ProductionTechnician;
use App\Models\User;
use App\Notifications\UserApplicationSent;
use App\Transformers\TechnicianTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class TechnicianController extends BaseController
{
    private $technician;

    public function __construct(Technician $technician)
    {
        $this->technician = $technician;
    }

    public function index()
    {
        $limit = (app('request')->input('limit') && app('request')->input('limit') <= 50 ? app('request')->input('limit') : 10);
        $pagen = (app('request')->input('page') ? app('request')->input('page') : 1);
        $pagename = (app('request')->input('pagename') ? app('request')->input('pagename') : 'page');
        $technician = $this->technician->paginate($limit, ['*'], 'page', $pagen);
        return $this->response->paginator($technician, new TechnicianTransformer());
    }

    /**
     * @api {get} /posts/{id} (post detail)
     * @apiDescription (post detail)
     * @apiGroup Post
     * @apiPermission none
     * @apiParam {String='comments','user'} [include]  include
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 200 OK
     *   {
     *     "data": {
     *       "id": 1,
     *       "user_id": 3,
     *       "title": "foo",
     *       "content": "",
     *       "created_at": "2016-03-30 15:36:30",
     *       "user": {
     *         "data": {
     *           "id": 3,
     *           "email": "foo@bar.com1",
     *           "name": "",
     *           "avatar": "",
     *           "created_at": "2016-03-30 15:34:01",
     *           "updated_at": "2016-03-30 15:34:01",
     *           "deleted_at": null
     *         }
     *       },
     *       "comments": {
     *         "data": [
     *           {
     *             "id": 1,
     *             "post_id": 1,
     *             "user_id": 1,
     *             "reply_user_id": 0,
     *             "content": "foobar",
     *             "created_at": "2016-04-06 14:51:34"
     *           }
     *         ],
     *         "meta": {
     *           "total": 1
     *         }
     *       }
     *     }
     *   }
     */
    public function show($id)
    {

        $technician = $this->technician->findOrFail($id);

        return $this->response->item($technician, new TechnicianTransformer());
    }
    /*
     */
    public function search(Request $request)
    {
        $technician = $this->technician->customLike($request->all())->orderBy('name')->get();
        return $this->response->item($technician, new TechnicianTransformer());
    }
    /**
     * @api {technician} /technician (create technician)
     * @apiDescription (create technician)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiParam {String} title
     * @apiParam {String} description
     * @apiParam {DateTime} start
     * @apiParam {DateTime} end
     * @apiParam {DateTime} end
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 201 Created
     */
    public function store(Request $request)
    {

        $validator = \Validator::make($request->input(), [
            'name' => 'required|string|max:50',
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        \DB::beginTransaction();
        try {
            $attributes['name'] = $request->name;
            $attributes['email'] = $request->email;
            $attributes['contact'] = ($request->has('contact') ? $request->contact : '');

            $technician = $this->technician->create($attributes);
            if ($request->has('usercreate') && (bool) $request->usercreate) {
                $validator = \Validator::make($request->all(), [
                    'useremail' => 'required|email|unique:user,email',
                    'username' => 'required|min:3',
                    'active' => 'required',
                ]);
                if ($validator->fails()) {
                    \DB::rollback();
                    return $this->errorBadRequest($validator);
                }
                if ($request->has('userpassword')) {
                    $password = $request->get('userpassword');
                } else {
                    $password = str_random(8);
                }
                if ($request->has('userrole')) {
                    $role = $request->userrole;
                } else { // default role
                    $role = 'technician';
                }
                $user_attributes = [
                    'email' => $request->useremail,
                    'name' => $request->username,
                    'password' => Hash::make($password),
                    'role' => $role,
                    'active' => (int) $request->active,
                ];
                $user = User::create($user_attributes);
                $userid = $user['id'];
                $notification_ = array('password' => $password);
                
                

                //notification
                $user->notify(new UserApplicationSent($notification_));

            }
            if (in_array('adminproduction', $this->user()->role) || in_array('production', $this->user()->role)) {
                $restrict = $this->user()->production_member()->first();
                $condition = isset($restrict->id_production) ? $restrict->id_production : null;
                ProductionTechnician::create(['id_technician' => $technician['id_technician'], 'id_production' => $condition]);
            }
            \DB::commit();
            // Return 201 plus data
            return $this->response
                ->item($technician, new TechnicianTransformer())
                ->setStatusCode(201);
        } catch (\Exception $e) {
            \DB::rollback();
            return $this->response->array([
                'status_code' => 400,
                'message' => $e,
            ])->setStatusCode(400);
        } catch (\Throwable $e) {
            \DB::rollback();
            return $this->response->array([
                'status_code' => 400,
                'message' => $e->getMessage(),
            ])->setStatusCode(400);

        }

    }

    /**
     * @api {put} /technician/{id} (update post)
     * @apiDescription (update post)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiParam {String} title  post title
     * @apiParam {String} content  post content
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204 NO CONTENT
     */
    public function update($id, Request $request)
    {
        $technician = $this->technician->findOrFail($id);

        $validator = \Validator::make($request->input(), [
            'name' => 'required|string',
            'email' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        $technician->name = $request->name;
        $technician->email = $request->email;
        if ($request->has('contact')) {
            $technician->contact = $request->contact;
        }

        $technician->save();
        return $this->response->item($technician, new TechnicianTransformer());
    }

    /**
     * @api {delete} /technician/{id} (delete post)
     * @apiDescription (delete post)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204 NO CONTENT
     */
    public function destroy($id)
    {
        $technician = $this->technician->findOrFail($id);
        $technician->delete();
        //$post->forceDelete(); delete from table
        return $this->response->noContent();
    }
}
