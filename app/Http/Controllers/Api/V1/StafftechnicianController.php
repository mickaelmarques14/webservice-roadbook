<?php
namespace App\Http\Controllers\Api\V1;

use App\Models\Stafftechnician;
use App\Models\User;
use App\Notifications\UserApplicationSent;
use App\Transformers\StafftechnicianTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class StafftechnicianController extends BaseController
{
    private $stafftechnician;
    private $listfields = [
        'stafftechnician.id_stafftechnician', 'stafftechnician.name', 'stafftechnician.email', 'stafftechnician.contact', 'stafftechnician.id_user',
        'technician.id_technician', 'technician.name as technician'];
    private $listfields2 = [
        'stafftechnician.id_stafftechnician', 'stafftechnician.name', 'stafftechnician.email', 'stafftechnician.contact', 'stafftechnician.id_user',
        'user.role'];
    private $listfields3 = [
        'stafftechnician.id_stafftechnician', 'stafftechnician.name', 'stafftechnician.email', 'stafftechnician.contact', 'stafftechnician.id_user',
        'user.email as useremail','user.name as username'];
    public function __construct(Stafftechnician $stafftechnician)
    {
        $this->stafftechnician = $stafftechnician;
    }
    public function index()
    {
        $limit = (app('request')->input('limit') && app('request')->input('limit') <= 50 ? app('request')->input('limit') : 10);
        $pagen = (app('request')->input('page') ? app('request')->input('page') : 1);
        $pagename = (app('request')->input('pagename') ? app('request')->input('pagename') : 'page');
        $stafftechnician = $this->stafftechnician->technicianJoin()->paginate($limit, $this->listfields, 'page', $pagen);
        return $this->response->paginator($stafftechnician, new StafftechnicianTransformer());
    }

  
    /**
     * @api {get} /posts/{id} (post detail)
     * @apiDescription (post detail)
     * @apiGroup Post
     * @apiPermission none
     * @apiParam {String='comments','user'} [include]  include
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 200 OK
     *   {
     *     "data": {
     *       "id": 1,
     *       "user_id": 3,
     *       "title": "foo",
     *       "content": "",
     *       "created_at": "2016-03-30 15:36:30",
     *       "user": {
     *         "data": {
     *           "id": 3,
     *           "email": "foo@bar.com1",
     *           "name": "",
     *           "avatar": "",
     *           "created_at": "2016-03-30 15:34:01",
     *           "updated_at": "2016-03-30 15:34:01",
     *           "deleted_at": null
     *         }
     *       },
     *       "comments": {
     *         "data": [
     *           {
     *             "id": 1,
     *             "post_id": 1,
     *             "user_id": 1,
     *             "reply_user_id": 0,
     *             "content": "foobar",
     *             "created_at": "2016-04-06 14:51:34"
     *           }
     *         ],
     *         "meta": {
     *           "total": 1
     *         }
     *       }
     *     }
     *   }
     */
    public function show($id)
    {

        $stafftechnician = $this->stafftechnician->technicianJoin()->select($this->listfields)->findOrFail($id);
        return $this->response->item($stafftechnician, new StafftechnicianTransformer());
    }

    /**
     *
     *
     *
     *
     *
     */
    public function search(Request $request)
    {

        $stafftechnician = $this->stafftechnician->customLike($request->except('id_technician'))->where('id_technician',$request->only('id_technician'))->orderBy('name')->get();
        return $this->response->item($stafftechnician, new StafftechnicianTransformer());
    }
    /**
     * @api {stafftechnician} /stafftechnician (create stafftechnician)
     * @apiDescription (create stafftechnician)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiParam {String} title
     * @apiParam {String} description
     * @apiParam {DateTime} start
     * @apiParam {DateTime} end
     * @apiParam {DateTime} end
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 201 Created
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->input(), [
            'name' => 'required|string|max:50',
            'email' => 'required|email',
        ]);
        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        \DB::beginTransaction();
        try {
            $userid = null;
            if ($request->has('usercreate') && (bool) $request->usercreate) { //create user true
                $validator = \Validator::make($request->all(), [
                    'useremail' => 'required|email|unique:user,email',
                    'username' => 'required|min:3',
                    'active' => 'required',
                ]);
                if ($validator->fails()) {
                    \DB::rollback();
                    return $this->errorBadRequest($validator);
                }
                if ($request->has('userpassword')) {
                    $password = $request->get('userpassword');
                } else {
                    $password = str_random(8);
                }
                if ($request->has('userrole')) {
                    $role = $request->userrole;
                } else { // default role
                    $role = 'user';
                }
                $user_attributes = [
                    'email' => $request->useremail,
                    'name' => $request->username,
                    'password' => Hash::make($password),
                    'role' => $role,
                    'active' => (int) $request->active,
                ];
                $user = User::create($user_attributes);
                $userid = $user['id'];
                $notification_ = array('password' => $password);
                $user->notify(new UserApplicationSent($notification_));
            }
            $attributes['name'] = $request->name;
            $attributes['email'] = $request->email;
            $attributes['id_production'] = ($request->has('production') ? $request->production : null);
            $attributes['id_user'] = $userid;
            $attributes['contact'] = ($request->has('contact') ? $request->contact : '');
            $stafftechnician = $this->stafftechnician->create($attributes);
            \DB::commit();
            return $this->response
                ->item($stafftechnician, new StafftechnicianTransformer())
                ->setStatusCode(201);
        } catch (\Exception $e) {
            \DB::rollback();
            if ($validator->fails()) {
                return $this->errorBadRequest($validator);
            }
            return $this->response->array([
                'status_code' => 400,
                'message' => $e->getMessage(),
            ])->setStatusCode(400);
        } catch (\Throwable $e) {
            \DB::rollback();
            return $this->response->array([
                'status_code' => 400,
                'message' => $e->getMessage(),
            ])->setStatusCode(400);
        }
    }
    /**
     * @api {put} /stafftechnician/{id} (update post)
     * @apiDescription (update post)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiParam {String} title  post title
     * @apiParam {String} content  post content
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204 NO CONTENT
     */
    public function update($id, Request $request)
    {
        $stafftechnician = $this->stafftechnician->findOrFail($id);

        $validator = \Validator::make($request->input(), [
            'name' => 'required|string|max:50',
            'email' => 'required|email',
        ]);
        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        \DB::beginTransaction();
        try {

                $userid = $stafftechnician->id_user;
            if ($request->has('usercreate') && (bool) $request->usercreate) { //create user true
             
                if ($request->has('userpassword')) {
                    $password = $request->get('userpassword');
                } else {
                    $password = str_random(8);
                }
                if ($request->has('userrole')) {
                    $role = $request->userrole;
                } else { // default role
                    $role = 'user';
                }
                if (isset($userid)) { // update current user
                    $validator = \Validator::make($request->all(), [
                        'useremail' => 'required|email|unique:user,email,'.$userid,
                        'username' => 'required|min:3',
                        'active' => 'required',
                    ]);
                    if ($validator->fails()) {
                        \DB::rollback();
                        return $this->errorBadRequest($validator);
                    }
                    $user = User::findOrFail($userid);
                    $user->email = $request->useremail;
                    $user->name = $request->username;
                    $user->password = $password;
                    $user->role = $role;
                    $user->active = (int) $request->active;
                    $user->save();
                } else { //create user
                    $validator = \Validator::make($request->all(), [
                        'useremail' => 'required|email|unique:user,email',
                        'username' => 'required|min:3',
                        'active' => 'required',
                    ]);
                    if ($validator->fails()) {
                        \DB::rollback();
                        return $this->errorBadRequest($validator);
                    }
                    $user_attributes = [
                        'email' => $request->useremail,
                        'name' => $request->username,
                        'password' => Hash::make($password),
                        'role' => $role,
                        'active' => (int) $request->active,
                    ];
                    $user = User::create($user_attributes);
                    $userid = $user['id'];
                    $notification_ = array('password' => $password);
                    $user->notify(new UserApplicationSent($notification_));
                }
                $stafftechnician->id_user = $userid;
            }
            $stafftechnician->name = $request->name;
            $stafftechnician->email = $request->email;

            $stafftechnician->contact = ($request->has('contact') ? $request->contact : '');
            $stafftechnician->save();
            \DB::commit();
            return $this->response->item($stafftechnician, new StafftechnicianTransformer());
        } catch (\Exception $e) {
            \DB::rollback();
            if ($validator->fails()) {
                return $this->errorBadRequest($validator);
            }

            return $this->response->array([
                'status_code' => 400,
                'message' => $e->getMessage(),
            ])->setStatusCode(400);
        } catch (\Throwable $e) {
            \DB::rollback();
            return $this->response->array([
                'status_code' => 400,
                'message' => $e->getMessage(),
            ])->setStatusCode(400);
        }
    }
    /**
     * @api {delete} /stafftechnician/{id} (delete post)
     * @apiDescription (delete post)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204 NO CONTENT
     */
    public function destroy($id)
    {
        $stafftechnician = $this->stafftechnician->findOrFail($id);
        $stafftechnician->delete();
        //$post->forceDelete(); delete from table
        return $this->response->noContent();
    }
}
