<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\StageTechnician;
use App\Transformers\StageTechnicianTransformer;
use Illuminate\Http\Request;

class StageTechnicianController extends BaseController
{
    private $stagetechnician;

    public function __construct(StageTechnician $stagetechnician)
    {
        $this->stagetechnician = $stagetechnician;
    }
    public function index()
    {
        $limit = (app('request')->input('limit') && app('request')->input('limit') <= 50 ? app('request')->input('limit') : 10);
        $pagen = (app('request')->input('page') ? app('request')->input('page') : 1);
        $pagename = (app('request')->input('pagename') ? app('request')->input('pagename') : 'page');
        $stagetechnician = $this->stagetechnician->eventJoin()->technicianLeftJoin()->paginate($limit, ['*'], 'page', $pagen);
        return $this->response->paginator($stagetechnician, new StageTechnicianTransformer());
    }

    /**
     * @api {get} /posts/{id} (post detail)
     * @apiDescription (post detail)
     * @apiGroup Post
     * @apiPermission none
     * @apiParam {String='comments','user'} [include]  include
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 200 OK
     *   {
     *     "data": {
     *       "id": 1,
     *       "user_id": 3,
     *       "title": "foo",
     *       "content": "",
     *       "created_at": "2016-03-30 15:36:30",
     *       "user": {
     *         "data": {
     *           "id": 3,
     *           "email": "foo@bar.com1",
     *           "name": "",
     *           "avatar": "",
     *           "created_at": "2016-03-30 15:34:01",
     *           "updated_at": "2016-03-30 15:34:01",
     *           "deleted_at": null
     *         }
     *       },
     *       "comments": {
     *         "data": [
     *           {
     *             "id": 1,
     *             "post_id": 1,
     *             "user_id": 1,
     *             "reply_user_id": 0,
     *             "content": "foobar",
     *             "created_at": "2016-04-06 14:51:34"
     *           }
     *         ],
     *         "meta": {
     *           "total": 1
     *         }
     *       }
     *     }
     *   }
     */
    public function show($id)
    {

        $stagetechnician = $this->stagetechnician->findOrFail($id);
        $fractal = new \League\Fractal\Manager();
        $fractal->setSerializer(new \League\Fractal\Serializer\DataArraySerializer());
        $response = new \League\Fractal\Resource\Item($stagetechnician, new StageTechnicianTransformer());
        return $this->response->array($fractal->createData($response)->toArray());
    }

    /**
     * @api {stagetechnician} /stagetechnician (create stagetechnician)
     * @apiDescription (create stagetechnician)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiParam {String} title
     * @apiParam {String} description
     * @apiParam {DateTime} start
     * @apiParam {DateTime} end
     * @apiParam {DateTime} end
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 201 Created
     */
    public function store(Request $request)
    {

        $validator = \Validator::make($request->input(), [
            'id_stage' => 'required',
            'id_technician' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        $attributes = $request->all();
        $stagetechnician = $this->stagetechnician->create($attributes);
        return $this->response
            ->item($stagetechnician, new StageTechnicianTransformer())
            ->setStatusCode(201);

    }

    /**
     * @api {put} /stagetechnician/{id} (update post)
     * @apiDescription (update post)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiParam {String} title  post title
     * @apiParam {String} content  post content
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204 NO CONTENT
     */
    public function update($id, Request $request)
    {
        $stagetechnician = $this->stagetechnician->findOrFail($id);
        $stagetechnician->type = $request->type;
        $stagetechnician->id_technician = $request->id_technician;
        $stagetechnician->save();
        return $this->response->item($stagetechnician, new StageTechnicianTransformer());
    }

    /**
     * @api {delete} /stagetechnician/{id} (delete post)
     * @apiDescription (delete post)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204 NO CONTENT
     */
    public function destroy($id)
    {
        $stagetechnician = $this->stagetechnician->findOrFail($id);
        $stagetechnician->delete();
        //$post->forceDelete(); delete from table
        return $this->response->noContent();
    }
}
