<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Production;
use App\Transformers\ProductionTransformer;
use Illuminate\Http\Request;
use App\Notifications\UserApplicationSent;
use App\Models\User; 
use App\Models\ProductionMember;
use Illuminate\Support\Facades\Hash;

class ProductionController extends BaseController
{
    private $production;

    public function __construct(Production $production)
    {
        $this->production = $production;
    }

    public function index()
    {
        $limit = (app('request')->input('limit') && app('request')->input('limit') <= 50 ? app('request')->input('limit') : 10);
        $pagen = (app('request')->input('page') ? app('request')->input('page') : 1);
        $pagename = (app('request')->input('pagename') ? app('request')->input('pagename') : 'page');
        $production = $this->production->paginate($limit, ['*'], 'page', $pagen);
        return $this->response->paginator($production, new ProductionTransformer());
    }

    /**
     * @api {get} /posts/{id} (post detail)
     * @apiDescription (post detail)
     * @apiGroup Post
     * @apiPermission none
     * @apiParam {String='comments','user'} [include]  include
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 200 OK
     *   {
     *     "data": {
     *       "id": 1,
     *       "user_id": 3,
     *       "title": "foo",
     *       "content": "",
     *       "created_at": "2016-03-30 15:36:30",
     *       "user": {
     *         "data": {
     *           "id": 3,
     *           "email": "foo@bar.com1",
     *           "name": "",
     *           "avatar": "",
     *           "created_at": "2016-03-30 15:34:01",
     *           "updated_at": "2016-03-30 15:34:01",
     *           "deleted_at": null
     *         }
     *       },
     *       "comments": {
     *         "data": [
     *           {
     *             "id": 1,
     *             "post_id": 1,
     *             "user_id": 1,
     *             "reply_user_id": 0,
     *             "content": "foobar",
     *             "created_at": "2016-04-06 14:51:34"
     *           }
     *         ],
     *         "meta": {
     *           "total": 1
     *         }
     *       }
     *     }
     *   }
     */
    public function show($id)
    {

        $production = $this->production->findOrFail($id);

        return $this->response->item($production, new ProductionTransformer());
    }
    /**
     *
     *
     *
     *
     *
     */
    public function search(Request $request)
    {

        $production = $this->production->customLike($request->all())->orderBy('name')->get();
        return $this->response->item($production, new ProductionTransformer());
    }
    /**
     * @api {production} /production (create production)
     * @apiDescription (create production)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiParam {String} title
     * @apiParam {String} description
     * @apiParam {DateTime} start
     * @apiParam {DateTime} end
     * @apiParam {DateTime} end
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 201 Created
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->input(), [
            'name' => 'required|string|max:50',
            'email' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        \DB::beginTransaction();
        try {
            $attributes['name'] = $request->name;
            $attributes['email'] = $request->email;
            if ($request->has('lat') && $request->has('lng')) {
                $attributes['latitude'] = $request->lat;
                $attributes['longitude'] = $request->lng;
            }
            $attributes['contact'] =($request->has('contact') ? $request->contact : '');
            if ($request->has('address')) {
                $attributes['address'] = $request->address;
            }

            if ($request->has('website')) {
                $attributes['website'] = $request->website;
            }

            if ($request->has('facebook')) {
                $attributes['facebook'] = $request->facebook;
            }

            if ($request->has('twitter')) {
                $attributes['twitter'] = $request->twitter;
            }

            if ($request->has('instagram')) {
                $attributes['instagram'] = $request->instagram;
            }

            $production = $this->production->create($attributes);
            if ($request->has('usercreate') && (bool) $request->usercreate) {
                $validator = \Validator::make($request->all(), [
                    'useremail' => 'required|email|unique:user,email',
                    'username' => 'required|min:3',
                    'active' => 'required',
                ]);
                if ($validator->fails()) {
                    \DB::rollback();
                    return $this->errorBadRequest($validator);
                }
                if ($request->has('userpassword')) {
                    $password = $request->get('userpassword');
                } else {
                    $password = str_random(8);
                }
                if ($request->has('userrole')) {
                    $role = $request->userrole;
                } else { // default role
                    $role = 'adminproduction';
                }
                $user_attributes = [
                    'email' => $request->useremail,
                    'name' => $request->username,
                    'password' => Hash::make($password),
                    'role' => $role,
                    'active' => (int) $request->active,
                ];
                $user = User::create($user_attributes);
                $userid = $user['id'];
                $notification_ = array('password' => $password);
                //productionmember
                $attributes2['name'] = $request->username;
                $attributes2['email'] = $request->useremail;
                $attributes2['id_production'] = $production['id_production'];
                $attributes2['id_user'] = $userid;
                $attributes2['contact'] = ($request->has('contact') ? $request->contact : '');
                $productionmember = ProductionMember::create($attributes2);
                
                //notification
                $user->notify(new UserApplicationSent($notification_));
                
            }
            \DB::commit();
            // Return 201 plus data
            return $this->response
                ->item($production, new ProductionTransformer())
                ->setStatusCode(201);
        } catch (\Exception $e) {
            \DB::rollback();
            if ($validator->fails()) {
                return $this->errorBadRequest($validator);
            }
            return $this->response->array([
                'status_code' => 400,
                'message' => $e->getMessage(),
            ])->setStatusCode(400);
        } catch (\Throwable $e) {
            \DB::rollback();
            return $this->response->array([
                'status_code' => 400,
                'message' => $e->getMessage(),
            ])->setStatusCode(400);

        }
    }

    /**
     * @api {put} /production/{id} (update post)
     * @apiDescription (update post)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiParam {String} title  post title
     * @apiParam {String} content  post content
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204 NO CONTENT
     */
    public function update($id, Request $request)
    {
        $production = $this->production->findOrFail($id);

        $validator = \Validator::make($request->input(), [
            'name' => 'required|string|max:50',
            'email' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        $production->name = $request->name;
        $production->email = $request->email;
        if ($request->has('contact')) {
            $production->contact = $request->contact;
        }

        if ($request->has('address')) {
            $production->address = $request->address;
        }

        if ($request->has('twitter')) {
            $production->twitter = $request->twitter;
        }

        if ($request->has('facebook')) {
            $production->facebook = $request->facebook;
        }

        if ($request->has('instagram')) {
            $production->instagram = $request->instagram;
        }

        if ($request->has('website')) {
            $production->website = $request->website;
        }

        if ($request->has('lat') && $request->has('lng')) {
            $production->latitude = $request->lat;
            $production->longitude = $request->lng;
        }
        $production->save();

        return $this->response->item($production, new ProductionTransformer());
    }

    /**
     * @api {delete} /production/{id} (delete post)
     * @apiDescription (delete post)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204 NO CONTENT
     */
    public function destroy($id)
    {
        $production = $this->production->findOrFail($id);
        $production->delete();
        //$post->forceDelete(); delete from table
        return $this->response->noContent();
    }
}
