<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Scheduleperformance;
use App\Transformers\ScheduleperformanceTransformer;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ScheduleperformanceController extends BaseController
{
    private $scheduleperformance;

    public function __construct(Scheduleperformance $scheduleperformance)
    {
        $this->scheduleperformance = $scheduleperformance;
    }

    public function index()
    {
        $limit = (app('request')->input('limit') && app('request')->input('limit') <= 50 ? app('request')->input('limit') : 10);
        $pagen = (app('request')->input('page') ? app('request')->input('page') : 1);
        $pagename = (app('request')->input('pagename') ? app('request')->input('pagename') : 'page');
        $scheduleperformance = $this->scheduleperformance->paginate($limit, ['*'], 'page', $pagen);
        return $this->response->paginator($scheduleperformance, new ScheduleperformanceTransformer());
    }
    public function stageIndex($id)
    {
        $limit = (app('request')->input('limit') && app('request')->input('limit') <= 50 ? app('request')->input('limit') : 50);
        $pagen = (app('request')->input('page') ? app('request')->input('page') : 1);
        $pagename = (app('request')->input('pagename') ? app('request')->input('pagename') : 'page');
        $scheduleperformance = $this->scheduleperformance
            ->where(['id_stage' => $id])
            ->paginate($limit, ['*'], 'page', $pagen);
        return $this->response->paginator($scheduleperformance, new ScheduleperformanceTransformer());
    }
    public function scheduleperformancedays($id, Request $request)
    {

        $validator = \Validator::make($request->input(), [
            'startdate' => 'required',
            'enddate' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        //$start = Carbon::parse($request->startdate)->format('Y-m-d H:i:s');
       
     //   $end = Carbon::parse($request->enddate)->format('Y-m-d H:i:s');
        $scheduleperformance = $this->scheduleperformance->artistJoin()
        ->where('id_stage' ,'=', $id)
        ->whereDate('date_begin','>=',$request->startdate)
        ->whereDate('date_end','<=',$request->enddate)->get(
            ['scheduleperformance.id_scheduleperformance as id_schedule', 'scheduleperformance.name as title',  \DB::raw('DATE_FORMAT(scheduleperformance.date_begin, "%Y-%m-%dT%TZ") as start'), \DB::raw('DATE_FORMAT(scheduleperformance.date_end, "%Y-%m-%dT%TZ") as end'),'scheduleperformance.id_artist as id_artist','artist.name as artist',\DB::raw('"1" as type')]);
        return $this->response->item($scheduleperformance, new ScheduleperformanceTransformer()); //, new ScheduleperformanceTransformer());
    }
    /*

    $start = Carbon::parse($request->start)->format('Y-m-d H:i:s');
    $end = Carbon::parse($request->end)->format('Y-m-d H:i:s');
    $scheduleperformance = $this->scheduleperformance->eventJoin()->where('stage.id_stage' ,'=', $id)
    ->where('scheduleperformance.date_begin','>=',$start)
    ->where('scheduleperformance.date_end','<=',$end)
    ->get();
    return $this->response->item($scheduleperformance, new ScheduleperformanceTransformer());
     */
    public function show($id)
    {

      
        $scheduleperformance = $this->scheduleperformance->findOrFail($id);
        $fractal = new \League\Fractal\Manager();
        $fractal->setSerializer(new \League\Fractal\Serializer\DataArraySerializer());
        $fractal->parseIncludes(['stage', 'artist']);
        $response = new \League\Fractal\Resource\Item($scheduleperformance, new ScheduleperformanceTransformer());
        return $this->response->array($fractal->createData($response)->toArray());
    }
    /**
     *
     *
     *
     *
     *
     */
    public function search(Request $request)
    {
        $scheduleperformance = $this->scheduleperformance->customLike($request->all())->orderBy('name')->get();
        return $this->response->item($scheduleperformance, new ScheduleperformanceTransformer());
    }
    /**
     * @api {scheduleperformance} /scheduleperformance (create scheduleperformance)
     * @apiDescription (create scheduleperformance)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiParam {String} title
     * @apiParam {String} description
     * @apiParam {DateTime} start
     * @apiParam {DateTime} end
     * @apiParam {DateTime} end
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 201 Created
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->input(), [
            'name' => 'required|string|max:50',
            'stage' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        $start = null;
        $end = null;
        if ($request->has('startdate')) {
            $start = Carbon::parse($request->startdate)->format('Y-m-d H:i:s');
        }
        if ($request->has('enddate')) {
            $end = Carbon::parse($request->enddate)->format('Y-m-d H:i:s');
        }
        if ($request->has('status'))  $attributes['status'] = $request->status;
        $attributes['name'] = $request->name;
        $attributes['date_begin'] = $start;
        $attributes['date_end'] = $end;
        $attributes['id_stage'] = $request->stage;

        if ($request->has('artist')) {
            $attributes['id_artist'] = $request->artist;
        }

        $scheduleperformance = $this->scheduleperformance->create($attributes);
        // Return 201 plus data
        return $this->response
            ->item($scheduleperformance, new ScheduleperformanceTransformer())
            ->setStatusCode(201);
    }

    /**
     * @api {put} /scheduleperformance/{id} (update post)
     * @apiDescription (update post)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiParam {String} title  post title
     * @apiParam {String} content  post content
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204 NO CONTENT
     */
    public function update($id, Request $request)
    {
        $scheduleperformance = $this->scheduleperformance->findOrFail($id);

        $validator = \Validator::make($request->input(), [
            'name' => 'required|string|max:50',
            'stage' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        $start = null;
        $end = null;
        if ($request->has('startdate')) {
            $start = Carbon::parse($request->startdate)->format('Y-m-d H:i:s');
        }
        if ($request->has('enddate')) {
            $end = Carbon::parse($request->enddate)->format('Y-m-d H:i:s');
        }
        if ($request->has('status'))   $scheduleperformance->status = $request->status;
        $scheduleperformance->name = $request->name;
        $scheduleperformance->date_begin = $start;
        $scheduleperformance->date_end = $end;
        if ($request->has('artist')) {
            $scheduleperformance->id_artist = $request->artist;
        }
        $scheduleperformance->id_stage = $request->stage;

        $scheduleperformance->save();

        return $this->response->item($scheduleperformance, new ScheduleperformanceTransformer());
    }

    /**
     * @api {delete} /scheduleperformance/{id} (delete post)
     * @apiDescription (delete post)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204 NO CONTENT
     */
    public function destroy($id)
    {
        $scheduleperformance = $this->scheduleperformance->findOrFail($id);
        $scheduleperformance->delete();
        //$post->forceDelete(); delete from table
        return $this->response->noContent();
    }
}
