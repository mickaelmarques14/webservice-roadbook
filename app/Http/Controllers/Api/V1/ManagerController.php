<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Manager;
use App\Models\ProductionManager;
use App\Models\User;
use App\Notifications\UserApplicationSent;
use App\Transformers\ManagerTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ManagerController extends BaseController
{
    private $manager;

    public function __construct(Manager $manager)
    {
        $this->manager = $manager;
    }

    public function index()
    {
        $limit = (app('request')->input('limit') && app('request')->input('limit') <= 50 ? app('request')->input('limit') : 10);
        $pagen = (app('request')->input('page') ? app('request')->input('page') : 1);
        $pagename = (app('request')->input('pagename') ? app('request')->input('pagename') : 'page');
        if (in_array('admin', $this->user()->role) || in_array('superadmin', $this->user()->role)) {
            $manager = $this->manager->paginate($limit, ['*'], 'page', $pagen);
        }
        if (in_array('adminproduction', $this->user()->role) || in_array('production', $this->user()->role)) {
            $restrict = $this->user()->production_member()->first();
            $condition = isset($restrict->id_production) ? $restrict->id_production : null;
            $manager = $this->manager
            ->join('production_manager','production_manager.id_manager','manager.id_manager')
            ->where('id_production', $condition)->paginate($limit, ['*'], $pagename, $pagen);
        }
        
        return $this->response->paginator($manager, new ManagerTransformer());
    }

    /**
     * @api {get} /posts/{id} (post detail)
     * @apiDescription (post detail)
     * @apiGroup Post
     * @apiPermission none
     * @apiParam {String='comments','user'} [include]  include
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 200 OK
     *   {
     *     "data": {
     *       "id": 1,
     *       "user_id": 3,
     *       "title": "foo",
     *       "content": "",
     *       "created_at": "2016-03-30 15:36:30",
     *       "user": {
     *         "data": {
     *           "id": 3,
     *           "email": "foo@bar.com1",
     *           "name": "",
     *           "avatar": "",
     *           "created_at": "2016-03-30 15:34:01",
     *           "updated_at": "2016-03-30 15:34:01",
     *           "deleted_at": null
     *         }
     *       },
     *       "comments": {
     *         "data": [
     *           {
     *             "id": 1,
     *             "post_id": 1,
     *             "user_id": 1,
     *             "reply_user_id": 0,
     *             "content": "foobar",
     *             "created_at": "2016-04-06 14:51:34"
     *           }
     *         ],
     *         "meta": {
     *           "total": 1
     *         }
     *       }
     *     }
     *   }
     */
    public function show($id)
    {

        $manager = $this->manager->findOrFail($id)->makeVisible(['avatar']);
        $fractal = new \League\Fractal\Manager();
        $fractal->setSerializer(new \League\Fractal\Serializer\DataArraySerializer());
        $fractal->parseIncludes(['user']);
        $response = new \League\Fractal\Resource\Item($manager, new ManagerTransformer());
        return $this->response->array($fractal->createData($response)->toArray());
    }
    /*
     */
    public function search(Request $request)
    {
        $manager = $this->manager->customLike($request->all())->orderBy('name')->get();
        return $this->response->item($manager, new ManagerTransformer());
    }
    /**
     * @api {manager} /manager (create manager)
     * @apiDescription (create manager)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiParam {String} title
     * @apiParam {String} description
     * @apiParam {DateTime} start
     * @apiParam {DateTime} end
     * @apiParam {DateTime} end
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 201 Created
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->input(), [
            'name' => 'required|string|max:50',
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        \DB::beginTransaction();
        try {
            $userid = null;
            if ($request->has('usercreate') && (bool) $request->usercreate) { //create user true
                $validator = \Validator::make($request->all(), [
                    'useremail' => 'required|email|unique:user,email',
                    'username' => 'required|min:3',
                    'active' => 'required',
                ]);
                if ($validator->fails()) {
                    \DB::rollback();
                    return $this->errorBadRequest($validator);
                }
                if ($request->has('userpassword')) {
                    $password = $request->get('userpassword');
                } else {
                    $password = str_random(8);
                }
                if ($request->has('userrole')) {
                    $role = $request->userrole;
                } else { // default role
                    $role = 'user';
                }
                $user_attributes = [
                    'email' => $request->useremail,
                    'name' => $request->username,
                    'password' => Hash::make($password),
                    'role' => $role,
                    'active' => (int) $request->active,
                ];
                $user = User::create($user_attributes);
                $userid = $user['id'];
                $notification_ = array('password' => $password);
                $user->notify(new UserApplicationSent($notification_));
            }
            $attributes['name'] = $request->name;
            $attributes['email'] = $request->name;
            $attributes['id_user'] = $userid;
            $attributes['contact'] = ($request->has('contact') ? $request->contact : '');
            $manager = $this->manager->create($attributes);
            if (in_array('adminproduction', $this->user()->role) || in_array('production', $this->user()->role)) {
                $restrict = $this->user()->production_member()->first();
                $condition = isset($restrict->id_production) ? $restrict->id_production : null;
                ProductionManager::create(['id_manager' => $manager['id_manager'], 'id_production' => $condition]);
            }
            \DB::commit();
            return $this->response
                ->item($manager, new ManagerTransformer())
                ->setStatusCode(201);
        } catch (\Exception $e) {
            \DB::rollback();
            if ($validator->fails()) {
                return $this->errorBadRequest($validator);
            }
            return $this->response->array([
                'status_code' => 400,
                'message' => $e->getMessage(),
            ])->setStatusCode(400);
        } catch (\Throwable $e) {
            \DB::rollback();
            return $this->response->array([
                'status_code' => 400,
                'message' => $e->getMessage(),
            ])->setStatusCode(400);

        }
    }

    /**
     * @api {put} /manager/{id} (update post)
     * @apiDescription (update post)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiParam {String} title  post title
     * @apiParam {String} content  post content
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204 NO CONTENT
     */
    public function update($id, Request $request)
    {
        $manager = $this->manager->findOrFail($id);

        $validator = \Validator::make($request->input(), [
            'name' => 'required|string|max:50',
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        $manager->name = $request->name;
        $manager->save();
        return $this->response->item($manager, new ManagerTransformer());
    }

    /**
     * @api {delete} /manager/{id} (delete post)
     * @apiDescription (delete post)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204 NO CONTENT
     */
    public function destroy($id)
    {
        $manager = $this->manager->findOrFail($id);
        if (in_array('adminproduction', $this->user()->role) || in_array('production', $this->user()->role)) {
            $restrict = $this->user()->production_member()->first();
            $condition = isset($restrict->id_production) ? $restrict->id_production : null;
            ProductionManager::where('id_manager', $manager['id_manager'])->orWhere('id_production', $condition)->delete();
        }else {
        $manager->delete();
        }
        //$post->forceDelete(); delete from table
        return $this->response->noContent();
    }
}
