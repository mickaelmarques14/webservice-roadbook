<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Stagedocumentcomment;
use App\Transformers\StagedocumentcommentTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use App\Notifications\UserCommentSent;
use App\Models\User;
use App\Models\Performanceartistmanager;

class StagedocumentcommentController extends BaseController
{
    private $stagedocumentcomment;
    private $listfields;

    public function __construct(Stagedocumentcomment $stagedocumentcomment)
    {
        $this->stagedocumentcomment = $stagedocumentcomment;
        $this->listfields = [
            'stagedocumentcomment.id_stagedocumentcomment', 'stagedocumentcomment.name', 'stagedocumentcomment.status'];
    }
    public function index()
    {
        $limit = (app('request')->input('limit') && app('request')->input('limit') <= 50 ? app('request')->input('limit') : 10);
        $pagen = (app('request')->input('page') ? app('request')->input('page') : 1);
        $pagename = (app('request')->input('pagename') ? app('request')->input('pagename') : 'page');
        $stagedocumentcomment = $this->stagedocumentcomment->paginate($limit, $this->listfields, 'page', $pagen);
        return $this->response->paginator($stagedocumentcomment, new StagedocumentcommentTransformer());
    }
    /**
     * @api {get} /posts/{id} (post detail)
     * @apiDescription (post detail)
     * @apiGroup Post
     * @apiPermission none
     * @apiParam {String='comments','user'} [include]  include
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 200 OK
     *   {
     *     "data": {
     *       "id": 1,
     *       "user_id": 3,
     *       "title": "foo",
     *       "content": "",
     *       "created_at": "2016-03-30 15:36:30",
     *       "user": {
     *         "data": {
     *           "id": 3,
     *           "email": "foo@bar.com1",
     *           "name": "",
     *           "avatar": "",
     *           "created_at": "2016-03-30 15:34:01",
     *           "updated_at": "2016-03-30 15:34:01",
     *           "deleted_at": null
     *         }
     *       },
     *       "comments": {
     *         "data": [
     *           {
     *             "id": 1,
     *             "post_id": 1,
     *             "user_id": 1,
     *             "reply_user_id": 0,
     *             "content": "foobar",
     *             "created_at": "2016-04-06 14:51:34"
     *           }
     *         ],
     *         "meta": {
     *           "total": 1
     *         }
     *       }
     *     }
     *   }
     */
    public function show($id)
    {
        $stagedocumentcomment = $this->stagedocumentcomment->select($this->listfields)->findOrFail($id);
        return $this->response->item($stagedocumentcomment, new StagedocumentcommentTransformer());
    }
    /**
     *
     *
     *
     *
     *
     */
    public function search(Request $request)
    {
        $stagedocumentcomment = $this->stagedocumentcomment->customLike($request->all())->orderBy('name')->get();
        return $this->response->item($stagedocumentcomment, new StagedocumentcommentTransformer());
    }
    /**
     * @api {stagedocumentcomment} /stagedocumentcomment (create stagedocumentcomment)
     * @apiDescription (create stagedocumentcomment)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiParam {String} title
     * @apiParam {String} description
     * @apiParam {DateTime} start
     * @apiParam {DateTime} end
     * @apiParam {DateTime} end
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 201 Created
     */
    public function store(Request $request)
    {

        $validator = \Validator::make($request->input(), [
            'stagedocument' => 'required',
            'name' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        $a = $this->user();
        $attributes['name'] = $request->name;
        $attributes['status'] = $request->status;
        $attributes['id_stagedocument'] = $request->stagedocument;
        $attributes['id_user'] = $this->user()->id;
        if ($request->has('user') && (in_array('admin', $this->user()->role) || in_array('superadmin', $this->user()->role)))
        $attributes['id_user'] = $request->user;
        $stagedocumentcomment = $this->stagedocumentcomment->create($attributes);
        $usersend = User::find($attributes['id_user']);
        $artists = $stagedocumentcomment->stagedocument->stage->scheduleperformance->artist;
        $managers = $artists->manager;
      /*  if($user){
            $notification_ = array('user' => $user['email'], 'status' => $attributes['status'] ,'message');
        $user->notify(new UserCommentSent($notification_));
        }*/
        // Return 201 plus data
        return $this->response
            ->item($stagedocumentcomment, new StagedocumentcommentTransformer())
            ->setStatusCode(201);
    }

    /**
     * @api {put} /stagedocumentcomment/{id} (update post)
     * @apiDescription (update post)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiParam {String} title  post title
     * @apiParam {String} content  post content
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204 NO CONTENT
     */
    public function update($id, Request $request)
    {
        $stagedocumentcomment = $this->stagedocumentcomment->findOrFail($id);
        $validator = \Validator::make($request->input(), [
            'stagedocument' => 'required',
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        $stagedocumentcomment->name = $request->name;
        $stagedocumentcomment->status = $request->status;
        $stagedocumentcomment->id_stagedocument = $request->stagedocument;
        $stagedocumentcomment->id_user = $request->user;
        $stagedocumentcomment->save();
        return $this->response->item($stagedocumentcomment, new StagedocumentcommentTransformer());
    }

    /**
     * @api {delete} /stagedocumentcomment/{id} (delete post)
     * @apiDescription (delete post)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204 NO CONTENT
     */
    public function destroy($id)
    {
        $stagedocumentcomment = $this->stagedocumentcomment->findOrFail($id);
        $stagedocumentcomment->delete();
        //$post->forceDelete(); delete from table
        return $this->response->noContent();
    }
}
