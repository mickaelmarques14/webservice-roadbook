<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Stagedocument;
use App\Transformers\StagedocumentTransformer;
use Illuminate\Http\Request;


class StagedocumentController extends BaseController
{
    private $stagedocument;
    private $listfields;

    public function __construct(Stagedocument $stagedocument)
    {
        $this->stagedocument = $stagedocument;
        $this->listfields = [
            'stagedocument.id_stagedocument', 'stagedocument.name', 'stagedocument.status'];
    }
    public function index()
    {
        $limit = (app('request')->input('limit') && app('request')->input('limit') <= 50 ? app('request')->input('limit') : 10);
        $pagen = (app('request')->input('page') ? app('request')->input('page') : 1);
        $pagename = (app('request')->input('pagename') ? app('request')->input('pagename') : 'page');
        $stagedocument = $this->stagedocument->paginate($limit, $this->listfields, 'page', $pagen);
        return $this->response->paginator($stagedocument, new StagedocumentTransformer());
    }
    public function stage($id, Request $request){
        
    }

    /**
     * @api {get} /posts/{id} (post detail)
     * @apiDescription (post detail)
     * @apiGroup Post
     * @apiPermission none
     * @apiParam {String='comments','user'} [include]  include
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:

     */
    public function show($id)
    {

        $stagedocument = $this->stagedocument->select($this->listfields)->findOrFail($id);

        return $this->response->item($stagedocument, new StagedocumentTransformer());
    }
    /**
     *
     *
     *
     *
     *
     */
    public function search(Request $request)
    {
        $stagedocument = $this->stagedocument->customLike($request->all())->orderBy('name')->get();
        return $this->response->item($stagedocument, new StagedocumentTransformer());
    }
    public function post($id, Request $request)
    {
        if(!empty($id)) {
        $allstagedocument = array_column(Stagedocument::where('id_stage', $id)->select('id_stagedocument')->get()->toArray(),'id_stagedocument');
        \DB::beginTransaction();
        try {
            foreach ($request->data as $value) {
                if ($value['id_stagedocument'] != 0) { //update
                    $stagedocument = $this->stagedocument->findOrFail($value['id_stagedocument']);
                    $stagedocument->name = $value['name'];
                    $stagedocument->file = $value['file'];
                    $stagedocument->description = $value['description'];
                    $stagedocument->id_stage = $id;
                    //$stagedocument->id_user = $value['person'];
                   /* $stagedocument->status = 'pending' ;
                    if($request->>has('status'))
                    $stagedocument->status = $request->status;
                    */
                    $stagedocument->save();
                    if (($key = array_search($value['id_stagedocument'], $allstagedocument)) !== false) { // value exist, don't remove
                        unset($allstagedocument[$key]);
                    }
                } else { //create

                    //$attributes['name'] ='pending' ;
                    $attributes['name'] = $value['name'];
                    $attributes['description'] = $value['description'];
                    $attributes['file'] = $value['file'];
                    $attributes['id_stage'] = $id;
                    $stagedocument = $this->stagedocument->create($attributes);
                }
            }
            if (!empty($allstagedocument)) {
                Stagedocument::whereIn('id_stagedocument', $allstagedocument)->delete();
            }
            \DB::commit();
            return $this->response->array([
                'status_code' => 200,
                'data' => 'Success',
            ])->setStatusCode(200);
        } catch (\Exception $e) {
            \DB::rollback();
            return $this->response->array([
                'status_code' => 400,
                'message' => $e->getMessage(),
            ])->setStatusCode(400);
        } catch (\Throwable $e) {
            \DB::rollback();
            return $this->response->array([
                'status_code' => 400,
                'message' => $e->getMessage(),
            ])->setStatusCode(400);
        }
    }
    }
    /**
     * @api {stagedocument} /stagedocument (create stagedocument)
     * @apiDescription (create stagedocument)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiParam {String} title
     * @apiParam {String} description
     * @apiParam {DateTime} start
     * @apiParam {DateTime} end
     * @apiParam {DateTime} end
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 201 Created
     */
    public function store(Request $request)
    {

        $validator = \Validator::make($request->input(), [
            'stage' => 'required',
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        $attributes['name'] = $request->name;
        $attributes['description'] = $request->status;
        $attributes['id_stage'] = $request->stage;
        $stagedocument = $this->stagedocument->create($attributes);
        // Return 201 plus data
        return $this->response
            ->item($stagedocument, new StagedocumentTransformer())
            ->setStatusCode(201);
    }

    /**
     * @api {put} /stagedocument/{id} (update post)
     * @apiDescription (update post)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiParam {String} title  post title
     * @apiParam {String} content  post content
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204 NO CONTENT
     */
    public function update($id, Request $request)
    {
        $stagedocument = $this->stagedocument->findOrFail($id);

        $validator = \Validator::make($request->input(), [
            'stage' => 'required',
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        $stagedocument->name = $request->name;
        $stagedocument->description = $request->status;
        $stagedocument->id_stage = $request->stage;
        $stagedocument->save();
        return $this->response->item($stagedocument, new StagedocumentTransformer());
    }

    /**
     * @api {delete} /stagedocument/{id} (delete post)
     * @apiDescription (delete post)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204 NO CONTENT
     */
    public function destroy($id)
    {
        $stagedocument = $this->stagedocument->findOrFail($id);
        $stagedocument->delete();
        //$post->forceDelete(); delete from table
        return $this->response->noContent();
    }
    public function upload(Request $request){
		$validator = \Validator::make($request->all(), [
			'file' => 'file|mimes:pdf,doc,docx|max:10000'//10mb
		]);
		if ($validator->fails()) {
			return $this->errorBadRequest($validator);
        }
        $path = app()->basePath('public/files/');
        if(!file_exists( $path)) mkdir( $path,'0755');
		$imageData = $request->file('file');
		$filename = time().str_random(5).'.'.$imageData->getClientOriginalExtension();
		$destinationPath = rtrim($path);
		$imageData->move($destinationPath, $filename);
return $this->response->array([
    'status_code' => 200,
    'data' => ['file'=>$filename, 'path'=>url('files/'.$filename)]
])->setStatusCode(200);
	}
}
