<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Schedulehotel;
use App\Transformers\SchedulehotelTransformer;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SchedulehotelController extends BaseController
{
    private $schedulehotel;

    public function __construct(Schedulehotel $schedulehotel)
    {
        $this->schedulehotel = $schedulehotel;
    }

    public function index()
    {
        $limit = (app('request')->input('limit') && app('request')->input('limit') <= 50 ? app('request')->input('limit') : 10);
        $pagen = (app('request')->input('page') ? app('request')->input('page') : 1);
        $pagename = (app('request')->input('pagename') ? app('request')->input('pagename') : 'page');
        $schedulehotel = $this->schedulehotel->paginate($limit, ['*'], 'page', $pagen);
        return $this->response->paginator($schedulehotel, new SchedulehotelTransformer());
    }
    public function stageIndex($id)
    {
        $limit = (app('request')->input('limit') && app('request')->input('limit') <= 50 ? app('request')->input('limit') : 50);
        $pagen = (app('request')->input('page') ? app('request')->input('page') : 1);
        $pagename = (app('request')->input('pagename') ? app('request')->input('pagename') : 'page');
        $schedulehotel = $this->schedulehotel
            ->where(['id_stage' => $id])
            ->paginate($limit, ['*'], 'page', $pagen);
        return $this->response->paginator($schedulehotel, new SchedulehotelTransformer());
    }
    public function schedulehoteldays($id, Request $request)
    {
        $validator = \Validator::make($request->input(), [
            'startdate' => 'required',
            'enddate' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        // $start = Carbon::parse($request->startdate)->format('Y-m-d H:i:s');
        // $end = Carbon::parse($request->enddate)->format('Y-m-d H:i:s');
        $schedulehotel = $this->schedulehotel->artistJoin()->hotelJoin()
            ->where('id_stage', '=', $id)
            ->whereDate('date_begin', '>=', $request->startdate)
            ->whereDate('date_end', '<=', $request->enddate)->get(
            ['schedulehotel.id_schedulehotel as id_schedule', 'schedulehotel.name as title',
                \DB::raw('DATE_FORMAT(schedulehotel.date_begin, "%Y-%m-%dT%TZ") as start'), \DB::raw('DATE_FORMAT(schedulehotel.date_end, "%Y-%m-%dT%TZ") as end'),
                'single', 'double', 'twin', 'schedulehotel.comment', 'schedulehotel.id_hotel as hotel', 'hotel.name as hotelname',
                'schedulehotel.id_artist as id_artist', 'artist.name as artist', \DB::raw('"3" as type')]);
        return $this->response->item($schedulehotel, new SchedulehotelTransformer());
    }
    /*

    $start = Carbon::parse($request->start)->format('Y-m-d H:i:s');
    $end = Carbon::parse($request->end)->format('Y-m-d H:i:s');
    $schedulehotel = $this->schedulehotel->eventJoin()->where('stage.id_stage' ,'=', $id)
    ->where('schedulehotel.date_begin','>=',$start)
    ->where('schedulehotel.date_end','<=',$end)
    ->get();
    return $this->response->item($schedulehotel, new SchedulehotelTransformer());
     */
    public function show($id)
    {

        $schedulehotel = $this->schedulehotel->findOrFail($id);
        $fractal = new \League\Fractal\Manager();
        $fractal->setSerializer(new \League\Fractal\Serializer\DataArraySerializer());
        $fractal->parseIncludes(['hotel', 'stage', 'artist']);
        $response = new \League\Fractal\Resource\Item($schedulehotel, new SchedulehotelTransformer());
        return $this->response->array($fractal->createData($response)->toArray());
    }
    /**
     *
     *
     *
     *
     *
     */
    public function search(Request $request)
    {
        $schedulehotel = $this->schedulehotel->customLike($request->all())->orderBy('name')->get();
        return $this->response->item($schedulehotel, new SchedulehotelTransformer());
    }
    /**
     * @api {schedulehotel} /schedulehotel (create schedulehotel)
     * @apiDescription (create schedulehotel)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiParam {String} title
     * @apiParam {String} description
     * @apiParam {DateTime} start
     * @apiParam {DateTime} end
     * @apiParam {DateTime} end
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 201 Created
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->input(), [
            'name' => 'required|string|max:50',
            'stage' => 'required',
            'startdate' => 'required',
            'enddate' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        $start = null;
        $end = null;
        if ($request->has('startdate')) {
            $start = Carbon::parse($request->startdate)->format('Y-m-d H:i:s');
        }
        if ($request->has('enddate')) {
            $end = Carbon::parse($request->enddate)->format('Y-m-d H:i:s');
        }
        if ($request->has('status')) {
            $attributes['status'] = $request->status;
        }

        $attributes['name'] = $request->name;
        $attributes['date_begin'] = $start;
        $attributes['date_end'] = $end;
        $attributes['id_stage'] = $request->stage;
        if ($request->has('hotel')) {
            $attributes['id_hotel'] = $request->hotel;
        }
        $attributes['single'] = ($request->has('single')) ? $request->single : 0;
        $attributes['double'] = ($request->has('double')) ? $request->double : 0;
        $attributes['twin'] = ($request->has('twin')) ? $request->twin : 0;
        $attributes['comment'] = $request->comment;
        if ($request->has('artist')) {
            $attributes['id_artist'] = $request->artist;
        }

        $schedulehotel = $this->schedulehotel->create($attributes);
        // Return 201 plus data
        return $this->response
            ->item($schedulehotel, new SchedulehotelTransformer())
            ->setStatusCode(201);
    }

    /**
     * @api {put} /schedulehotel/{id} (update post)
     * @apiDescription (update post)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiParam {String} title  post title
     * @apiParam {String} content  post content
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204 NO CONTENT
     */
    public function update($id, Request $request)
    {
        $schedulehotel = $this->schedulehotel->findOrFail($id);

        $validator = \Validator::make($request->input(), [
            'name' => 'required|string|max:50',
            'stage' => 'required',
            'startdate' => 'required',
            'enddate' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        $start = null;
        $end = null;
        if ($request->has('startdate')) {
            $start = Carbon::parse($request->startdate)->format('Y-m-d H:i:s');
        }
        if ($request->has('enddate')) {
            $end = Carbon::parse($request->enddate)->format('Y-m-d H:i:s');
        }
        if ($request->has('status')) {
            $schedulehotel->status = $request->status;
        }

        $schedulehotel->name = $request->name;
        $schedulehotel->date_begin = $start;
        $schedulehotel->date_end = $end;
        $schedulehotel->single = $request->single;
        $schedulehotel->double = $request->double;
        $schedulehotel->twin = $request->twin;
        $schedulehotel->comment = $request->comment;
        if ($request->has('hotel')) {
            $schedulehotel->id_hotel = $request->hotel;
        }
        if ($request->has('artist')) {
            $schedulehotel->id_artist = $request->artist;
        }
        $schedulehotel->id_stage = $request->stage;

        $schedulehotel->save();

        return $this->response->item($schedulehotel, new SchedulehotelTransformer());
    }

    /**
     * @api {delete} /schedulehotel/{id} (delete post)
     * @apiDescription (delete post)
     * @apiGroup Post
     * @apiPermission jwt
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204 NO CONTENT
     */
    public function destroy($id)
    {
        $schedulehotel = $this->schedulehotel->findOrFail($id);
        $schedulehotel->delete();
        //$post->forceDelete(); delete from table
        return $this->response->noContent();
    }
}
