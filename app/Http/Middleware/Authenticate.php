<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Response;

class Authenticate
{
    /**
     * The authentication guard factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param \Illuminate\Contracts\Auth\Factory $auth
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     * @param string|null              $guard
     *
     * @return mixed
    "message": "Token has expired",
    "status_code": 401,
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $newToken = false;

        $token = \Auth::getToken();

        if (!$token) {
            return response()->json([
                'status_code' => 403,
                'message' => 'Token required',
            ])->setStatusCode(403);
        }
     
        try {
            \Auth::checkOrFail($token); //valid
        } catch (TokenExpiredException $e) {
            try {
                //try to renew
                $newToken = \Auth::refresh();
            } catch (JWTException $e) {
                return response()->json(['status_code' => 403, 'message' => 'Token expired'])->setStatusCode(403);
            }
        } catch (JWTException $e) {
            return response()->json(['status_code' => 403, 'message' => 'Invalid token'])->setStatusCode(403);
        }

        $response = $next($request);
        // possible fix for options error //
        $response->header('Access-Control-Allow-Methods', 'HEAD, GET, POST, PUT, PATCH, DELETE');
        $response->header('Access-Control-Allow-Headers', $request->header('Access-Control-Request-Headers'));
        $response->header('Access-Control-Allow-Origin', '*');

        if ($newToken) {
            $response->header('Access-Control-Expose-Headers', 'Authorization'); //expose the new token
            $response->headers->set('Authorization', 'Bearer ' . $newToken);
        } else {
            $response->headers->set('Authorization', 'Bearer ' . $token);
        }

        return $response;
        /*
    // Login the user instance for global usage
    if ($this->auth->guard($guard)->guest()) {
    return response('Unauthorized.', 401);
    }

    return $next($request); */
    }
}
